//
//  FirstViewController.swift
//  AlcoholMonitor
//
//  Created by Jack on 25/06/2015.
//  Copyright (c) 2015 Unimelb. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkFirstLaunch()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        
    }
    
    func checkFirstLaunch(){
        
        var userDefaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if(userDefaults.objectForKey("userInfo") == nil){
        
            // Create the alert controller
            var alertController = UIAlertController(title: "First Time Launch", message: "Please Setup your personal information", preferredStyle: .Alert)
        
            // Create the actions
            var okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                alertController.dismissViewControllerAnimated(true, completion: nil)
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewControllerWithIdentifier("InformationSetUpViewController") as! UIViewController // Explicit cast is required here.
                viewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
                self.presentViewController(viewController, animated: true, completion: nil)
                self.navigationItem.hidesBackButton = true;
                
                NSLog("OK Pressed---Go to set up personal information")
            }
            // Add the actions
            alertController.addAction(okAction)
            
            // Present the controller
            self.navigationController!.presentViewController(alertController, animated: true, completion: nil)
         }
    }
    
    func removeOldTemplete(){
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if(userDefaults.objectForKey("drinksSubmission") != nil){
            userDefaults.removeObjectForKey("drinksSubmission")
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if(identifier == "createNewTemplete"){
            self.removeOldTemplete()
        }

        return true
    }
}

