//
//  SubmitTempleteTableViewController.swift
//  AlcoholMonitor
//
//  Created by Jack on 26/06/2015.
//  Copyright (c) 2015 Unimelb. All rights reserved.
//

import UIKit

class SubmitTempleteTableViewController: UITableViewController {
    
    
    let kPickerAnimationDuration = 0.40 // duration for the animation to slide the date picker into view
    let kDatePickerTag           = 99   // view tag identifiying the date picker view
    
    let kTitleKey = "title" // key for obtaining the data source item's title
    let kDateKey  = "date"  // key for obtaining the data source item's date value
    
    // keep track of which rows have date cells
    let kDateStartRow = 0
    let kDateEndRow   = 1
    
    let kDateCellID       = "dateCell";       // the cells with the start or end date
    let kDatePickerCellID = "datePickerCell"; // the cell containing the date picker
    let kDrinkCellID      = "drinkCell";      // the remaining cells at the end
    let kAddDrinkCellID = "addDrinkCell"
    
    var dateArray: [[String: AnyObject]] = []
    var drinkArray:NSMutableArray = NSMutableArray()
    var dateFormatter = NSDateFormatter()
    
    // keep track which indexPath points to the cell with UIDatePicker
    var datePickerIndexPath: NSIndexPath?
    
    var pickerCellRowHeight: CGFloat = 216
    
    var cellHeight = CGFloat(60)
    var imageHeight = CGFloat(50)
    var imageWidth = CGFloat(30)

    @IBOutlet var pickerView: UIDatePicker!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup date data source
        let itemOne = [kTitleKey : "Start Date", kDateKey : NSDate()]
        let itemTwo = [kTitleKey : "End Date", kDateKey : NSDate()]
        dateArray = [itemOne, itemTwo]
        
        dateFormatter.dateStyle = .MediumStyle // show short-style date format
        dateFormatter.timeStyle = .ShortStyle
        
        // if the local changes while in the background, we need to be notified so we can update the date
        // format in the table view cells
        //
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "localeChanged:", name: NSCurrentLocaleDidChangeNotification, object: nil)
    }
    
    
    override func viewWillAppear(animated: Bool) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.removeObjectForKey("tempCurrentDrinkSubmission")
        
        if(userDefaults.objectForKey("drinksSubmission") != nil){
            drinkArray = userDefaults.objectForKey("drinksSubmission") as! NSMutableArray
        }
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Locale
    
    /*! Responds to region format or locale changes.
    */
    func localeChanged(notif: NSNotification) {
        // the user changed the locale (region format) in Settings, so we are notified here to
        // update the date format in the table view cells
        //
        tableView.reloadData()
    }
    
    
    /*! Determines if the given indexPath has a cell below it with a UIDatePicker.
    
    @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
    */
    func hasPickerForIndexPath(indexPath: NSIndexPath) -> Bool {
        var hasDatePicker = false
        
        let targetedRow = indexPath.row + 1
        
        let checkDatePickerCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: targetedRow, inSection: 0))
        let checkDatePicker = checkDatePickerCell?.viewWithTag(kDatePickerTag)
        
        hasDatePicker = checkDatePicker != nil
        return hasDatePicker
    }
    
    /*! Updates the UIDatePicker's value to match with the date of the cell above it.
    */
    func updateDatePicker() {
        if let indexPath = datePickerIndexPath {
            let associatedDatePickerCell = tableView.cellForRowAtIndexPath(indexPath)
            if let targetedDatePicker = associatedDatePickerCell?.viewWithTag(kDatePickerTag) as! UIDatePicker? {
                let itemData = dateArray[self.datePickerIndexPath!.row - 1]
                targetedDatePicker.setDate(itemData[kDateKey] as! NSDate, animated: false)
            }
        }
    }
    
    /*! Determines if the UITableViewController has a UIDatePicker in any of its cells.
    */
    func hasInlineDatePicker() -> Bool {
        return datePickerIndexPath != nil
    }
    
    /*! Determines if the given indexPath points to a cell that contains the UIDatePicker.
    
    @param indexPath The indexPath to check if it represents a cell with the UIDatePicker.
    */
    func indexPathHasPicker(indexPath: NSIndexPath) -> Bool {
        return hasInlineDatePicker() && datePickerIndexPath?.row == indexPath.row
    }
    
    /*! Determines if the given indexPath points to a cell that contains the start/end dates.
    
    @param indexPath The indexPath to check if it represents start/end date cell.
    */
    func indexPathHasDate(indexPath: NSIndexPath) -> Bool {
        var hasDate = false
        
        if (indexPath.row == kDateStartRow) || (indexPath.row == kDateEndRow || (hasInlineDatePicker() && (indexPath.row == kDateEndRow + 1))) {
            hasDate = true
        }
        return hasDate
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 2 && drinkArray.count > 0){
            return cellHeight
        }
        return (indexPathHasPicker(indexPath) ? pickerCellRowHeight : tableView.rowHeight)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 3
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        let sectionTitle = ["Date & Time","Add Drinks","Drinks Consumed"]
        return sectionTitle[section]
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if hasInlineDatePicker() {
                // we have a date picker, so allow for it in the number of rows in this section
                var numRows = dateArray.count
                return ++numRows;
            }
            return dateArray.count;
        }else if(section == 1){
                return 1
        }else{
            return drinkArray.count < 1 ? 1 : drinkArray.count;
        }
        
    }
    

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        var cellID = kDrinkCellID
        if(indexPath.section == 0){
            if indexPathHasPicker(indexPath) {
                // the indexPath is the one containing the inline date picker
                cellID = kDatePickerCellID     // the current/opened date picker cell
            } else if indexPathHasDate(indexPath) {
                // the indexPath is one that contains the date information
                cellID = kDateCellID       // the start/end date cells
            }
            
            cell = tableView.dequeueReusableCellWithIdentifier(cellID) as? UITableViewCell
            
            // if we have a date picker open whose cell is above the cell we want to update,
            // then we have one more cell than the model allows
            //
            var modelRow = indexPath.row
            if (datePickerIndexPath != nil && datePickerIndexPath?.row <= indexPath.row) {
                modelRow--
            }
            
            let itemData = dateArray[modelRow]
            if(cellID == kDateCellID){
                cell?.textLabel?.text = itemData[kTitleKey] as? String
                cell?.detailTextLabel?.text = self.dateFormatter.stringFromDate(itemData[kDateKey] as! NSDate)
            }
        }else if(indexPath.section == 1){
            cellID = kAddDrinkCellID
            cell = tableView.dequeueReusableCellWithIdentifier(cellID) as? UITableViewCell
        }
        else{
            cell = tableView.dequeueReusableCellWithIdentifier(cellID) as! UITableViewCell
            if(drinkArray.count == 0){
                cell?.textLabel?.text = "Haven't Add Any Drinks"
            }else{
                cell?.textLabel?.text = ""
                let currentDrink = drinkArray[indexPath.row] as! NSDictionary
                let size = "Size: " + (currentDrink["sizeCell"] as! String)
                let quantity = "Quantity: " + (currentDrink["quantityCell"] as! String)

                let pathForDrinksPropertyList = NSBundle.mainBundle().pathForResource("DrinksPropertyDic", ofType: "plist")
                let drinksPropertyDic = NSDictionary(contentsOfFile: pathForDrinksPropertyList!)
                let imageName = (drinksPropertyDic!.objectForKey(currentDrink["drinkTypeCell"]!) as! NSDictionary).objectForKey("ImageName") as! String
                let image = UIImage(named: imageName)
                if (cell == nil) {
                    cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellID)
                }
                
                if(cell!.viewWithTag(10) == nil || cell!.viewWithTag(11) == nil || cell!.viewWithTag(12) == nil){
                    
                    var oneLabel:UILabel = UILabel(frame: CGRectMake(60, 0, cell.frame.size.width - cell.imageView!.frame.width, cellHeight / 2 ))
                    oneLabel.font = UIFont(name: cell.textLabel!.font.fontName, size: 15)
                    oneLabel.textAlignment = NSTextAlignment.Left
                    oneLabel.text = size
                    oneLabel.tag = 10
                    cell!.contentView.addSubview(oneLabel)
                    
                    var twoLabel:UILabel = UILabel(frame: CGRectMake(60, cellHeight / 2, cell.frame.size.width - cell.imageView!.frame.width, cellHeight / 2))
                    twoLabel.font = UIFont(name: cell.textLabel!.font.fontName, size: 15)
                    twoLabel.textAlignment = NSTextAlignment.Left
                    twoLabel.text = quantity
                    twoLabel.tag = 11
                    cell!.contentView.addSubview(twoLabel)
                    
                    var imageview: UIImageView = UIImageView(frame: CGRectMake(10, 0, imageWidth, imageHeight))
                    imageview.image = image
                    imageview.tag = 12
                    cell!.contentView.addSubview(imageview)
                    
                }else{
                    let label1:UILabel = cell.viewWithTag(10) as! UILabel
                    label1.text = size
                    let label2:UILabel = cell.viewWithTag(11) as! UILabel
                    label2.text = quantity
                    let imageview:UIImageView = cell.viewWithTag(12) as! UIImageView
                    imageview.image = image
                }
            }
        }
        return cell!
    }
    
    /*! Adds or removes a UIDatePicker cell below the given indexPath.
    
    @param indexPath The indexPath to reveal the UIDatePicker.
    */
    func toggleDatePickerForSelectedIndexPath(indexPath: NSIndexPath) {
        
        tableView.beginUpdates()
        
        let indexPaths = [NSIndexPath(forRow: indexPath.row + 1, inSection: 0)]
        
        // check if 'indexPath' has an attached date picker below it
        if hasPickerForIndexPath(indexPath) {
            // found a picker below it, so remove it
            tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Fade)
        } else {
            // didn't find a picker below it, so we should insert it
            tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Fade)
        }
        tableView.endUpdates()
    }
    
    /*! Reveals the date picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
    
    @param indexPath The indexPath to reveal the UIDatePicker.
    */
    func displayInlineDatePickerForRowAtIndexPath(indexPath: NSIndexPath) {
        
        // display the date picker inline with the table content
        tableView.beginUpdates()
        
        var before = false // indicates if the date picker is below "indexPath", help us determine which row to reveal
        if hasInlineDatePicker() {
            before = datePickerIndexPath?.row < indexPath.row
        }
        
        var sameCellClicked = (datePickerIndexPath?.row == indexPath.row + 1)
        
        // remove any date picker cell if it exists
        if self.hasInlineDatePicker() {
            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: datePickerIndexPath!.row, inSection: 0)], withRowAnimation: .Fade)
            datePickerIndexPath = nil
        }
        
        if !sameCellClicked {
            // hide the old date picker and display the new one
            let rowToReveal = (before ? indexPath.row - 1 : indexPath.row)
            let indexPathToReveal = NSIndexPath(forRow: rowToReveal, inSection: 0)
            
            toggleDatePickerForSelectedIndexPath(indexPathToReveal)
            datePickerIndexPath = NSIndexPath(forRow: indexPathToReveal.row + 1, inSection: 0)
        }
        
        // always deselect the row containing the start or end date
        tableView.deselectRowAtIndexPath(indexPath, animated:true)
        
        tableView.endUpdates()
        
        // inform our date picker of the current date to match the current cell
        updateDatePicker()
    }
    
    
    
    // MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.reuseIdentifier == kDateCellID {
            displayInlineDatePickerForRowAtIndexPath(indexPath)
        } else {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    
    
    // MARK: - Actions
    
    /*! User chose to change the date by changing the values inside the UIDatePicker.
    
    @param sender The sender for this action: UIDatePicker.
    */
    
    
    @IBAction func dateAction(sender: UIDatePicker) {
        
        var targetedCellIndexPath: NSIndexPath?
        
        if self.hasInlineDatePicker() {
            // inline date picker: update the cell's date "above" the date picker cell
            //
            targetedCellIndexPath = NSIndexPath(forRow: datePickerIndexPath!.row - 1, inSection: 0)
        } else {
            // external date picker: update the current "selected" cell's date
            targetedCellIndexPath = tableView.indexPathForSelectedRow()!
        }
        
        var cell = tableView.cellForRowAtIndexPath(targetedCellIndexPath!)
        let targetedDatePicker = sender
        
        // update our data model
        var itemData = dateArray[targetedCellIndexPath!.row]
        itemData[kDateKey] = targetedDatePicker.date
        dateArray[targetedCellIndexPath!.row] = itemData
        
        // update the cell's date string
        cell?.detailTextLabel?.text = dateFormatter.stringFromDate(targetedDatePicker.date)
        
        self.checkStarEndDateDescending()
        // If the start date was set is latter than the end date was set then highlight them
    }

    func checkStarEndDateDescending() -> Bool{
        
        let startCellIndexPath = NSIndexPath(forRow: 0, inSection: 0)
        var startCell:UITableViewCell = tableView.cellForRowAtIndexPath(startCellIndexPath!)!
        
        var endCellIndexPath = NSIndexPath(forRow: 1, inSection: 0)
        // If the date picker is appear
        if self.hasInlineDatePicker() {
            let rowNumOfDatePicker = NSIndexPath(forRow: datePickerIndexPath!.row, inSection: 0).row
            if(rowNumOfDatePicker == 1){
                endCellIndexPath = NSIndexPath(forRow: 2, inSection: 0)
            }
            if(rowNumOfDatePicker == 2){
                endCellIndexPath = NSIndexPath(forRow: 1, inSection: 0)
            }

        }
        var endCell:UITableViewCell! = tableView.cellForRowAtIndexPath(endCellIndexPath!)
        
        let compareResult = (dateArray[0][kDateKey] as! NSDate).compare(dateArray[1][kDateKey] as! NSDate)
        if compareResult == NSComparisonResult.OrderedDescending {
            startCell.detailTextLabel?.textColor = UIColor.redColor()
            endCell.detailTextLabel?.textColor = UIColor.redColor()
            return true
        }else{
            startCell.detailTextLabel?.textColor = UIColor.grayColor()
            endCell.detailTextLabel?.textColor = UIColor.grayColor()
            return false
        }
    }
    
    // Check if start&end date/time interval is larger than 15min(else return 1) but less than 10 hours(else return 2)
    func checkStartEndDateIntervale() -> Int{
        let startDate = dateArray[0][kDateKey] as! NSDate
        let endDate = dateArray[1][kDateKey] as! NSDate
        let hourInterval = NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitHour, fromDate: startDate, toDate: endDate, options: nil).hour
        let minInterval =  NSCalendar.currentCalendar().components(NSCalendarUnit.CalendarUnitMinute, fromDate: startDate, toDate: endDate, options: nil).minute
        if(hourInterval >= 10){
            return 2
        }else if(minInterval < 15){
            return 1
        }else{
            return 0
        }
    }

    
    @IBAction func cancelButtonPressed(sender: AnyObject) {
        var alertController = UIAlertController(title: "Note", message: "The current templete will be destroyed, do you want to cancel?", preferredStyle: .Alert)
        // Create the actions
        var noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        var yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default){
            UIAlertAction in
            alertController.dismissViewControllerAnimated(true, completion: nil)
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.navigationController!.presentViewController(alertController, animated: true, completion: nil)
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
