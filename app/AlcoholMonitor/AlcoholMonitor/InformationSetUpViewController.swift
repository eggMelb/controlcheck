//
//  InformationSetUpViewController.swift
//  AlcoholMonitor
//
//  Created by Jack on 25/06/2015.
//  Copyright (c) 2015 Unimelb. All rights reserved.
//

import UIKit

class InformationSetUpViewController: UIViewController,UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet var scrollview: UIScrollView!
    
    @IBOutlet var paticipantNumTextField: UITextField!
    
    @IBOutlet var countryTextField: UITextField!
    
    @IBOutlet var genderSegmented: UISegmentedControl!
    var gender:String = "Male"
    
    @IBOutlet var DOBTextField: UITextField!
    
    @IBOutlet var weightTextField: UITextField!
    
    @IBOutlet var heightTextField: UITextField!
    
    @IBOutlet var accessCodeTextField: UITextField!
    
    var countryPicker: UIPickerView! = UIPickerView()
    var datePicker  : UIDatePicker = UIDatePicker()
    
    var activeField: UITextField?
    var countries:Array<String>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initlisePickerViews()
        
        paticipantNumTextField.delegate = self
        countryTextField.delegate = self
        DOBTextField.delegate = self
        weightTextField.delegate = self
        heightTextField.delegate = self
        accessCodeTextField.delegate = self
        
        var tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "DismissKeyboardOrPicker")
        view.addGestureRecognizer(tap)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let center = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardOnScreen:", name: UIKeyboardDidShowNotification, object: nil)
        center.addObserver(self, selector: "keyboardOffScreen:", name: UIKeyboardDidHideNotification, object: nil)
        
    }
    
    func initlisePickerViews(){
        // Country picker
        countryPicker.delegate = self
        countryPicker.dataSource = self
        countryPicker.backgroundColor = UIColor.whiteColor()
        countryTextField.inputView = countryPicker
        var countries: [String] = []
        for code in NSLocale.ISOCountryCodes() as! [String] {
            let id = NSLocale.localeIdentifierFromComponents([NSLocaleCountryCode: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayNameForKey(NSLocaleIdentifier, value: id) ?? "Country not found for code: \(code)"
            countries.append(name)
        }
        self.countries = countries
        
        // Date Picker
        datePicker.datePickerMode = UIDatePickerMode.Date
        datePicker.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        datePicker.backgroundColor = UIColor.whiteColor()
        DOBTextField.inputView = datePicker
    }
    

    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return countries.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return countries[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        countryTextField.text = countries[row]
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {

        return true
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        var dateformatter = NSDateFormatter()
        dateformatter.dateFormat = "dd/MM/yyyy"
        DOBTextField.text = dateformatter.stringFromDate(sender.date)
    }
    
    
    // Hide the Keyboard when press 'return' on the keyboard
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Hide the keyboard/Picker when user touches somewhere else
    func DismissKeyboardOrPicker(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(textField: UITextField){
        activeField = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField){
        activeField = nil
    }
    
    func keyboardOnScreen(notification: NSNotification){
        let info: NSDictionary  = notification.userInfo!
        let kbSize = info.valueForKey(UIKeyboardFrameEndUserInfoKey)?.CGRectValue().size
        
        let contentInsets:UIEdgeInsets  = UIEdgeInsetsMake(0.0, 0.0, kbSize!.height, 0.0)
        scrollview.contentInset = contentInsets
        scrollview.scrollIndicatorInsets = contentInsets
        var aRect: CGRect = self.view.frame
        aRect.size.height -= kbSize!.height
        
        //see if the active field is already visible
        if (!CGRectContainsPoint(aRect, activeField!.frame.origin) ) {
            let scrollPoint:CGPoint = CGPointMake(0.0, activeField!.frame.origin.y - kbSize!.height)
            scrollview.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    func keyboardOffScreen(notification: NSNotification){
        scrollview.setContentOffset(CGPoint(x: 0,y: 0),animated:true)
    }
    
    @IBAction func changeGender(sender: AnyObject) {
        switch genderSegmented.selectedSegmentIndex
        {
        case 0:
            self.gender = "Male";
        case 1:
            self.gender = "Female";
        default:
            break; 
        }
    }
    
    @IBAction func comfirmButtonPressed(sender: AnyObject) {
        if(self.checkCompleteness()){
            
            // See if user input a validate access code
            if(!validateAccessCode()){
                let warning = UIAlertView(title: "Incorrect", message: "Invalid Access Code", delegate: self, cancelButtonTitle: "Ok")
                warning.show()
                return
            }
    
            var alertController = UIAlertController(title: "Note", message: "Once comfirmed, your personal information cannot be changed", preferredStyle: .Alert)
            // Create the actions
            var comfirmAction = UIAlertAction(title: "Comfirm", style: UIAlertActionStyle.Default) {
                UIAlertAction in
                alertController.dismissViewControllerAnimated(true, completion: nil)
                self.savePersonalInformation()
                self.performSegueWithIdentifier("completeInformation",sender: self)
                NSLog("comfirm Pressed")
            }
            var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default){
                UIAlertAction in
                alertController.dismissViewControllerAnimated(true, completion: nil)
                NSLog("cancel Pressed")
            }
            alertController.addAction(comfirmAction)
            alertController.addAction(cancelAction)
            self.navigationController!.presentViewController(alertController, animated: true, completion: nil)
        }else{
            self.showIncompletenessWarning()
        }
        
    }
    
    func checkCompleteness() -> Bool{
        let textFields = [paticipantNumTextField,countryTextField,DOBTextField,weightTextField,heightTextField,accessCodeTextField]
        for textField in textFields{
            if(textField.text! == ""){
                return false
            }
            print(textField.text + "\n")
        }
        return true
    }
    
    func savePersonalInformation(){
        let userDefault = NSUserDefaults.standardUserDefaults()
        if(userDefault.objectForKey("userInfo") != nil){
            userDefault.removeObjectForKey("userInfo")
        }else{
            var userInfoDic:Dictionary<String,String>! = Dictionary()
            userInfoDic["paticipantID"] = paticipantNumTextField.text!
            userInfoDic["country"] = countryTextField.text!
            userInfoDic["gender"] = gender
            userInfoDic["dateOfBirth"] = countryTextField.text!
            userInfoDic["weight"] = weightTextField.text!
            userInfoDic["height"] = heightTextField.text!
            userInfoDic["accessCode"] = accessCodeTextField.text!
            
            userDefault.setValue(userInfoDic, forKey: "userInfo")
        }
    }
    
    
    func showIncompletenessWarning(){
        let warning = UIAlertView(title: "Note", message: "You have to complete all the fields", delegate: self, cancelButtonTitle: "Ok")
        warning.show()
    }
    
    func validateAccessCode() -> Bool{
        let correctAccessCode = String(format: "%i@cp#ss", ((self.paticipantNumTextField.text!.toInt()! + 8) * 3 + 6))
        if(correctAccessCode == self.accessCodeTextField.text!){
            return true
        }else{
            return false
        }
    }
    

    
}
