//
//  DrinkingDetailTableViewController.swift
//  AlcoholMonitor
//
//  Created by Jack on 27/06/2015.
//  Copyright (c) 2015 Unimelb. All rights reserved.
//

import UIKit

class DrinkingDetailTableViewController: UITableViewController, PopUpPickerViewDelegate {

    let cellList:[(identifier: String, title: String)] = [("drinkTypeCell","Drinks Type"),("sizeCell","Size"),("quantityCell","Quantity"),("alcoholContentCell","Alcohol Content")]
    
    var choosedData:Dictionary<String,String> = ["drinkTypeCell":"unknown","sizeCell":"unknown","quantityCell":"unknown","alcoholContentCell":"unknown"]
    
    var pickerView: PopUpPickerView!
    var pickerDataArray:Array<String> = Array<String>()
    var sizeSortedArray:Array<String> = Array<String>()
    var quantityArray:Array<String> = Array<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        
        pickerView = PopUpPickerView()
        pickerView.delegate = self
        if let window = UIApplication.sharedApplication().keyWindow {
            window.addSubview(pickerView)
        } else {
            self.view.addSubview(pickerView)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        if (userDefaults.objectForKey("tempCurrentDrinkSubmission") != nil){
            let currentChoices = userDefaults.objectForKey("tempCurrentDrinkSubmission") as! Dictionary<String,String>
            println(currentChoices)
            for key in currentChoices.keys{
                choosedData[key] = currentChoices[key]
            }
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 4
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let kCellIdentifier = cellList[indexPath.row].identifier
        let title = cellList[indexPath.row].title
        var cell: UITableViewCell! = tableView.dequeueReusableCellWithIdentifier(kCellIdentifier) as! UITableViewCell
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: kCellIdentifier)
        }
        cell.textLabel!.text = title
        cell.detailTextLabel!.text = choosedData[kCellIdentifier]
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        // If choose first row
        if(indexPath.row == 0){
            return
        }
        // If choose other row but drink type haven't set
        if(choosedData["drinkTypeCell"] == "unknown"){
            let warning = UIAlertView(title: "Note", message: "Please Choose Drink Types First", delegate: self, cancelButtonTitle: "Ok")
            warning.show()
            // De-Gray-out the selected row
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            return
        }
        let pathForDrinksPropertyDic = NSBundle.mainBundle().pathForResource("DrinksPropertyDic", ofType: "plist")
        let drinksPropertyDic = NSDictionary(contentsOfFile: pathForDrinksPropertyDic!)
        let currenyDrinkPorperty = drinksPropertyDic?.objectForKey(choosedData["drinkTypeCell"]!) as! NSDictionary
        // If select the row Size
        if(indexPath.row == 1){
            if(choosedData["drinkTypeCell"] == "Cider/Premix Spirit"){
                sizeSortedArray = ["Bottle (300ml)","Bottle (330ml)","Can (375ml)","Bottle (500ml)"]
            }else{
                let sizeDic = currenyDrinkPorperty.objectForKey("Sizes") as! Dictionary<String,Int>
                sizeSortedArray = self.getSortedArrayFromDictionary(sizeDic)
            }
            pickerDataArray = sizeSortedArray
            pickerView.showPicker()
        }
        // If select the row Quantity
        else if(indexPath.row == 2){
            var quantityArray = Array<String>()
            for i in 1...30 {
                quantityArray.append(String(i))
            }
            self.quantityArray = quantityArray
            pickerDataArray = quantityArray
            pickerView.showPicker()
        }else{
            // De-Gray-out the selected row
            self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
        
    }
    
    // for delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return pickerDataArray[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelect numbers: [Int]) {
        let selectedIndexPath = self.tableView.indexPathForSelectedRow()
        let selectedRow = selectedIndexPath!.row
        
        // If this is a picker for size selecting
        if(selectedRow == 1){
            choosedData["sizeCell"] = sizeSortedArray[numbers[0]]
        }
        // If this is a picker for quantity selecting
        if(selectedRow == 2){
            choosedData["quantityCell"] = quantityArray[numbers[0]]
        }
        self.tableView.reloadData()
    }
    
    func getSortedArrayFromDictionary(dic:Dictionary<String,Int>) -> Array<String>{
        var tempArray = Array<String>()
        var copyDic = Dictionary<String,Int>()
        var reversedDic = Dictionary<Int,String>()
        for key in dic.keys{
            copyDic[key] = dic[key]
            reversedDic[dic[key]!] = key
        }
        let count = copyDic.count
        for var i = 0; i < count; i++ {
            var value = minElement(copyDic.values)
            var name = reversedDic[value]
            copyDic.removeValueForKey(name!)
            tempArray.append(name! + " (" + String(value) + "ml)")
        }
        return tempArray
    }
    
    
    @IBAction func pressedDoneButton(sender: AnyObject) {
        // Check if all parameters are set
        for value in choosedData.values{
            if value == "unknown" {
                let warning = UIAlertView(title: "Note", message: "Unfinished data field", delegate: self, cancelButtonTitle: "Ok")
                warning.show()
                return
            }
        }
        
        //Else all parameter are set
        var newArray = NSMutableArray()
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if((userDefaults.objectForKey("drinksSubmission")) != nil){
            let originalArray = userDefaults.objectForKey("drinksSubmission") as! NSArray
            for element in originalArray{
                newArray.addObject(element)
            }
            newArray.addObject(choosedData)
            userDefaults.removeObjectForKey("drinksSubmission")
            userDefaults.setObject(newArray, forKey: "drinksSubmission")
        }else{
            newArray.addObject(choosedData)
            userDefaults.removeObjectForKey("drinksSubmission")
            userDefaults.setObject(newArray, forKey: "drinksSubmission")
        }
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    func checkAllParametersAreUnset() -> Bool{
        for value in choosedData.values {
            if value != "unknown"{
                return false
            }
        }
        return true
    }
    
    @IBAction func pressedCancelButton(sender: AnyObject) {

        if(self.checkAllParametersAreUnset()){
            self.navigationController?.popViewControllerAnimated(true)
            return
        }
        var alertController = UIAlertController(title: "Note", message: "All the filled data will be reset, do you want to cancel?", preferredStyle: .Alert)
        // Create the actions
        var noAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        var yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default){
            UIAlertAction in
            alertController.dismissViewControllerAnimated(true, completion: nil)
            self.navigationController?.popViewControllerAnimated(true)
        }
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.navigationController!.presentViewController(alertController, animated: true, completion: nil)
    }
    

}
