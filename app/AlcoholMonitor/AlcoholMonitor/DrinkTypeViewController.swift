//
//  DrinkTypeViewController.swift
//  AlcoholMonitor
//
//  Created by Jack on 27/06/2015.
//  Copyright (c) 2015 Unimelb. All rights reserved.
//

import UIKit

class DrinkTypeViewController: UIViewController {

    let screenWidth = UIScreen.mainScreen().bounds.width
    let screenHeight = UIScreen.mainScreen().bounds.height - 49
    let startHight:CGFloat = 64

    
    var buttons:Array<UIButton>! = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true;
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(animated: Bool) {
        self.view.backgroundColor = UIColor.blackColor()
        self.initializeButtons()
    }
    
    func initializeButtons(){
        let gap:CGFloat = 2
        let buttonWidth = (screenWidth - gap * 2) / 3
        let buttonHeight = (screenHeight - startHight - gap * 2) / 3
        let pathForDrinksPropertyList = NSBundle.mainBundle().pathForResource("DrinksPropertyList", ofType: "plist")
        let drinksPropertyArray = NSArray(contentsOfFile: pathForDrinksPropertyList!)
        
        for(var i = 0; i < 9; i++){
            var button = UIButton(frame: CGRectMake(CGFloat(i) % 3 * (buttonWidth + gap), startHight + floor(CGFloat(i) / 3) * (buttonHeight + gap),buttonWidth, buttonHeight))
            button.backgroundColor = UIColor.blackColor()
            
            let currentDrinkProperty = drinksPropertyArray?.objectAtIndex(i) as! NSDictionary
            let image = UIImage(named: currentDrinkProperty["ImageName"] as! String)
            let drinkType = currentDrinkProperty["DrinkType"] as! String
            
            button.backgroundColor = UIColor.grayColor()
            button.setImage(image, forState: .Normal)
            button.setTitle(drinkType, forState: UIControlState.Normal)
            button.setTitleColor(self.view.tintColor, forState: UIControlState.Normal)
            button.setTitleColor(UIColor.grayColor(), forState: UIControlState.Highlighted)
            button.titleLabel!.font = UIFont(name: button.titleLabel!.font.fontName , size: 14)
            button.backgroundColor = UIColor.whiteColor()

            let spacing:CGFloat = 13.0;
            
            // lower the text and push it left so it appears centered
            //  below the image
            let imageSize:CGSize = button.imageView!.image!.size;
            button.titleEdgeInsets = UIEdgeInsetsMake(
                0.0, -imageSize.width, -(imageSize.height + spacing), 0.0);
            
            // raise the image and push it right so it appears centered
            //  above the text
            
            let titleSize:CGSize = button.titleLabel!.text!.sizeWithAttributes([NSFontAttributeName: button.titleLabel!.font])
            button.imageEdgeInsets = UIEdgeInsetsMake( -(titleSize.height + spacing), 0.0, 0.0, -titleSize.width);
            button.titleLabel!.numberOfLines = 2;
            // button.titleLabel!.lineBreakMode = NSLineBreakByWordWrapping;
            button.titleLabel!.textAlignment = NSTextAlignment.Center;
            
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = i;
            self.view.addSubview(button)
            buttons.append(button)
        }
    }
    
    func buttonAction(sender:UIButton!)
    {
        var buttonSendTag:UIButton = sender
        
        let pathForDrinksPropertyList = NSBundle.mainBundle().pathForResource("DrinksPropertyList", ofType: "plist")
        let drinksPropertyArray = NSArray(contentsOfFile: pathForDrinksPropertyList!)
        let currentDrinkProperty = drinksPropertyArray?.objectAtIndex(buttonSendTag.tag) as! NSDictionary
        
        let drinkType = currentDrinkProperty["DrinkType"] as! String
        let alcoholContent = currentDrinkProperty["AlcoholPercentage"]!.stringValue as String
        
        var tempSubmissionDic = NSMutableDictionary()
        tempSubmissionDic.setObject(drinkType, forKey: "drinkTypeCell")
        tempSubmissionDic.setObject(alcoholContent, forKey: "alcoholContentCell")
        tempSubmissionDic.setObject("unknown", forKey: "sizeCell")
        tempSubmissionDic.setObject("unknown", forKey: "quantityCell")
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        if(userDefaults.objectForKey("tempCurrentDrinkSubmission") == nil){
            userDefaults.setObject(tempSubmissionDic, forKey: "tempCurrentDrinkSubmission")
        }else{
            let oldTempSubmission = userDefaults.objectForKey("tempCurrentDrinkSubmission") as! NSDictionary
            // If user choose a button but is different from the one he pressed previously
            if((oldTempSubmission.objectForKey("drinkTypeCell") as! String) != drinkType){
                userDefaults.removeObjectForKey("tempCurrentDrinkSubmission")
                userDefaults.setObject(tempSubmissionDic, forKey: "tempCurrentDrinkSubmission")
            }
        }
        self.navigationController?.popViewControllerAnimated(true)
        NSLog("User pick drink type: " + drinkType)
    }
    
        
        
    @IBAction func backToPreviousViewController(sender: AnyObject) {
        
    }
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

}
