//
//  DrinkTableViewController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 20/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "DrinkTableViewController.h"
#import "DataCollectionViewController.h"
#import "AppDelegate.h"

@interface DrinkTableViewController ()

@end

@implementation DrinkTableViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self fixNavigationBarCorruption];
    
    self.datePicker.minimumDate = [self getRoundedDate:[[NSDate date] dateByAddingTimeInterval:-2*24*60*60]]; //Minimum Date
    self.datePicker.maximumDate = [self getRoundedDate:[[NSDate date] dateByAddingTimeInterval:+1*1*15*60]]; //Maximum Date
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self.tableView reloadData];
    if ([self.tableView numberOfRowsInSection:0] == 1) {
        [self.navigationItem.leftBarButtonItem setEnabled:NO];
        [self setEditing:NO animated:YES];
        
        [self.navigationItem.rightBarButtonItem setTitle:@"Cancel"];
    }else {
        [self.navigationItem.leftBarButtonItem setEnabled:YES];
        
        [self.navigationItem.rightBarButtonItem setTitle:@"Submit"];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self performSelector:@selector(noDrinkCheck) withObject:nil afterDelay:0.8];
    self.tableView.userInteractionEnabled = NO;
    
    if (!self.drinksArray) {
        self.drinksArray = [[NSMutableArray alloc] initWithCapacity:100];
    }
    
    dismissToolbarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.navigationBar.frame.size.width, self.navigationController.navigationBar.frame.size.height+20)];
    dismissToolbarView.backgroundColor = [UIColor darkGrayColor];
    dismissToolbarView.alpha = 0.0f;
    dismissToolbarView.userInteractionEnabled = YES;
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [appDelegate.window addSubview:dismissToolbarView];
    
    dismissView = [[UIView alloc] initWithFrame:self.view.frame];
    dismissView.backgroundColor = [UIColor darkGrayColor];
    dismissView.alpha = 0.0f;
    dismissView.userInteractionEnabled = YES;
    [self.view addSubview:dismissView];
    
    UITapGestureRecognizer *taptap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView2:)];
    [dismissView addGestureRecognizer:taptap];
    
    taptap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView2:)];
    [dismissToolbarView addGestureRecognizer:taptap];
    
    UIBarButtonItem *submitBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitDrink:)];
    self.navigationItem.rightBarButtonItem = submitBarButtonItem;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView:)];
    [self.tableView addGestureRecognizer:tap];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, [self trueHeight], self.view.frame.size.width, 162)];
    [self.datePicker addTarget:self action:@selector(datePickerValueDidChange:) forControlEvents:UIControlEventValueChanged];
    self.datePicker.date = [self getRoundedDate:[NSDate date]];
    
    self.datePicker.minimumDate = [self getRoundedDate:[[NSDate date] dateByAddingTimeInterval:-2*24*60*60]];//Minimum Date
    self.datePicker.maximumDate = [self getRoundedDate:[[NSDate date] dateByAddingTimeInterval:+1*1*15*60]];//Maximum Date
    self.datePicker.hidden = YES;
    
    self.datePicker.backgroundColor = [UIColor whiteColor];
    
    self.datePicker.minuteInterval = 15;
    
    [self.view addSubview:self.datePicker];
    
    CLSNSLog(@"contentSizeInitial: %@", NSStringFromCGSize(self.tableView.contentSize));
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recalculatePositions:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GUI Event Handlers

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (self.datePicker.hidden == FALSE) {
        self.currentSelectedIndexPath = nil;
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:NO];
        [self hideDatePicker:NO];
    }
}

- (void) didTapOnTableView2:(UIGestureRecognizer*) recognizer {
    self.currentSelectedIndexPath = nil;
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    [self hideDatePicker:YES];
}

- (void) didTapOnTableView:(UIGestureRecognizer*) recognizer {
    CGPoint tapLocation = [recognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapLocation];
    
    if (indexPath) {
        recognizer.cancelsTouchesInView = NO;
    } else {
        self.currentSelectedIndexPath = nil;
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
        [self hideDatePicker:YES];
    }
}

- (void)datePickerValueDidChange:(UIDatePicker *)sender {
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
    
    if ([self.tableView indexPathForSelectedRow].row == 0) {
        self.currentStartDate = self.datePicker.date;
    }else if ([self.tableView indexPathForSelectedRow].row == 1) {
        self.currentEndDate = self.datePicker.date;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE dd MMM hh:mm a"];
    
    selectedCell.detailTextLabel.text = [dateFormatter stringFromDate:sender.date];
    
    UITableViewCell *startedTableViewCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    UITableViewCell *finishedTableViewCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
    
    NSDate *startDate = [dateFormatter dateFromString:startedTableViewCell.detailTextLabel.text];
    NSDate *endDate = [dateFormatter dateFromString:finishedTableViewCell.detailTextLabel.text];
    
    CLSNSLog(@"comparing dates: %@         %@", startDate, endDate);
    
    if( [startDate timeIntervalSinceDate:endDate] > 0 ) {
        startedTableViewCell.textLabel.textColor = [UIColor redColor];
        startedTableViewCell.detailTextLabel.textColor = [UIColor redColor];
        
        finishedTableViewCell.textLabel.textColor = [UIColor redColor];
        finishedTableViewCell.detailTextLabel.textColor = [UIColor redColor];
    }else {
        startedTableViewCell.textLabel.textColor = [UIColor blackColor];
        startedTableViewCell.detailTextLabel.textColor = [UIColor grayColor];
        
        finishedTableViewCell.textLabel.textColor = [UIColor blackColor];
        finishedTableViewCell.detailTextLabel.textColor = [UIColor grayColor];
    }
}

- (void)noDrinkCheck {
    BOOL noDrinks = TRUE;
    if (noDrinks) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0]-1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0]-1 inSection:0]];
        
    }
    self.tableView.userInteractionEnabled = YES;
}

- (void)submitDrink:(UIBarButtonItem *)sender {
    if ([sender.title isEqualToString:@"Cancel"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning" message:NSLocalizedString(@"DRINK_SUBMISSION_CANCEL_MESSAGE", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 31432;
        [alertView show];
    }else {
        UITableViewCell *startedTableViewCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        UITableViewCell *finishedTableViewCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
        if (startedTableViewCell.detailTextLabel.textColor == [UIColor redColor] || finishedTableViewCell.detailTextLabel.textColor == [UIColor redColor]) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"DRINK_SUBMISSION_TIMESTAMPS_CONFLICT_MESSAGE", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            alertView.tag = 4012;
            [alertView show];
        }else {
            if ([startedTableViewCell.detailTextLabel.text isEqualToString:@"(unfilled)"] || [finishedTableViewCell.detailTextLabel.text isEqualToString:@"(unfilled)"]) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Incomplete" message:NSLocalizedString(@"DRINK_SUBMISSION_TIMESTAMPS_BLANK_FIELDS_MESSAGE", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                alertView.tag = 4014;
                [alertView show];
            }else {
                if (self.currentStartDate == self.currentEndDate) {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"DRINK_SUBMISSION_TIMESTAMPS_EQUAL_MESSAGE", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    alertView.tag = 4015;
                    [alertView show];
                }else {
                    if (floor([self.currentStartDate timeIntervalSinceReferenceDate]) > floor([self.currentEndDate timeIntervalSinceReferenceDate])-(14*60)) {//Ensures start and end timestamps are at least 15 minutes apart
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"DRINK_SUBMISSION_TIMESTAMPS_MINIMUM_SESSION_DURATION_MESSAGE", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        alertView.tag = 4016;
                        [alertView show];
                    }else {
                        if (floor([self.currentStartDate timeIntervalSinceReferenceDate]) < floor([self.currentEndDate timeIntervalSinceReferenceDate])-(10*60*60)) {//Ensures sessions are not greater than 10 hours
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:NSLocalizedString(@"DRINK_SUBMISSION_TIMESTAMPS_MAXIMUM_SESSION_DURATION_MESSAGE", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            alertView.tag = 4017;
                            [alertView show];
                        }else {
                            [self.navigationController setNavigationBarHidden:YES animated:YES];
                            
                            [(HomeViewController *)[[(UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController viewControllers] objectAtIndex:0] showDismissLabel];
                            CLSNSLog(@"CLASS: %@", NSStringFromClass([[[(UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController viewControllers] objectAtIndex:0] class]));
                            
                            [self.navigationController popToRootViewControllerAnimated:YES];
                            
                            [self submitSessionData];
                        }
                    }
                }
            }
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        if (self.drinksArray) {
            return self.drinksArray.count + 1;
        }
        return 1;
    }
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CLSNSLog(@"contentSize (cellForRowAtIndexPath): %@", NSStringFromCGSize(self.tableView.contentSize));
    CLSNSLog(@"[self trueHeight]: %f", [self trueHeight]);
    
    NSInteger heightOffset = self.tableView.contentSize.height;
    if (heightOffset < [self trueHeight]) heightOffset = [self trueHeight];
    self.datePicker.frame = CGRectMake(0, heightOffset, self.datePicker.frame.size.width, self.datePicker.frame.size.height);
    
    if (indexPath.section == 0) {
        if (indexPath.row == self.drinksArray.count + 0 || (!self.drinksArray && indexPath.row == 0)) {
            UITableViewCell *addCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"add"];
            addCell.textLabel.text = @"New Drink";
            addCell.detailTextLabel.text = @"Tap to add";
            addCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
            return addCell;
        }
        
        
        UITableViewCell *drinkCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"drink"];
        Drink *drink = [self.drinksArray objectAtIndex:indexPath.row];
        drinkCell.textLabel.text = [NSString stringWithFormat:@"%@", drink.type];
        drinkCell.detailTextLabel.text = [NSString stringWithFormat:@"Size: %li mL  |  Alcohol: %@  |  x%li", (long)drink.size, drink.alcoholPercentage, (long)drink.quantity];
        
        drinkCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return drinkCell;
    }else {
        UITableViewCell *timestampCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"timestampCell"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE dd MMM hh:mm a"];
        
        if (indexPath.row == 0) {
            timestampCell.textLabel.text = @"Start Time";
            if (self.currentStartDate == nil) timestampCell.detailTextLabel.text = @"(unfilled)";
            else timestampCell.detailTextLabel.text = [dateFormatter stringFromDate:self.currentStartDate];
        }else {
            timestampCell.textLabel.text = @"End Time";
            if (self.currentEndDate == nil) timestampCell.detailTextLabel.text = @"(unfilled)";
            else timestampCell.detailTextLabel.text = [dateFormatter stringFromDate:self.currentEndDate];
        }
        
        if( [self.currentStartDate timeIntervalSinceDate:self.currentEndDate] > 0 ) {
            timestampCell.textLabel.textColor = [UIColor redColor];
            timestampCell.detailTextLabel.textColor = [UIColor redColor];
        }else {
            timestampCell.textLabel.textColor = [UIColor blackColor];
            timestampCell.detailTextLabel.textColor = [UIColor grayColor];
        }
        
        timestampCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return timestampCell;
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    if (indexPath.section == 0) {
        if ( (indexPath.row == 0 && !self.drinksArray) ||  (indexPath.row == self.drinksArray.count && self.drinksArray) ) return NO;
        return YES;
    }
    return NO;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.drinksArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        if ([self.tableView numberOfRowsInSection:0] == 1) {
            [self.navigationItem.leftBarButtonItem setEnabled:NO];
            [self setEditing:NO animated:YES];
            
            [self.navigationItem.rightBarButtonItem setTitle:@"Cancel"];
        }else {
            [self.navigationItem.leftBarButtonItem setEnabled:YES];
            
            [self.navigationItem.rightBarButtonItem setTitle:@"Submit"];
        }
        [self.tableView reloadData];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CLSNSLog(@"didSelectRowAtIndexPath");
    //    self.datePicker.backgroundColor = [UIColor lightGrayColor];
    [self.view bringSubviewToFront:self.datePicker];
    
    if (indexPath.section == 0) {
        self.currentSelectedIndexPath = nil;
        [self hideDatePicker:YES];
        
        if ( (indexPath.row == 0 && !self.drinksArray) ||  (indexPath.row == self.drinksArray.count && self.drinksArray) ) {
            [self performSegueWithIdentifier:@"showDrinkPropertiesTableViewController" sender:nil];
            return;
        }
        
        [self performSegueWithIdentifier:@"showDrinkPropertiesTableViewController" sender:[self.drinksArray objectAtIndex:indexPath.row]];
    }else if (indexPath.section == 1) {
        if (indexPath == [self.tableView indexPathForSelectedRow] && indexPath == self.currentSelectedIndexPath) {
            self.currentSelectedIndexPath = nil;
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [self hideDatePicker:YES];
            
            return;
        }
        self.currentSelectedIndexPath = indexPath;
        
        UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE dd MMM hh:mm a"];
        
        CLSNSLog(@"selectedCell.detailTextLabel.text: %@", selectedCell.detailTextLabel.text);
        CLSNSLog(@"[dateFormatter stringFromDate:self.datePicker.date]: %@", [dateFormatter stringFromDate:self.datePicker.date]);
        
        if ([selectedCell.detailTextLabel.text isEqualToString:@"(unfilled)"]) {
            selectedCell.detailTextLabel.text = [dateFormatter stringFromDate:[self getRoundedDate:[NSDate date]]];
            if (indexPath.row == 0) {
                self.currentStartDate = [self getRoundedDate:[NSDate date]];
                self.datePicker.date = self.currentStartDate;
            }else if (indexPath.row == 1) {
                self.currentEndDate = [self getRoundedDate:[NSDate date]];
                self.datePicker.date = self.currentEndDate;
            }
        }else {
            if (indexPath.row == 0) {
                self.datePicker.date = self.currentStartDate;
            }else if (indexPath.row == 1) {
                self.datePicker.date = self.currentEndDate;
            }
        }
        
        [self showDatePicker];
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    CLSNSLog(@"Editing %i", editing);
    [super setEditing:editing animated:animated];
    
    self.currentSelectedIndexPath = nil;
    [self hideDatePicker:YES];
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    CLSNSLog(@"DrinkTableViewController prepareForSegue");
    
    if ([NSStringFromClass([[segue destinationViewController] class]) isEqualToString:@"DrinkPropertiesTableViewController"]) {
        [[segue destinationViewController] setDrink:sender];
        CLSNSLog(@"TTT selectedRow: %li", (long)[self.tableView indexPathForSelectedRow].row);
        [[segue destinationViewController] setIsAddingNewDrink:([self.tableView indexPathForSelectedRow].row == 0 && !self.drinksArray) ||  ([self.tableView indexPathForSelectedRow].row == self.drinksArray.count && self.drinksArray)];
    }
    
    
    CLSNSLog(@"class1: %@", NSStringFromClass([[segue destinationViewController] class]));
    CLSNSLog(@"class2: %@", NSStringFromClass([sender class]));
    if ([NSStringFromClass([[segue destinationViewController] class]) isEqualToString:@"DrinkMenuViewController"]) {
        ((DrinkMenuViewController *)[segue destinationViewController]).isAutomated = [NSStringFromClass([sender class]) isEqualToString:@"DrinkPropertiesTableViewController"];
    }
}

#pragma mark - Show/Hide Date Picker Methods

- (void)showDatePicker {
    self.datePicker.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        dismissView.alpha = 0.5f;
        dismissToolbarView.alpha = 0.5f;
    }];
    
    NSInteger initialOffset = -self.tableView.contentInset.top;
    
    NSInteger contentHeightSize = self.tableView.contentSize.height;
    if (contentHeightSize < [self trueHeight]) {
        //Normal
        contentHeightSize = [self trueHeight]-216;
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]+216) animated:YES];
        
        NSInteger heightOffset = self.tableView.contentSize.height;
        if (heightOffset < [self trueHeight]) heightOffset = [self trueHeight];
        
        [UIView animateWithDuration:0.3 animations:^ {
            self.datePicker.frame = (CGRect){0, heightOffset-216, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
        }];
    }else {
        //Extended
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]+216) animated:YES];
    }
}

- (void)hideDatePicker:(BOOL)animated {
    NSInteger initialOffset = -self.tableView.contentInset.top;
    
    [UIView animateWithDuration:0.3 animations:^{
        dismissView.alpha = 0.0f;
        dismissToolbarView.alpha = 0.0f;
    }];
    
    NSInteger contentHeightSize = self.tableView.contentSize.height;
    if (contentHeightSize < [self trueHeight]) {
        //Normal
        contentHeightSize = [self trueHeight];
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]) animated:YES];
        
        NSInteger heightOffset = self.tableView.contentSize.height;
        if (heightOffset < [self trueHeight]) heightOffset = [self trueHeight];
        
        [UIView animateWithDuration:0.3 animations:^ {
            self.datePicker.frame = (CGRect){0, heightOffset, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
        } completion:^(BOOL completion) {
            self.datePicker.hidden = YES;
        }];
    }else {
        //Extended
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]) animated:NO];
        self.datePicker.hidden = YES;
    }
}

#pragma mark - Data Submission Methods

- (void)submitSessionData {
    [[UserDataProcessingEngine sharedEngine] loadBaseDictionary];
    
    NSDate *startDate = self.currentStartDate;
    NSDate *endDate = self.currentEndDate;
    
    float totalDrinkingTimeInMinutes = floorf([endDate timeIntervalSince1970] / 60.0f) - floorf([startDate timeIntervalSince1970] / 60.0f);
    float totalDrinkingTimeInHours = (int)totalDrinkingTimeInMinutes / 60.0f;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *startTime = [dateFormatter stringFromDate:startDate];
    NSString *endTime = [dateFormatter stringFromDate:endDate];
    
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    NSString *sessionStartDate = [dateFormatter stringFromDate:startDate];
    NSString *sessionEndDate = [dateFormatter stringFromDate:endDate];
    
    NSString *submissionDateString = [dateFormatter stringFromDate:[NSDate date]];
    
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *submissionTimeString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSMutableDictionary *sessionDictionary = [[NSMutableDictionary alloc] initWithCapacity:7];
    [sessionDictionary setObject:submissionDateString forKey:@"Submission Date"];
    [sessionDictionary setObject:submissionTimeString forKey:@"Submission Time"];
    [sessionDictionary setObject:sessionStartDate forKey:@"Session Start Date"];
    [sessionDictionary setObject:sessionEndDate forKey:@"Session End Date"];
    [sessionDictionary setObject:startTime forKey:@"Time: Commence Drinking"];
    [sessionDictionary setObject:endTime forKey:@"Time: Finish Drinking"];
    [sessionDictionary setObject:[NSString stringWithFormat:@"%1.0f", totalDrinkingTimeInMinutes] forKey:@"Total Drinking Time (minutes)"];
    [sessionDictionary setObject:[NSString stringWithFormat:@"%1.2f", totalDrinkingTimeInHours] forKey:@"Total Drinking Time (hours)"];
    
    int totalDrinks = 0;
    for (int index = 0; index < self.drinksArray.count; index++) {
        Drink *currentDrink = [self.drinksArray objectAtIndex:index];
        
        totalDrinks += currentDrink.quantity;
    }
    [sessionDictionary setObject:[NSString stringWithFormat:@"%i", totalDrinks] forKey:@"Total Drinks"];
    
    NSMutableDictionary *numberOfDrinksDictionary = [[NSMutableDictionary alloc] initWithCapacity:[[DrinkDataRetrievalEngine sharedEngine] drinkTypes].count];
    for (int index = 0; index < [[DrinkDataRetrievalEngine sharedEngine] drinkTypes].count; index++) {
        NSString *currentDrinkType = [NSString stringWithFormat:@"%@", [[[DrinkDataRetrievalEngine sharedEngine] drinkTypes] objectAtIndex:index]];
        NSArray *currentDescriptionSizeArray = [[DrinkDataRetrievalEngine sharedEngine] drinkDescriptionsForDrinkType:currentDrinkType];
        
        for (int sizeIndex = 0; sizeIndex < currentDescriptionSizeArray.count; sizeIndex++) {
            NSString *currentSizeDescription = [NSString stringWithFormat:@"%@", [currentDescriptionSizeArray objectAtIndex:sizeIndex]];
            
            NSInteger totalQuantity = 0;
            for (int subIndex = 0; subIndex < self.drinksArray.count; subIndex++) {
                Drink *currentDrink = [self.drinksArray objectAtIndex:subIndex];
                NSString *currentDrinkSizeDescription = [[DrinkDataRetrievalEngine sharedEngine] drinkDescriptionForDrinkType:currentDrink.type andSize:[NSString stringWithFormat:@"%li", (long)currentDrink.size]];
                
                if ([currentDrinkSizeDescription isEqualToString:currentSizeDescription]) {
                    totalQuantity += currentDrink.quantity;
                }
            }
            
            [numberOfDrinksDictionary setObject:[NSString stringWithFormat:@"%li", (long)totalQuantity] forKey:currentSizeDescription];
        }
    }
    
    CLSNSLog(@"data to be saved: %@        %@      %@", numberOfDrinksDictionary, [[UserDataProcessingEngine sharedEngine] userData], sessionDictionary);
    
    [[UserDataProcessingEngine sharedEngine] saveDataWithNumberOfDrinks:numberOfDrinksDictionary andUserData:[[UserDataProcessingEngine sharedEngine] userData] andSessionData:sessionDictionary];
    
    [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)submitCancelData {
    [((DataCollectionViewController *)[self.navigationController.viewControllers objectAtIndex:0]) submitNoDrinkingSessionData];
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    CLSNSLog(@"alertView with tag: %li tapped buttonIndex: %li", (long)alertView.tag, (long)buttonIndex);
    
    if (alertView.tag == 31432 && buttonIndex == 1) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        [(HomeViewController *)[((UINavigationController *)self.navigationController.presentingViewController).viewControllers objectAtIndex:0] showDismissLabel];
        
        [self submitCancelData];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [self.navigationController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Processors

- (void)recalculatePositions:(NSNotification *)notification {
    if (self.datePicker.hidden == FALSE) {
        [self recalculateHIDE];
        [self recalculateSHOW];
    }else if (self.datePicker.hidden == TRUE) {
        [self recalculateSHOW];
        [self recalculateHIDE];
    }
}

- (void)recalculateSHOW {
    self.datePicker.hidden = NO;
    
    NSInteger initialOffset = -self.tableView.contentInset.top;
    
    NSInteger contentHeightSize = self.tableView.contentSize.height;
    if (contentHeightSize < [self trueHeight]) {
        //Normal
        contentHeightSize = [self trueHeight]-216;
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]+216) animated:YES];
        
        NSInteger heightOffset = self.tableView.contentSize.height;
        if (heightOffset < [self trueHeight]) heightOffset = [self trueHeight];
        
        self.datePicker.frame = (CGRect){0, heightOffset-216, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
    }else {
        //Extended
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]+216) animated:YES];
    }
}

- (void)recalculateHIDE {
    NSInteger initialOffset = -self.tableView.contentInset.top;
    
    NSInteger contentHeightSize = self.tableView.contentSize.height;
    if (contentHeightSize < [self trueHeight]) {
        //Normal
        contentHeightSize = [self trueHeight];
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]) animated:YES];
        
        NSInteger heightOffset = self.tableView.contentSize.height;
        if (heightOffset < [self trueHeight]) heightOffset = [self trueHeight];
        
        self.datePicker.frame = (CGRect){0, heightOffset, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
        self.datePicker.hidden = YES;
    }else {
        //Extended
        [self.tableView setContentOffset:CGPointMake(0, initialOffset+contentHeightSize-[self trueHeight]) animated:NO];
        self.datePicker.hidden = YES;
    }
}

#pragma mark Generic Helpers

- (NSDate *)getRoundedDate:(NSDate *)inDate {
    NSInteger minuteInterval = 15;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:NSMinuteCalendarUnit fromDate:inDate];
    NSInteger minutes = [dateComponents minute];
    
    float minutesF = [[NSNumber numberWithInteger:minutes] floatValue];
    float minuteIntervalF = [[NSNumber numberWithInteger:minuteInterval] floatValue];
    
    // Determine whether to add 0 or the minuteInterval to time found by rounding down
    NSInteger roundingAmount = (fmodf(minutesF, minuteIntervalF)) > minuteIntervalF/2.0 ? minuteInterval : 0;
    NSInteger minutesRounded = ( (NSInteger)(minutes / minuteInterval) ) * minuteInterval;
    NSDate *roundedDate = [[NSDate alloc] initWithTimeInterval:60.0 * (minutesRounded + roundingAmount - minutes) sinceDate:inDate];
    
    return roundedDate;
}

- (CGFloat)trueHeight {
    return self.view.frame.size.height-self.tableView.contentInset.top + (([UIScreen mainScreen].bounds.size.height == 480) ? 40 : 0);
}

@end