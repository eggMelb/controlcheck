//
//  UserDataProcessingEngine.m
//  Alcohol Capture
//
//  Created by Jason Pan on 29/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "UserDataProcessingEngine.h"
#import <Crashlytics/Crashlytics.h>

@implementation UserDataProcessingEngine

+(UserDataProcessingEngine *)sharedEngine {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static UserDataProcessingEngine *instance = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        instance = [[self alloc] init];
        [instance initialize];
    });
    
    // returns the same object each time
    return instance;
}

//static id _instance;
//- (id) init
//{
//    if (_instance == nil)
//    {
//        _instance = [super init];
//    }
//    [_instance initialize];
//    return _instance;
//}
//
//+ (UserDataProcessingEngine *) sharedEngine
//{
//    if (!_instance)
//    {
//        return [[UserDataProcessingEngine alloc] init];
//    }
//    return _instance;
//}
//
////+(UserDataProcessingEngine *)sharedEngine {
////    if (_instance == nil)
////    {
////        _instance = [[super allocWithZone:nil] init];
////    }
////    
////    [_instance initialize];
////    
////    // returns the same object each time
////    return _instance;
////}

- (void)flushUserSessionDataCache {
    CLSNSLog(@"flushUserDataCache: Searching...");
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    NSMutableArray *userDataFilePathsArray = [NSMutableArray array];
    for (NSString *fileName in filePathsArray) {
        if ([fileName containsString:@".csv"] && [fileName containsString:@"ac"]) {
            [userDataFilePathsArray addObject:fileName];
        }
    }
    if (userDataFilePathsArray.count > 0) {
        CLSNSLog(@"Found: %@", userDataFilePathsArray);
        CLSNSLog(@"flushUserDataCache: Now flushing user data cache...data will not be recoverable");
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        for (NSString *fileName in userDataFilePathsArray) {
            NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
            
            BOOL fileExists = [fileManager fileExistsAtPath:path];
//            CLSNSLog(@"Path to file: %@", path);
//            CLSNSLog(@"File exists: %d", fileExists);
//            CLSNSLog(@"Is deletable file at path: %d", [fileManager isDeletableFileAtPath:path]);
            if (fileExists)
            {
                BOOL success = [fileManager removeItemAtPath:path error:&error];
                if (!success) CLSNSLog(@"Error: %@", [error localizedDescription]);
            }
        }
    }
    CLSNSLog(@"flushUserDataCache: Complete.");
}

- (void)rewriteAppVersion {
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithDictionary:self.userDataDictionary];
    
    NSString *liveAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    [userInfo setObject:liveAppVersion forKey:@"App Version"];
    
    CLSNSLog(@"rewriteAppVersion: new user info: %@", userInfo);
    
    [self saveUserDataWithUserInfoDictionary:userInfo];
}

- (void)requestUserDataVerification {
    //TODO; IMPLEMENT
}

- (void)verifyDataStructures {
    [self verifyUserAdminDataStructure];
    [self verifyUserPreferencesDataStructure];
}

- (void)verifyUserAdminDataStructure {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"userInfo.plist"];
    
    BOOL fileExists = [fileManager fileExistsAtPath:path];
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:path error:&error];
        if (!success) CLSNSLog(@"Error: %@", [error localizedDescription]);
    }
}

- (void)verifyUserPreferencesDataStructure {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"userPreferences.plist"];
    
    BOOL fileExists = [fileManager fileExistsAtPath:path];
    if (fileExists)
    {
        BOOL success = [fileManager removeItemAtPath:path error:&error];
        if (!success) CLSNSLog(@"Error: %@", [error localizedDescription]);
    }
}

- (void)checkAppVersionStability {
    if (!self.userDataDictionary) return;
    
    NSString *appVersion = [self.userDataDictionary objectForKey:@"App Version"];
    NSString *liveAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    CLSNSLog(@"checkAppVersionStability: logged appVersion is %@", appVersion);
    CLSNSLog(@"checkAppVersionStability: live appVersion is %@", liveAppVersion);
    
    if (![appVersion isEqualToString:liveAppVersion]) {
        CLSNSLog(@"checkAppVersionStability: Logged app version is incorrect; taking stability precautions.");
        [self flushUserSessionDataCache];
        [self verifyDataStructures];
        
        [self loadAppSettings];
        [self loadBaseDictionary];
        [self loadUserData];
        [self loadUserPreferences];
        
//        [self rewriteAppVersion];
        [self requestUserDataVerification];
    }else {
        CLSNSLog(@"checkAppVersionStability: Stable.");
    }
}

-(void)initialize {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    CLSNSLog(@"UserDataProcessingEngine: Initializing...");
    CLSNSLog(@"UserDataProcessingEngine: \"Local date folder is:\" %@", documentsDirectory);
    
    _isUserFirstStartup = FALSE;
    
    [self loadAppSettings];
    [self loadBaseDictionary];
    [self loadUserData];
    [self loadUserPreferences];
    
    [self checkAppVersionStability];
    
//    if (self.userDataDictionary) {
//        NSString *currentParticipantID = [NSString stringWithFormat:@"%@", [self.userDataDictionary objectForKey:@"Participant ID"]];
//        CLSNSLog(@"Logging crashlytics ParticipantID: %@", currentParticipantID);
////        NSString *currentParticipantCountry  = [NSString stringWithFormat:@"%@", [self.userDataDictionary objectForKey:@"Country"]];
//        
//        [[Crashlytics sha]]
//        [[Crashlytics sharedInstance] setUserIdentifier:currentParticipantID];
////        [[Crashlytics sharedInstance] setValue:currentParticipantCountry forKey:@"Country"];
//    }
    
    [self loadPastUploadDates];
}

-(void)loadAppSettings {
    self.appSettingsDictionary = [NSDictionary dictionaryWithContentsOfFile:[self filePathForDataName:@"app_settings"]];
    CLSNSLog(@"loadAppSettings: %@", self.appSettingsDictionary);
    
    if (self.appSettingsDictionary == nil) {
        CLSNSLog(@"fatal issue: appSettingsDictionary is nil");
        abort();
    }
}

-(void)loadPastUploadDates {
    CLSNSLog(@"loadPastUploadDates");
    
    NSString *cachedData = [NSString stringWithContentsOfFile:[self dataFilePath] encoding:NSUTF8StringEncoding error:nil];
    
    cachedData = [cachedData stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
    cachedData = [cachedData substringToIndex:cachedData.length-2];
    NSArray *components = [cachedData componentsSeparatedByString:@", "];
    
    NSMutableArray *pastSessionsArray = [[NSMutableArray alloc] initWithCapacity:1000];
//    CLSNSLog(@"baseA: %@", self.baseArray);
//    CLSNSLog(@"components: %@", components);
//    CLSNSLog(@"[self dataFilePath]: %@", [self dataFilePath]);
//    return;
    for (int index = 0; index < components.count; index += self.baseArray.count) {
//    for (int index = 0; index < components.count-self.baseArray.count; index += self.baseArray.count) {
        NSMutableArray *sessionArray = [[NSMutableArray alloc] initWithCapacity:self.baseArray.count];
        
        for (int subIndex = 0; subIndex < self.baseArray.count; subIndex++) {
            NSString *object = [NSString stringWithFormat:@"%@", [components objectAtIndex:index+subIndex]];
            [sessionArray addObject:object];
        }
        [pastSessionsArray addObject:sessionArray];
    }
    
    NSInteger submissionDateIndex = [self.baseArray indexOfObject:@"Submission Date"];
    NSInteger submissionTimeIndex = [self.baseArray indexOfObject:@"Submission Time"];
    
    NSMutableArray *pastDatesArray = [[NSMutableArray alloc] initWithCapacity:pastSessionsArray.count];
    
    for (NSInteger index = 1; index < pastSessionsArray.count; index ++) { //Start at 1 to remove header
        NSString *submissionDate = [NSString stringWithFormat:@"%@", [[pastSessionsArray objectAtIndex:index] objectAtIndex:submissionDateIndex]];
        NSString *submissionTime = [NSString stringWithFormat:@"%@", [[pastSessionsArray objectAtIndex:index] objectAtIndex:submissionTimeIndex]];
        [pastDatesArray addObject:[NSString stringWithFormat:@"%@~%@", submissionDate, submissionTime]];
    }
    self.pastUploadTimestamps = pastDatesArray;
    
    NSMutableArray *pastUploadDates = [[NSMutableArray alloc] initWithCapacity:self.pastUploadTimestamps.count];
    NSMutableArray *pastUploadTimes = [[NSMutableArray alloc] initWithCapacity:self.pastUploadTimestamps.count];
    
    for (int index = 0; index < self.pastUploadTimestamps.count; index++) {
        NSString *timestamp = [self.pastUploadTimestamps objectAtIndex:self.pastUploadTimestamps.count-index-1];
        NSString *submissionDateString = [timestamp substringToIndex:[timestamp rangeOfString:@"~"].location];
        NSString *submissionTimeString = [timestamp substringFromIndex:[timestamp rangeOfString:@"~"].location+1];
        
        NSDateFormatter *intputDateFormatter = [[NSDateFormatter alloc] init];
        
        intputDateFormatter.dateFormat = @"dd/MM/yyyy";
        NSDate *submissionDate = [intputDateFormatter dateFromString:submissionDateString];
        
        intputDateFormatter.dateFormat = @"HH:mm";
        NSDate *submissionTime = [intputDateFormatter dateFromString:submissionTimeString];
        
        NSDateFormatter *outputDateFormatter = [[NSDateFormatter alloc] init];
        
        outputDateFormatter.dateFormat = @"EEEE, MMM dd, yyyy";
        [pastUploadDates addObject:[outputDateFormatter stringFromDate:submissionDate]];
        
        outputDateFormatter.dateFormat = @"hh:mm a";
        [pastUploadTimes addObject:[outputDateFormatter stringFromDate:submissionTime]];
    }
    
    self.pastUploadDates = pastUploadDates;
    self.pastUploadTimes = pastUploadTimes;
}

-(NSDictionary *)userData {
    return self.userDataDictionary;
}

-(void)loadUserPreferences {
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"UserPreferencesDataTemplate" ofType:@"plist"];
//    self.userPreferencesDictionary = [NSDictionary dictionaryWithContentsOfFile:path];
//    return;
    
    self.userPreferencesDictionary = [NSDictionary dictionaryWithContentsOfFile:[self filePathForDataName:@"preferences"]];
    CLSNSLog(@"loadUserPreferences: %@", self.userPreferencesDictionary);
    
    if (self.userPreferencesDictionary == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"UserPreferencesDataTemplate" ofType:@"plist"];
        [self saveUserPreferencesWithPreferenceDictionary:[NSDictionary dictionaryWithContentsOfFile:path]];
    }
}

-(NSDictionary *)notificationsPreferences {
    //Added Fri3Apr2015 - 5:12:49pm
    //Attempt reload
    NSDictionary *tempDictionary = [NSDictionary dictionaryWithContentsOfFile:[self filePathForDataName:@"preferences"]];
    if (tempDictionary) {
        if ([tempDictionary.allKeys containsObject:@"Notifications"]) {
            return [tempDictionary objectForKey:@"Notifications"];
        }
    }
    
    return [self.userPreferencesDictionary objectForKey:@"Notifications"];
}

-(void)saveUserPreferencesWithPreferenceDictionary:(NSDictionary *)preferenceDictionary {
    BOOL success = [preferenceDictionary writeToFile:[self filePathForDataName:@"preferences"] atomically:TRUE];
    if (success) CLSNSLog(@"UserDataProcessingEngine: \"saveUserPreferencesWithPreferenceDictionary\" succeeded.");
    
    [self loadUserPreferences];
}

-(NSString *)fileNameForDataName:(NSString *)dataName {
    if ([dataName isEqualToString:@"user"]) return @"userInfo.plist";
    else if ([dataName isEqualToString:@"session"]) {
        NSString *fileName = [NSString stringWithFormat:@"ac%@-%@", [self.userDataDictionary objectForKey:@"Participant ID"], [self.userDataDictionary objectForKey:@"App Version"]];
        return [NSString stringWithFormat:@"%@.csv", fileName];
    }
    else if ([dataName isEqualToString:@"preferences"])  return @"userPreferences.plist";
    else if ([dataName isEqualToString:@"drink"])  return @"";
    
    return @"";
}

-(NSString *)filePathForDataName:(NSString *)dataName {
    if ([dataName isEqualToString:@""] || dataName == nil) {
        CLSNSLog(@"UserDataProcessingEngine: Invalid dataName!");
        return nil;
    }
    
    if ([dataName isEqualToString:@"app_settings"]) {
        return [NSString stringWithFormat:@"%@", [[NSBundle mainBundle] pathForResource:@"AppSettings" ofType:@"plist"]];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fileName = [self fileNameForDataName:dataName];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}

-(void)updateUserDataWithUserInfoDictionary:(NSDictionary *)userInfoDictionary {
    NSMutableDictionary *existingUserDataDictionary = [NSMutableDictionary dictionaryWithContentsOfFile:[self filePathForDataName:@"user"]];
    if (!existingUserDataDictionary) existingUserDataDictionary = [[NSMutableDictionary alloc] initWithCapacity:100];
    
    for (int index = 0; index < userInfoDictionary.count; index++) {
        NSString *currentHeader = [NSString stringWithFormat:@"%@", [userInfoDictionary.allKeys objectAtIndex:index]];
        NSString *currentObject = [NSString stringWithFormat:@"%@", [userInfoDictionary objectForKey:currentHeader]];
        
        currentHeader = [currentHeader lowercaseString];
        
        if ([currentHeader rangeOfString:@"participant"].location != NSNotFound && [currentHeader rangeOfString:@"id"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Participant ID"];
        }
        if ([currentHeader rangeOfString:@"initials"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Participant Initials"];
        }
        if ([currentHeader rangeOfString:@"country"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Country"];
        }
        if ([currentHeader rangeOfString:@"gender"].location != NSNotFound || [currentHeader rangeOfString:@"sex"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Gender"];
        }
        if ([currentHeader rangeOfString:@"age"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Age (years)"];
        }
        if ([currentHeader rangeOfString:@"date"].location != NSNotFound && [currentHeader rangeOfString:@"birth"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Date of Birth"];
        }
        if ([currentHeader rangeOfString:@"app"].location != NSNotFound && [currentHeader rangeOfString:@"version"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"App Version"];
        }
        if ([currentHeader rangeOfString:@"weight"].location != NSNotFound || [currentHeader rangeOfString:@"mass"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Weight"];
        }
        if ([currentHeader rangeOfString:@"height"].location != NSNotFound) {
            [existingUserDataDictionary setObject:currentObject forKey:@"Height"];
        }
    }
    
    
    BOOL success = [existingUserDataDictionary writeToFile:[self filePathForDataName:@"user"] atomically:TRUE];
    if (success) CLSNSLog(@"UserDataProcessingEngine: \"updateUserDataWithUserInfoDictionary\" succeeded.");
    
    [self loadUserData];
}

-(void)saveUserDataWithUserInfoDictionary:(NSDictionary *)userInfoDictionary {
    BOOL success = [userInfoDictionary writeToFile:[self filePathForDataName:@"user"] atomically:TRUE];
    if (success) CLSNSLog(@"UserDataProcessingEngine: \"saveUserDataWithUserInfoDictionary\" succeeded.");
    
    [self loadUserData];
}

-(void)loadUserData {
    self.userDataDictionary = [NSDictionary dictionaryWithContentsOfFile:[self filePathForDataName:@"user"]];
}

-(BOOL)isStudyPeriodExpired {
    if (forceResearchPeriodExpire) return NO;
    int maximumStudyPeriod = [[[UserDataProcessingEngine sharedEngine].appSettingsDictionary objectForKey:@"Maximum Study Period (Days)"] intValue];
    NSDate *originalDate = [[UserDataProcessingEngine sharedEngine] userFirstSignInDate];
    NSDate *expirationDay = [NSDate dateWithTimeInterval:maximumStudyPeriod*24*60*60 sinceDate:originalDate];
    
    CLSNSLog(@"originalDate: %@", originalDate);
    CLSNSLog(@"expirationDay: %@", expirationDay);
    
//    BOOL isStudyPeriodExpired = ([expirationDay timeIntervalSinceDate:originalDate] <= 0);//oroginal
//    BOOL isStudyPeriodExpired = ([expirationDay timeIntervalSinceDate:originalDate] >= 0);//attempted fix
    BOOL isStudyPeriodExpired = ([expirationDay timeIntervalSinceDate:[NSDate date]] <= 0);
    //Simulation onlu
    BOOL simulationIsStudyPeriodExpired = ([[NSDate date] timeIntervalSinceDate:expirationDay] <= 0);
//    if (isDebuggingEndTrialPeriod) isStudyPeriodExpired = simulationIsStudyPeriodExpired;
    
    CLSNSLog(@"isStudyPeriodExpired: %@", isStudyPeriodExpired ? @"YES" : @"NO");
    
    return isStudyPeriodExpired;
}

- (NSDate *)userFirstSignInDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    return [dateFormatter dateFromString:[self.userDataDictionary objectForKey:@"First Sign-in Date"]];
}

-(BOOL)isUserSignedIn {
//    if (!self.userDataDictionary) _isUserFirstStartup = TRUE;
    if (!self.userDataDictionary) self.isUserFirstStartup = TRUE;
    return (self.userDataDictionary ? TRUE : FALSE);
}

-(void)loadBaseDictionary {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"UserDataTemplate" ofType:@"plist"];
    NSMutableArray *headersArray = [[NSArray arrayWithContentsOfFile:path] mutableCopy];
    self.templateHeadersArray = [headersArray copy];
    
    NSInteger drinkTypesIndex = -1;
    for (int index = 0; index < headersArray.count; index++) {
        if ([[NSString stringWithFormat:@"%@", [headersArray objectAtIndex:index]] isEqualToString:@"{DRINK TYPES}"]) drinkTypesIndex = index;
    }
    
    if (drinkTypesIndex == -1) abort();
    
    NSArray *drinkTypesArray = [[DrinkDataRetrievalEngine sharedEngine] drinkTypes];
    for (int index = 0; index < drinkTypesArray.count; index++) {
        NSString *currentDrinkType = [NSString stringWithFormat:@"%@", [drinkTypesArray objectAtIndex:index]];
        NSArray *descriptionSizeArray = [[DrinkDataRetrievalEngine sharedEngine] drinkDescriptionsForDrinkType:currentDrinkType];
        
        for (int sizeIndex = 0; sizeIndex < descriptionSizeArray.count; sizeIndex++) {
            NSString *currentHeader = [descriptionSizeArray objectAtIndex:sizeIndex];
            [headersArray insertObject:currentHeader atIndex:drinkTypesIndex];
            drinkTypesIndex += 1;
        }
    }
    [headersArray removeObject:@"{DRINK TYPES}"];
    
    self.baseArray = headersArray;
    
    CLSNSLog(@"Loaded baseArray: %@", self.baseArray);
}

-(NSMutableString *)headerData {
    NSMutableString *writeString = [NSMutableString stringWithCapacity:0];
    for (int index = 0; index < self.baseArray.count; index++) {
        [writeString appendString:[NSString stringWithFormat:@"%@", [self.baseArray objectAtIndex:index]]];
        if (index < self.baseArray.count-1) [writeString appendString:@", "];
        else [writeString appendString:@"\n"];
    }
    
    return writeString;
}

-(NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:[self fileNameForDataName:@"session"]];
}

-(void)saveSessionData:(NSDictionary *)sessionDataDictionary {
    self.currentSessionData = sessionDataDictionary;
}

-(NSDictionary *)sessionData {
    return self.currentSessionData;
}

-(void)saveDataWithDataArray:(NSArray *)dataArray {
    NSMutableString *writeString = [NSMutableString stringWithCapacity:0];
    NSFileHandle *handle;
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self dataFilePath]]) {
        CLSNSLog(@"Created File");
        [[NSFileManager defaultManager] createFileAtPath: [self dataFilePath] contents:nil attributes:nil];
        handle = [NSFileHandle fileHandleForWritingAtPath:[self dataFilePath]];
        
        [handle truncateFileAtOffset:[handle seekToEndOfFile]];
        [handle writeData:[[self headerData] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    handle = [NSFileHandle fileHandleForWritingAtPath:[self dataFilePath]];
    for (int index = 0; index < dataArray.count; index++) {
        [writeString appendString:[NSString stringWithFormat:@"%@", [dataArray objectAtIndex:index]]];
        if (index < dataArray.count-1) [writeString appendString:@", "];
        else [writeString appendString:@"\n"];
    }
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    [handle writeData:[writeString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [self uploadData];
}

-(NSString *)dropboxFolderPath {
    NSString *liveAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    return [NSString stringWithFormat:@"Alcohol Capture Data/%@/v%@", currentAppStatus, liveAppVersion];
//    return @"Beta";
//    return @"Trial";
//    return @"Production";
}

-(void)uploadData {
    DBPath *folderPath = [[DBPath root] childPath:[self dropboxFolderPath]];
    
    DBError *error;
    BOOL folderCreationSuccessfull = [[DBFilesystem sharedFilesystem] createFolder:folderPath error:&error];
    
    if (error) {
        if (error.code == DBErrorExists) {
            
        }else {
            CLSNSLog(@"Dropbox: Issue creating folder. Aborting.");
            return;
        }
    }
    
    if (folderCreationSuccessfull) {
        DBPath *newPath = [[DBPath root] childPath:[NSString stringWithFormat:@"%@/%@", folderPath, [self fileNameForDataName:@"session"]]];
        
        DBError *error;
        DBFile *file = [[DBFilesystem sharedFilesystem] createFile:newPath error:&error];
        
        if (error) {
            if (error.code == DBErrorExists) {
                file = [[DBFilesystem sharedFilesystem] openFile:newPath error:&error];
                [file writeData:[NSData dataWithContentsOfFile:[self dataFilePath]] error:nil];
            }
        }else {
            [file writeData:[NSData dataWithContentsOfFile:[self dataFilePath]] error:nil];
        }
    }
}

-(void)saveDataWithDrinks:(NSArray *)drinksArray {
    
}

-(void)saveDataWithNumberOfDrinks:(NSDictionary *)numberOfDrinksDictionary andUserData:(NSDictionary *)userDictionary andSessionData:(NSDictionary *)sessionDictionary {
    NSMutableArray *dataArray = [[NSMutableArray alloc] initWithCapacity:self.baseArray.count];
    
    for (int index = 0; index < self.baseArray.count; index++) {
        NSString *currentHeader = [NSString stringWithFormat:@"%@", [self.baseArray objectAtIndex:index]];
        
        if ([numberOfDrinksDictionary.allKeys containsObject:currentHeader]) {
            [dataArray addObject:[numberOfDrinksDictionary objectForKey:currentHeader]];
        }else {
            //Not a drink header
            currentHeader = [currentHeader lowercaseString];
            
            if ([currentHeader rangeOfString:@"participant"].location != NSNotFound && [currentHeader rangeOfString:@"id"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Participant ID"]];
            }
            if ([currentHeader rangeOfString:@"initials"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Participant Initials"]];
            }
            if ([currentHeader rangeOfString:@"country"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Country"]];
            }
            if ([currentHeader rangeOfString:@"gender"].location != NSNotFound || [currentHeader rangeOfString:@"sex"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Gender"]];
            }
            if ([currentHeader rangeOfString:@"age"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Age (years)"]];
            }
            if ([currentHeader rangeOfString:@"date"].location != NSNotFound && [currentHeader rangeOfString:@"birth"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Date of Birth"]];
            }
            if ([currentHeader rangeOfString:@"app"].location != NSNotFound && [currentHeader rangeOfString:@"version"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"App Version"]];
            }
            if ([currentHeader rangeOfString:@"weight"].location != NSNotFound || [currentHeader rangeOfString:@"mass"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Weight"]];
            }
            if ([currentHeader rangeOfString:@"height"].location != NSNotFound) {
                [dataArray addObject:[userDictionary objectForKey:@"Height"]];
            }
            if ([currentHeader rangeOfString:@"submission"].location != NSNotFound && [currentHeader rangeOfString:@"date"].location != NSNotFound) {
                [dataArray addObject:[sessionDictionary objectForKey:@"Submission Date"]];
            }
            if ([currentHeader rangeOfString:@"submission"].location != NSNotFound && [currentHeader rangeOfString:@"time"].location != NSNotFound) {
                [dataArray addObject:[sessionDictionary objectForKey:@"Submission Time"]];
            }
            if ([currentHeader rangeOfString:@"date"].location != NSNotFound && [currentHeader rangeOfString:@"session"].location != NSNotFound) {
                if ([currentHeader rangeOfString:@"start"].location != NSNotFound || [currentHeader rangeOfString:@"begin"].location != NSNotFound || [currentHeader rangeOfString:@"initial"].location != NSNotFound) {
                    [dataArray addObject:[sessionDictionary objectForKey:@"Session Start Date"]];
                }else if ([currentHeader rangeOfString:@"end"].location != NSNotFound || [currentHeader rangeOfString:@"finish"].location != NSNotFound || [currentHeader rangeOfString:@"final"].location != NSNotFound) {
                    [dataArray addObject:[sessionDictionary objectForKey:@"Session End Date"]];
                }
            }
            if ([currentHeader rangeOfString:@"time"].location != NSNotFound && ([currentHeader rangeOfString:@"start"].location != NSNotFound || [currentHeader rangeOfString:@"commence"].location != NSNotFound)) {
                [dataArray addObject:[sessionDictionary objectForKey:@"Time: Commence Drinking"]];
            }
            if ([currentHeader rangeOfString:@"time"].location != NSNotFound && ([currentHeader rangeOfString:@"finish"].location != NSNotFound || [currentHeader rangeOfString:@"end"].location != NSNotFound)) {
                [dataArray addObject:[sessionDictionary objectForKey:@"Time: Finish Drinking"]];
            }
            if ([currentHeader rangeOfString:@"time"].location != NSNotFound && [currentHeader rangeOfString:@"total"].location != NSNotFound) {
                if ([currentHeader rangeOfString:@"minutes"].location != NSNotFound) {
                    [dataArray addObject:[sessionDictionary objectForKey:@"Total Drinking Time (minutes)"]];
                }else if ([currentHeader rangeOfString:@"hours"].location != NSNotFound) {
                    [dataArray addObject:[sessionDictionary objectForKey:@"Total Drinking Time (hours)"]];
                }
            }
            if ([currentHeader rangeOfString:@"total"].location != NSNotFound && [currentHeader rangeOfString:@"drinks"].location != NSNotFound) {
                [dataArray addObject:[sessionDictionary objectForKey:@"Total Drinks"]];
            }
        }
        
    }
    
    [self saveDataWithDataArray:dataArray];
    
    CLSNSLog(@"saving dataArray: %@", dataArray);
}

- (NSDictionary *)retrieveSummaryInformation {
    NSString *cachedData = [NSString stringWithContentsOfFile:[self dataFilePath] encoding:NSUTF8StringEncoding error:nil];
    
    cachedData = [cachedData stringByReplacingOccurrencesOfString:@"\n" withString:@", "];
    cachedData = [cachedData substringToIndex:cachedData.length-2];
    NSArray *components = [cachedData componentsSeparatedByString:@", "];
    
    NSMutableArray *pastSessionsArray = [[NSMutableArray alloc] initWithCapacity:1000];
    
    for (int index = 0; index < components.count; index += self.baseArray.count) {
        NSMutableArray *sessionArray = [[NSMutableArray alloc] initWithCapacity:self.baseArray.count];
        
        for (int subIndex = 0; subIndex < self.baseArray.count; subIndex++) {
            NSString *object = [NSString stringWithFormat:@"%@", [components objectAtIndex:index+subIndex]];
            [sessionArray addObject:object];
        }
        [pastSessionsArray addObject:sessionArray];
    }
    
    NSInteger submissionDateIndex = [self.baseArray indexOfObject:@"Submission Date"];
    NSInteger sessionStartDateIndex = [self.baseArray indexOfObject:@"Session Start Date"];
    NSInteger sessionEndDateIndex = [self.baseArray indexOfObject:@"Session End Date"];
    NSInteger totalDrinksIndex = [self.baseArray indexOfObject:@"Total Drinks"];

    NSMutableArray *submissionDatesArray = [[NSMutableArray alloc] initWithCapacity:pastSessionsArray.count];
    NSMutableArray *sessionStartDateArray = [[NSMutableArray alloc] initWithCapacity:pastSessionsArray.count];
    NSMutableArray *sessionEndDateArray = [[NSMutableArray alloc] initWithCapacity:pastSessionsArray.count];
    CGFloat totalDrinks = 0;
    CGFloat totalStandardDrinks = 0;
    
    for (NSInteger index = 1; index < pastSessionsArray.count; index ++) { //Start at 1 to remove header
        NSString *submissionDate = [NSString stringWithFormat:@"%@", [[pastSessionsArray objectAtIndex:index] objectAtIndex:submissionDateIndex]];
        NSString *sessionStartDate = [NSString stringWithFormat:@"%@", [[pastSessionsArray objectAtIndex:index] objectAtIndex:sessionStartDateIndex]];
        NSString *sessionEndDate = [NSString stringWithFormat:@"%@", [[pastSessionsArray objectAtIndex:index] objectAtIndex:sessionEndDateIndex]];
        
        [submissionDatesArray addObject:[NSString stringWithFormat:@"%@", submissionDate]];
        [sessionStartDateArray addObject:[NSString stringWithFormat:@"%@", sessionStartDate]];
        [sessionEndDateArray addObject:[NSString stringWithFormat:@"%@", sessionEndDate]];
        
        totalDrinks += [[[pastSessionsArray objectAtIndex:index] objectAtIndex:totalDrinksIndex] integerValue];
        
        //560 - 764
        for (int drinkIndex = (int)[self.templateHeadersArray indexOfObject:@"{DRINK TYPES}"]; drinkIndex < self.baseArray.count; drinkIndex++) {
            int numberOfDrinksConsumed = [[[pastSessionsArray objectAtIndex:index] objectAtIndex:drinkIndex] intValue];
            if (numberOfDrinksConsumed == 0) continue;
            
            NSString *currentDrinkDescription = [self.baseArray objectAtIndex:drinkIndex];
            
            Drink *currentDrink = [Drink new];
            currentDrink.type = [currentDrinkDescription substringToIndex:[currentDrinkDescription rangeOfString:@" (" options:NSBackwardsSearch].location];
            NSString *drinkSize = [currentDrinkDescription substringFromIndex:[currentDrinkDescription rangeOfString:@"(" options:NSBackwardsSearch].location+1];
            drinkSize = [drinkSize substringToIndex:[drinkSize rangeOfString:@" mL" options:NSBackwardsSearch].location];
            currentDrink.size = [drinkSize integerValue];
            
            NSDictionary *drinkInfo = [[DrinkDataRetrievalEngine sharedEngine] infoForDrinkType:currentDrink.type];
            currentDrink.alcoholPercentage = [NSString stringWithFormat:@"%@", [drinkInfo objectForKey:@"AlcoholPercentage"]];
//            CLSNSLog(@"Drink: %@", currentDrink);
//            CLSNSLog(@"Drink: %@", drinkSize);
//            CLSNSLog(@"Drink: %li", (long)currentDrink.size);
            
            float standardDrink = ((float)currentDrink.size/1000.0f) * [(currentDrink.alcoholPercentage) floatValue] * 0.789f;//Standard Drink Formula
            standardDrink *= (double)numberOfDrinksConsumed;
            totalStandardDrinks += standardDrink;
//            NSLog(@"standard drink: %1.3f || %1.3f || %1.3f || == %1.3f", (float)currentDrink.size/1000.0f, ([(currentDrink.alcoholPercentage) floatValue]/1.0f), 0.789f, standardDrink);
//            CLSNSLog(@"totalStandardDrinks: %1.2f", totalStandardDrinks);
        }
        
//        NSArray *drinkTypesArray = [[DrinkDataRetrievalEngine sharedEngine] drinkTypes];
//        for (int index = 0; index < drinkTypesArray.count; index++) {
//            NSString *currentDrinkType = [NSString stringWithFormat:@"%@", [drinkTypesArray objectAtIndex:index]];
//            NSArray *descriptionSizeArray = [[DrinkDataRetrievalEngine sharedEngine] drinkDescriptionsForDrinkType:currentDrinkType];
//            
//            for (int sizeIndex = 0; sizeIndex < descriptionSizeArray.count; sizeIndex++) {
//                NSString *currentHeader = [descriptionSizeArray objectAtIndex:sizeIndex];
//                [headersArray insertObject:currentHeader atIndex:drinkTypesIndex];
//                drinkTypesIndex += 1;
//            }
//        }
    }
    
    totalDrinks = totalStandardDrinks; //Removed @ Mon11thMay12:26:21pm 2014 - bug
    
    NSMutableSet *mutualSessionSet = [NSMutableSet setWithArray:sessionStartDateArray];
    [mutualSessionSet unionSet:[NSSet setWithArray:sessionEndDateArray]];
    [mutualSessionSet removeObject:@"N/A"];
    
    CLSNSLog(@"mutualSessionSet: %@", mutualSessionSet);
    
    
    
    
    
    
    
//    NSMutableDictionary *numberOfDrinksDictionary = [[NSMutableDictionary alloc] initWithCapacity:[[DrinkDataRetrievalEngine sharedEngine] drinkTypes].count];
//    for (int index = 0; index < [[DrinkDataRetrievalEngine sharedEngine] drinkTypes].count; index++) {
//        NSString *currentDrinkType = [NSString stringWithFormat:@"%@", [[[DrinkDataRetrievalEngine sharedEngine] drinkTypes] objectAtIndex:index]];
//        NSArray *currentDescriptionSizeArray = [[DrinkDataRetrievalEngine sharedEngine] drinkDescriptionsForDrinkType:currentDrinkType];
//        
////        for (int sizeIndex = 0; sizeIndex < currentDescriptionSizeArray.count; sizeIndex++) {
////            NSString *currentSizeDescription = [NSString stringWithFormat:@"%@", [currentDescriptionSizeArray objectAtIndex:sizeIndex]];
////            
////            NSInteger totalQuantity = 0;
////            for (int subIndex = 0; subIndex < self.drinksArray.count; subIndex++) {
////                Drink *currentDrink = [self.drinksArray objectAtIndex:subIndex];
////                NSString *currentDrinkSizeDescription = [[DrinkDataRetrievalEngine sharedEngine] drinkDescriptionForDrinkType:currentDrink.type andSize:[NSString stringWithFormat:@"%li", (long)currentDrink.size]];
////                
////                if ([currentDrinkSizeDescription isEqualToString:currentSizeDescription]) {
////                    totalQuantity += currentDrink.quantity;
////                }
////            }
////            
////            [numberOfDrinksDictionary setObject:[NSString stringWithFormat:@"%li", (long)totalQuantity] forKey:currentSizeDescription];
////        }
//    }
    
    
    
    
    
    
    CGFloat numberOfStandardDrinks = (int)totalDrinks;
    int numberOfDrinkingDays = (int)mutualSessionSet.count;
//    int lengthOfResearchPeriod = [[[UserDataProcessingEngine sharedEngine].appSettingsDictionary objectForKey:@"Maximum Study Period (Days)"] intValue];
    float averageStandardDrinksPerDay = ((float)numberOfStandardDrinks / numberOfDrinkingDays);
    
    CLSNSLog(@"numberOfStandardDrinks: %1.2f", numberOfStandardDrinks);
    CLSNSLog(@"numberOfDrinkingDays: %i", numberOfDrinkingDays);
    CLSNSLog(@"averageStandardDrinksPerDay: %1.2f", averageStandardDrinksPerDay);
    
    NSDictionary *summaryInformationDictionary = @{@"numberOfStandardDrinks": [NSNumber numberWithFloat:numberOfStandardDrinks],
                                                   @"numberOfDrinkingDays": [NSNumber numberWithFloat:numberOfDrinkingDays],
                                                   @"averageStandardDrinksPerDay": [NSNumber numberWithFloat:averageStandardDrinksPerDay]};
    
    return summaryInformationDictionary;
}

- (void)expireData {
    [self flushUserSessionDataCache];
}

- (BOOL)isSessionDataUnavailable {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    NSMutableArray *userDataFilePathsArray = [NSMutableArray array];
    for (NSString *fileName in filePathsArray) {
        if ([fileName containsString:@".csv"] && [fileName containsString:@"ac"]) {
            [userDataFilePathsArray addObject:fileName];
        }
    }
    if (userDataFilePathsArray.count > 0) {
        CLSNSLog(@"Found: %@", userDataFilePathsArray);
        CLSNSLog(@"flushUserDataCache: Now flushing user data cache...data will not be recoverable");
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        for (NSString *fileName in userDataFilePathsArray) {
            NSString *path = [documentsDirectory stringByAppendingPathComponent:fileName];
            
            BOOL fileExists = [fileManager fileExistsAtPath:path];
            
            if (fileExists) return NO;
        }
    }
    
    return YES;
}

@end
