//
//  DrinkMenuItemButton.h
//  Alcohol Capture
//
//  Created by Jason Pan on 29/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DrinkMenuItemButton : UIButton

///From 0.0f to 1.0f
@property (nonatomic, assign) CGFloat drinkStrengthLevel;

@end