//
//  ParticipantInformationViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 12/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataProcessingEngine.h"
#import "JPTextField.h"

@interface ParticipantInformationViewController : UIViewController <UIAlertViewDelegate, UITextFieldDelegate, UIPickerViewDelegate> {
    IBOutlet UIScrollView *dataEntryScrollView;
    IBOutlet UIView *dataEntryContentView;
    
    IBOutlet JPTextField *idTextField;
    IBOutlet JPTextField *countryTextfield;
    IBOutlet UISegmentedControl *genderSegmentControl;
    IBOutlet JPTextField *dobTextField;
    IBOutlet JPTextField *weightTextfield;
    IBOutlet JPTextField *heightTextfield;
    
    IBOutlet UILabel *accessCodeLabel;
    IBOutlet UITextField *accessCodeTextfield;
    
    BOOL keyboardIsShown;
}
@property (nonatomic, assign) CGRect currentKeyboardFrame;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, assign) BOOL datePickerIsShown;

@property (nonatomic, strong) UIPickerView *countryPicker;
@property (nonatomic, assign) BOOL countryPickerIsShown;
@property (nonatomic, strong) NSArray *countriesArray;

@end
