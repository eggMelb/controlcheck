//
//  HomeViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 19/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataProcessingEngine.h"

@interface HomeViewController : UIViewController {
    IBOutlet UILabel *promptLabel;
}
@property (nonatomic, assign) BOOL backgroundStatus;

-(void)showDismissLabel;

@end
