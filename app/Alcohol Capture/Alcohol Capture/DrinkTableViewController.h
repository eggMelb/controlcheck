//
//  DrinkTableViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 20/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+FixNavigationBarCorruption.h"

#import "DrinkPropertiesTableViewController.h"

#import "HomeViewController.h"

#import "UserDataProcessingEngine.h"

@interface DrinkTableViewController : UITableViewController <UIAlertViewDelegate> {
    UIView *dismissView;
    UIView *dismissToolbarView;
}

@property (nonatomic, strong) NSMutableArray *drinksArray;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSIndexPath *currentSelectedIndexPath;

@property (nonatomic, strong) NSDate *currentStartDate;
@property (nonatomic, strong) NSDate *currentEndDate;

@end
