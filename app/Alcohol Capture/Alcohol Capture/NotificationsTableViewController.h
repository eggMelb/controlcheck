//
//  NotificationsTableViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 4/12/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataProcessingEngine.h"

#define kNumberOfMorningReminders 1

@interface NotificationsTableViewController : UITableViewController <UIAlertViewDelegate>

@property (nonatomic, assign) BOOL hasMadeChanges;

@property (nonatomic, strong) UIDatePicker *datePicker;
@property (nonatomic, strong) NSIndexPath *currentSelectedIndexPath;

@end
