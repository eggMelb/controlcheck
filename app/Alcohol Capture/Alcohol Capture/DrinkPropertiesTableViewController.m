//
//  DrinkPropertiesTableViewController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 20/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "DrinkPropertiesTableViewController.h"

@interface DrinkPropertiesTableViewController ()

@end

@implementation DrinkPropertiesTableViewController

- (BOOL)navigationShouldPopOnBackButton {
    BOOL needsShowConfirmation = FALSE;
    
    if ([[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]].detailTextLabel.text isEqualToString:@"(unfilled)"]) needsShowConfirmation = TRUE;
    if ([[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]].detailTextLabel.text isEqualToString:@"(unfilled)"]) needsShowConfirmation = TRUE;
    if ([[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]].detailTextLabel.text isEqualToString:@"(unfilled)"]) needsShowConfirmation = TRUE;
    
    if ([[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].detailTextLabel.text isEqualToString:@"(null)"] &&
        [[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]].detailTextLabel.text isEqualToString:@"(null)"] &&
        [[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]].detailTextLabel.text isEqualToString:@"(null)"] &&
        [[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]].detailTextLabel.text isEqualToString:@"(null)"]) {
        
        return YES;
    }else {
        if (!needsShowConfirmation && self.isAddingNewDrink) {
            CLSNSLog(@"adding New Drink");
            
            [((DrinkTableViewController *)[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]).drinksArray addObject:self.drink];//TODO: ADD SAFETY NET
            self.isAddingNewDrink = FALSE;
        }
    }
    
    if(needsShowConfirmation) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Incomplete" message:NSLocalizedString(@"DRINK_PROPERTIES_INCOMPLETE_FIELDS_MESSAGE", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 122;
        [alertView show];
        
        return NO;
    }else {
        DrinkTableViewController *viewController = ((DrinkTableViewController *)[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]);
        
        [viewController.navigationController setNavigationBarHidden:NO animated:YES];
        [viewController.tableView reloadData];
        if ([viewController.tableView numberOfRowsInSection:0] == 1) {
            [viewController.navigationItem.leftBarButtonItem setEnabled:NO];
            [viewController setEditing:NO animated:YES];
            
            [viewController.navigationItem.rightBarButtonItem setTitle:@"Cancel"];
        }else {
            [viewController.navigationItem.leftBarButtonItem setEnabled:YES];
            
            [viewController.navigationItem.rightBarButtonItem setTitle:@"Submit"];
        }
    }
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return [self navigationShouldPopOnBackButton];
}

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    propertyArray = @[@"Type", @"Quantity", @"Alcohol Content", @"Size"];
    
    int minimumQuantity = [[[UserDataProcessingEngine sharedEngine].appSettingsDictionary objectForKey:@"Minimum Drink Quantity"] intValue];
    int maximumQuantity = [[[UserDataProcessingEngine sharedEngine].appSettingsDictionary objectForKey:@"Maximum Drink Quantity"] intValue];
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:maximumQuantity-minimumQuantity];
    for (int index = minimumQuantity; index <= maximumQuantity; index++) {
        [array addObject:[NSString stringWithFormat:@"%i", index]];
    }
    self.quantityDataArray = array;
    
    self.alcoholPercentageDataArray = [[NSMutableArray alloc] initWithCapacity:100];
    self.sizeDataArray = [[DrinkDataRetrievalEngine sharedEngine] descriptionSizesForDrinkType:self.drink.type withOrder:YES];
    
    self.tableView.bounces = NO;
    
    self.propertyPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 162)];
    self.propertyPickerView.delegate = self;
    self.propertyPickerView.dataSource = self;
    [self.tableView addSubview:self.propertyPickerView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView:)];
    [self.tableView addGestureRecognizer:tap];
    
    if (self.drink == nil) {
        self.navigationItem.hidesBackButton = YES;
        //New Drink
        self.drink = [Drink new];
        self.drink.type = @"";
        self.drink.quantity = 0;
        self.drink.alcoholPercentage = @"0%";
        self.drink.size = 0;
        
        self.title = @"New Drink";
        [self performSelector:@selector(presentMenu) withObject:nil afterDelay:0.3];
        
        return;
    }
    
    [self reloadProperties];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GUI Event Handlers

- (void)presentMenu {
    [[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] performSegueWithIdentifier:@"showDrinksMenuViewController" sender:self];//TODO: ADD SAFETY NET
    [self performSelector:@selector(unhideBackButtonBug) withObject:nil afterDelay:0.8];
}

- (void)unhideBackButtonBug {
    self.navigationItem.hidesBackButton = NO;
}

- (void)reloadProperties {
    CLSNSLog(@"TESTTEST: %@", [[DrinkDataRetrievalEngine sharedEngine] descriptionSizeForDrinkType:self.drink.type andSize:[NSString stringWithFormat:@"%li", (long)self.drink.size]]);
    if (self.drink.size) {
        if (self.drink.quantity == 1) propertyDictionary = [@{@"Type": self.drink.type, @"Quantity": [NSString stringWithFormat:@"%li drink", (long)self.drink.quantity], @"Alcohol Content": self.drink.alcoholPercentage, @"Size": [[DrinkDataRetrievalEngine sharedEngine] descriptionSizeForDrinkType:self.drink.type andSize:[NSString stringWithFormat:@"%li", (long)self.drink.size]]} mutableCopy];
        else propertyDictionary = [@{@"Type": self.drink.type, @"Quantity": [NSString stringWithFormat:@"%li drinks", (long)self.drink.quantity], @"Alcohol Content": self.drink.alcoholPercentage, @"Size": [[DrinkDataRetrievalEngine sharedEngine] descriptionSizeForDrinkType:self.drink.type andSize:[NSString stringWithFormat:@"%li", (long)self.drink.size]]} mutableCopy];
    }else {
        if (self.drink.quantity == 1) propertyDictionary = [@{@"Type": self.drink.type, @"Quantity": [NSString stringWithFormat:@"%li drink", (long)self.drink.quantity], @"Alcohol Content": self.drink.alcoholPercentage, @"Size": [NSString stringWithFormat:@"%li mL", (long)self.drink.size]} mutableCopy];
        else propertyDictionary = [@{@"Type": self.drink.type, @"Quantity": [NSString stringWithFormat:@"%li drinks", (long)self.drink.quantity], @"Alcohol Content": self.drink.alcoholPercentage, @"Size": [NSString stringWithFormat:@"%li mL", (long)self.drink.size]} mutableCopy];
    }
    
    [self.tableView reloadData];
    if (self.currentSelectedIndexPath) [self.tableView selectRowAtIndexPath:self.currentSelectedIndexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CLSNSLog(@"didSelectRowAtIndexPath");
    if (indexPath == [self.tableView indexPathForSelectedRow] && indexPath == self.currentSelectedIndexPath) {
        self.currentSelectedIndexPath = nil;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self hidePickerView];
        
        return;
    }
    
    [self.propertyPickerView selectRow:0 inComponent:0 animated:NO];
    
    CLSNSLog(@"CLASS: %@", NSStringFromClass([[self parentViewController] class]));
    if (indexPath.row == 0) {
        [[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2] performSegueWithIdentifier:@"showDrinksMenuViewController" sender:[tableView cellForRowAtIndexPath:indexPath]];//TODO: ADD SAFETY NET
        
        self.currentSelectedIndexPath = nil;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self hidePickerView];
    }else if (indexPath.row == 1) {
        self.pickerViewDataArray = [self.quantityDataArray mutableCopy];
        [self.propertyPickerView reloadAllComponents];
        self.propertyPickerView.backgroundColor = [UIColor lightGrayColor];
        [self.view bringSubviewToFront:self.propertyPickerView];
        self.currentSelectedIndexPath = indexPath;
        [self showPickerView];
        
        NSString *selectedCellData = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]].detailTextLabel.text;
        
        if (![selectedCellData isEqualToString:@"(unfilled)"]) {
            selectedCellData = [selectedCellData stringByReplacingOccurrencesOfString:@"drinks" withString:@""];
            selectedCellData = [selectedCellData stringByReplacingOccurrencesOfString:@"drink" withString:@""];
            selectedCellData = [selectedCellData stringByReplacingOccurrencesOfString:@" " withString:@""];
            CLSNSLog(@"selectedCellData: %@", selectedCellData);
            [self.propertyPickerView selectRow:[self.pickerViewDataArray indexOfObject:selectedCellData] inComponent:0 animated:NO];
        }
    }else if (indexPath.row == 2) {
        self.currentSelectedIndexPath = nil;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self hidePickerView];
    }else if (indexPath.row == 3) {
        self.pickerViewDataArray = [self.sizeDataArray mutableCopy];
        [self.propertyPickerView reloadAllComponents];
        self.propertyPickerView.backgroundColor = [UIColor lightGrayColor];
        [self.view bringSubviewToFront:self.propertyPickerView];
        self.currentSelectedIndexPath = indexPath;
        [self showPickerView];
        
        NSString *selectedCellData = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]].detailTextLabel.text;
        
        if (![selectedCellData isEqualToString:@"(unfilled)"]) {
            CLSNSLog(@"selectedCellData: %@", selectedCellData);
            if ([self.pickerViewDataArray containsObject:selectedCellData]) {
                [self.propertyPickerView selectRow:[self.pickerViewDataArray indexOfObject:selectedCellData] inComponent:0 animated:NO];
            }else {
                [self.propertyPickerView.delegate pickerView:self.propertyPickerView didSelectRow:0 inComponent:0];
            }
        }
    }else {
        self.pickerViewDataArray = [NSMutableArray arrayWithObjects:@"Test", @"Test", @"Test", @"Test", nil];
        [self.propertyPickerView reloadAllComponents];
    }
    
    NSString *selectedCellData = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]].detailTextLabel.text;
    
    if ([selectedCellData isEqualToString:@"(unfilled)"]) {
        [self.propertyPickerView.delegate pickerView:self.propertyPickerView didSelectRow:0 inComponent:0];
    }
}

- (void) didTapOnTableView:(UIGestureRecognizer*) recognizer {
    CGPoint tapLocation = [recognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapLocation];
    
    if (indexPath) {
        recognizer.cancelsTouchesInView = NO;
    } else {
        self.currentSelectedIndexPath = nil;
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
        [self hidePickerView];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return propertyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *propertyCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Drink_Property"];
    
    propertyCell.textLabel.text = [propertyArray objectAtIndex:indexPath.row];
    propertyCell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [propertyDictionary objectForKey:[propertyArray objectAtIndex:indexPath.row]]];
    if (indexPath.row == 1 && [propertyCell.detailTextLabel.text isEqualToString:@"0 drinks"]) {
        propertyCell.detailTextLabel.text = @"(unfilled)";
    }
    if (indexPath.row == 2 && [propertyCell.detailTextLabel.text isEqualToString:@"0%"]) {
        propertyCell.detailTextLabel.text = @"(unfilled)";
    }
    if (indexPath.row == 3 && [propertyCell.detailTextLabel.text isEqualToString:@"0 mL"]) {
        propertyCell.detailTextLabel.text = @"(unfilled)";
    }
    
    if ([propertyCell.detailTextLabel.text isEqualToString:@"(null)"]) {
        propertyCell.detailTextLabel.text = @"(unfilled)";
    }
    
    propertyCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    if (indexPath.row == 2) {
        propertyCell.selectionStyle = UITableViewCellSelectionStyleNone;
        propertyCell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return propertyCell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Show/Hide UIPickerView Methods

- (void)showPickerView {
    [UIView animateWithDuration:0.3 animations:^ {
        self.propertyPickerView.frame = (CGRect){0, self.view.frame.size.height-self.propertyPickerView.frame.size.height-44-20, self.propertyPickerView.frame.size.width, self.propertyPickerView.frame.size.height};
    }];
}

- (void)hidePickerView {
    [UIView animateWithDuration:0.3 animations:^ {
        self.propertyPickerView.frame = (CGRect){0, self.view.frame.size.height+44+20, self.propertyPickerView.frame.size.width, self.propertyPickerView.frame.size.height};
    }];
}

#pragma mark - UIPickerView Delegate Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.pickerViewDataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (self.pickerViewDataArray) return [self.pickerViewDataArray objectAtIndex:row];
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([self.tableView indexPathForSelectedRow].row == 1) {
        self.drink.quantity = [[pickerView.delegate pickerView:pickerView titleForRow:row forComponent:component] intValue];
        [self reloadProperties];
    }else if ([self.tableView indexPathForSelectedRow].row == 2) {
        self.drink.alcoholPercentage = [NSString stringWithFormat:@"%@%%", [pickerView.delegate pickerView:pickerView titleForRow:row forComponent:component]];
        [self reloadProperties];
    }else if ([self.tableView indexPathForSelectedRow].row == 3) {
        CLSNSLog(@"TEST 5; TYPE: %@ || DescriptionSizes: %@ || Size: %li", self.drink.type, [pickerView.delegate pickerView:pickerView titleForRow:row forComponent:component], (long)self.drink.size);
        
        self.drink.size = [[DrinkDataRetrievalEngine sharedEngine] millilitresForDrinkType:self.drink.type andDescriptionSizes:[pickerView.delegate pickerView:pickerView titleForRow:row forComponent:component]];
        [self reloadProperties];
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 122) {
        if (buttonIndex == 1) {
            DrinkTableViewController *viewController = ((DrinkTableViewController *)[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2]);
            
            [viewController.navigationController setNavigationBarHidden:NO animated:YES];
            [viewController.tableView reloadData];
            if ([viewController.tableView numberOfRowsInSection:0] == 1) {
                [viewController.navigationItem.leftBarButtonItem setEnabled:NO];
                [viewController setEditing:NO animated:YES];
                
                [viewController.navigationItem.rightBarButtonItem setTitle:@"Cancel"];
            }else {
                [viewController.navigationItem.leftBarButtonItem setEnabled:YES];
                
                [viewController.navigationItem.rightBarButtonItem setTitle:@"Submit"];
            }
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

@end
