//
//  DrinkMenuItemButton.m
//  Alcohol Capture
//
//  Created by Jason Pan on 29/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "DrinkMenuItemButton.h"
#import <Crashlytics/Crashlytics.h>

@implementation DrinkMenuItemButton

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.drinkStrengthLevel = 1.0f;
    }
    return self;
}

- (void)layoutSubviews {
    // Allow default layout, then center imageView
    [super layoutSubviews];
    
    UIImageView *imageView = [self imageView];
    CGRect imageFrame = imageView.frame;
    imageFrame.origin.x = (int)((self.frame.size.width - imageFrame.size.width)/ 2);
    imageView.frame = imageFrame;
    
    CLSNSLog(@"forced layoutSubviews");
    
    imageView.alpha = self.drinkStrengthLevel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
