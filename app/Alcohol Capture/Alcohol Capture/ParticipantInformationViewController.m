//
//  ParticipantInformationViewController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 12/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "ParticipantInformationViewController.h"

@interface ParticipantInformationViewController ()

@end

@implementation ParticipantInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self initialize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI Event Handlers

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self resignAllFieldResponders:YES];
}

- (void)didTapOnScrollView {
    [self resignAllFieldResponders:YES];
}

- (IBAction)continueSetup:(id)sender {
    if ([idTextField.text isEqualToString:@""] || [countryTextfield.text isEqualToString:@""] || [dobTextField.text isEqualToString:@""] || [weightTextfield.text isEqualToString:@""] || [heightTextfield.text isEqualToString:@""] || genderSegmentControl.selectedSegmentIndex == -1) {
        UIAlertView *alertView = [self errorAlertViewWithTitle:@"Incomplete" message:NSLocalizedString(@"PARTICIPANT_INFORMATION_BLANK_FIELDS_MESSAGE", nil)];
        [alertView show];
    }else {
        if ([accessCodeTextfield.text isEqualToString:@""]) {
            UIAlertView *alertView = [self errorAlertViewWithTitle:@"Error" message:NSLocalizedString(@"PARTICIPANT_INFORMATION_BLANK_ACCESS_CODE_MESSAGE", nil)];
            [alertView show];
        }else {
            if ([accessCodeTextfield.text isEqualToString:[NSString stringWithFormat:@"%i@cp#ss", ([idTextField.text intValue] + 8) * 3 + 6]]) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Warning" message:NSLocalizedString(@"PARTICIPANT_INFORMATION_VERIFICATION_MESSAGE", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                alertView.tag = 245;
                [alertView show];
            }else {
                UIAlertView *alertView = [self errorAlertViewWithTitle:@"Error" message:NSLocalizedString(@"PARTICIPANT_INFORMATION_INCORRECT_ACCESS_CODE_MESSAGE", nil)];
                [alertView show];
            }
        }
    }
}

- (void)cancel:(UIBarButtonItem *)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Initializers

- (void)loadUserData {
    //Load user data, if signed in
    BOOL isSignedIn = [[UserDataProcessingEngine sharedEngine] isUserSignedIn];
    if (isSignedIn) {
        NSDictionary *userInfo = [[UserDataProcessingEngine sharedEngine] userData];
        
        idTextField.text = [userInfo objectForKey:@"Participant ID"];
        countryTextfield.text = [userInfo objectForKey:@"Country"];
        genderSegmentControl.selectedSegmentIndex = [[userInfo objectForKey:@"Gender"] integerValue] - 1;
        dobTextField.text = [userInfo objectForKey:@"Date of Birth"];
        weightTextfield.text = [userInfo objectForKey:@"Weight"];
        heightTextfield.text = [userInfo objectForKey:@"Height"];
        
        idTextField.enabled = !isSignedIn;
        countryTextfield.enabled = !isSignedIn;
        genderSegmentControl.enabled = !isSignedIn;
        dobTextField.enabled = !isSignedIn;
        weightTextfield.enabled = !isSignedIn;
        heightTextfield.enabled = !isSignedIn;
        
        accessCodeLabel.hidden = isSignedIn;
        accessCodeTextfield.enabled = !isSignedIn;
        accessCodeTextfield.hidden = isSignedIn;
        
        UIBarButtonItem *cancelBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(cancel:)];
        self.navigationItem.rightBarButtonItem = cancelBarButtonItem;
    }
}

- (void)registerKeyboardNotifications {
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
}

- (void)initializeContentViews {
    //make contentSize bigger than your scrollSize (you will need to figure out for your own use case)
    CGSize scrollContentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height*2);
    dataEntryScrollView.contentSize = scrollContentSize;
    [dataEntryScrollView setContentOffset:CGPointMake(0, 0)];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnScrollView)];
    [dataEntryScrollView addGestureRecognizer:tapGestureRecognizer];
}

- (void)initializeDatePicker {
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 162)];
    [self.datePicker addTarget:self action:@selector(datePickerValueDidChange:) forControlEvents:UIControlEventValueChanged];
    self.datePicker.date = [NSDate date];
    
    self.datePicker.backgroundColor = [UIColor whiteColor];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    //    self.datePicker.locale = [NSLocale localeWithLocaleIdentifier:<#(NSString *)#>] //TODO: Locales
    
    NSDate *referenceDate = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:referenceDate];
    
    const int maximumAge = 100;
    const int minimumAge = 13; //15; - Changed Fri22May2015
    
    dateComponents.year -= maximumAge;
    NSDate *MaximumAgeDate = [calendar dateFromComponents:dateComponents];
    dateComponents.year += maximumAge;
    
    dateComponents.year -= minimumAge;
    NSDate *MinimumAgeDate = [calendar dateFromComponents:dateComponents];
    dateComponents.year += minimumAge;
    
    dateComponents.year -= maximumAge;
    self.datePicker.minimumDate = MaximumAgeDate;//[[NSDate date] dateByAddingTimeInterval:-1*365*1*24*60*60];//Minimum Date
    dateComponents.year -= minimumAge;
    self.datePicker.maximumDate = MinimumAgeDate;//[[NSDate date] dateByAddingTimeInterval:+1*8*60*60];//Maximum Date
    self.datePicker.hidden = YES;
    
    [self.view addSubview:self.datePicker];
    
    self.datePickerIsShown = FALSE;
    
    UITapGestureRecognizer *dobEdit_tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editDOB)];
    [dobTextField addGestureRecognizer:dobEdit_tapGestureRecognizer];
}

- (void)initializeCountryPicker {
    self.countriesArray = [self sortedCountryArray];
    //    self.countriesArray = @[@"Australia", @"Spain"];
    //    self.countriesArray = @[@"Australia"];
    
    self.countryPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 0)];
    //    [self.countryPicker addTarget:self action:@selector(countryPickerValueDidChange:) forControlEvents:UIControlEventValueChanged];
    self.countryPicker.delegate = self;
    
    self.countryPicker.backgroundColor = [UIColor whiteColor];
    self.countryPicker.hidden = YES;
    
    [self.view addSubview:self.countryPicker];
    
    self.countryPickerIsShown = FALSE;
    
    UITapGestureRecognizer *countryEdit_tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editCountry)];
    [countryTextfield addGestureRecognizer:countryEdit_tapGestureRecognizer];
}

- (void)initializeTextFieldDelegates {
    idTextField.delegate = self;
    countryTextfield.delegate = self;
    dobTextField.delegate = self;
    weightTextfield.delegate = self;
    heightTextfield.delegate = self;
    accessCodeTextfield.delegate = self;
    
    [idTextField becomeFirstResponder];
}

- (void)initialize {
    [self loadUserData];
    [self registerKeyboardNotifications];
    keyboardIsShown = NO;
    [self initializeContentViews];
    [self initializeDatePicker];
    [self initializeCountryPicker];
    [self performSelector:@selector(initializeTextFieldDelegates) withObject:nil afterDelay:0];
}

#pragma mark - Helpers

- (NSArray *)sortedCountryArray {
    NSLocale *locale = [NSLocale currentLocale];
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    
    NSMutableArray *sortedCountryArray = [[NSMutableArray alloc] init];
    
    for (NSString *countryCode in countryArray) {
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [sortedCountryArray addObject:displayNameString];
        
    }
    
    [sortedCountryArray sortUsingSelector:@selector(localizedCompare:)];
    
    return [NSArray arrayWithArray:sortedCountryArray];
}

- (void)adjustDataEntryScrollViewForHeightOffset:(CGFloat)heightOffset {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [dataEntryScrollView setContentOffset:CGPointMake(0, heightOffset)];
    [UIView commitAnimations];
}

- (UIAlertView *)errorAlertViewWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    return alertView;
}

#pragma mark - Keyboard Event Handlers

- (void)keyboardWillShow:(NSNotification *)notification {
    CLSNSLog(@"keyboardWillShow");
    
    NSDictionary* userInfo = [notification userInfo];
    CGPoint keyboardOrigin = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin;
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.currentKeyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    UIView *firstResponder = nil;
    for (UIView *subView in [[dataEntryScrollView.subviews objectAtIndex:0] subviews]) {
        if ([subView isFirstResponder]) {
            firstResponder = subView;
        }
    }
    
    CLSNSLog(@"keyboardOrigin: %@", NSStringFromCGPoint(keyboardOrigin));
    CLSNSLog(@"firstResponder.frame.origin: %@", NSStringFromCGPoint(firstResponder.frame.origin));
    NSInteger heightOffset = (firstResponder.frame.origin.y - firstResponder.frame.size.height) - (keyboardOrigin.y - keyboardSize.height);
    if (heightOffset < 0) heightOffset = 0;
    
    [self adjustDataEntryScrollViewForHeightOffset:heightOffset];
    keyboardIsShown = YES;
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self adjustDataEntryScrollViewForHeightOffset:0];
    keyboardIsShown = NO;
}

#pragma mark - UITextField Delegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField {
    CLSNSLog(@"textFieldDidEndEditing");
    
    if (idTextField.text.length > 0) idTextField.text = [NSString stringWithFormat:@"%i", [idTextField.text intValue]];
    if (weightTextfield.text.length > 0) weightTextfield.text = [NSString stringWithFormat:@"%i", [weightTextfield.text intValue]];
    if (heightTextfield.text.length > 0) heightTextfield.text = [NSString stringWithFormat:@"%i", [heightTextfield.text intValue]];
    
    if ([idTextField.text isEqualToString:@"0"]) idTextField.text = @"";
    if ([weightTextfield.text isEqualToString:@"0"]) weightTextfield.text = @"";
    if ([heightTextfield.text isEqualToString:@"0"]) heightTextfield.text = @"";
    
    keyboardIsShown = NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField != dobTextField) {
        self.datePickerIsShown = TRUE;
        [self showDatePicker];
    }
    if (textField != countryTextfield) {
        self.countryPickerIsShown = TRUE;
        [self showCountryPicker];
    }
    
    if (keyboardIsShown) return;
    
    CGPoint keyboardOrigin = self.currentKeyboardFrame.origin;
    CGSize keyboardSize = self.currentKeyboardFrame.size;
    
    NSInteger heightOffset = (textField.frame.origin.y - textField.frame.size.height) - (keyboardOrigin.y - keyboardSize.height);
    if (heightOffset < 0) heightOffset = 0;
    
    [self adjustDataEntryScrollViewForHeightOffset:heightOffset];
    keyboardIsShown = YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField == dobTextField) {
        [self performSelector:@selector(editDOB) withObject:nil afterDelay:0];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        dobTextField.text = [[dateFormatter stringFromDate:self.datePicker.date] lowercaseString];
        
        return NO;
    }else if (textField == countryTextfield) {
        [self performSelector:@selector(editCountry) withObject:nil afterDelay:0];
        
        countryTextfield.text = [[self.countriesArray objectAtIndex:[self.countryPicker selectedRowInComponent:0]] capitalizedString];
        
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self resignAllFieldResponders:YES];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == accessCodeTextfield) return YES;
    
    // Check for non-numeric characters
    NSUInteger lengthOfString = string.length;
    for (NSInteger loopIndex = 0; loopIndex < lengthOfString; loopIndex++) {
        unichar character = [string characterAtIndex:loopIndex];
        if (character < 48) return NO; // 48 unichar for 0
        if (character > 57) return NO; // 57 unichar for 9
    }
    // Check for total length
    NSUInteger proposedNewLength = textField.text.length - range.length + string.length;
    if (textField == weightTextfield && proposedNewLength > 3) return NO;
    if (textField == heightTextfield && proposedNewLength > 3) return NO;
    if (textField == dobTextField && proposedNewLength > 3) return NO;
    if (proposedNewLength > 6) return NO;
    return YES;
}

#pragma mark - Miscellaneous

- (void)resignAllFieldResponders:(BOOL)shouldHidePickers {
    [idTextField resignFirstResponder];
    [countryTextfield resignFirstResponder];
    [dobTextField resignFirstResponder];
    [weightTextfield resignFirstResponder];
    [heightTextfield resignFirstResponder];
    [accessCodeTextfield resignFirstResponder];
    
    if (shouldHidePickers) {
        self.datePickerIsShown = FALSE;
        [self hideDatePicker:YES];
        
        self.countryPickerIsShown = FALSE;
        [self hideCountryPicker:YES];
    }
}

#pragma mark - Processors

- (void)saveParticipantInformation {
    UINavigationController *controller = (UINavigationController *)self.presentingViewController;
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^ {
        UINavigationController *sessionController = [self.storyboard instantiateViewControllerWithIdentifier:@"sessionController"];
        [controller presentViewController:sessionController animated:YES completion:nil];
    }];
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] initWithCapacity:5];
    
    NSInteger participantNumber = [idTextField.text integerValue];
    NSString *participantNumberString = [NSString stringWithFormat:@"%ld", (long)participantNumber];
    NSMutableString *participantID = [[NSMutableString alloc] initWithCapacity:6];
    for (int index = 0; index < 6-participantNumberString.length; index++) {
        [participantID appendString:@"0"];
    }
    [participantID appendString:participantNumberString];
    CLSNSLog(@"logged pID: %@", participantID);
    
    [userInfo setObject:participantID forKey:@"Participant ID"];
    [userInfo setObject:[NSString stringWithFormat:@"%@", countryTextfield.text] forKey:@"Country"];
    [userInfo setObject:[NSString stringWithFormat:@"%ld", (long)genderSegmentControl.selectedSegmentIndex+1] forKey:@"Gender"];
    [userInfo setObject:[NSString stringWithFormat:@"%@", dobTextField.text] forKey:@"Date of Birth"];
    [userInfo setObject:[NSString stringWithFormat:@"%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]] forKey:@"App Version"];
    [userInfo setObject:[NSString stringWithFormat:@"%@", weightTextfield.text] forKey:@"Weight"];
    [userInfo setObject:[NSString stringWithFormat:@"%@", heightTextfield.text] forKey:@"Height"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [userInfo setObject:[NSString stringWithFormat:@"%@", [[dateFormatter stringFromDate:[NSDate date]] lowercaseString]] forKey:@"First Sign-in Date"];
    
    [[UserDataProcessingEngine sharedEngine] saveUserDataWithUserInfoDictionary:userInfo];
}

#pragma mark UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    CLSNSLog(@"Tapped %li", (long)buttonIndex);
    if (alertView.tag == 245 && buttonIndex == 1) {
        [self saveParticipantInformation];
    }
}

#pragma mark UIDatePicker Event Handler

- (void)datePickerValueDidChange:(UIDatePicker *)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    dobTextField.text = [[dateFormatter stringFromDate:sender.date] lowercaseString];
}

#pragma mark Picker Show/Hide Methods

- (void)showDatePicker {
    self.datePicker.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^ {
        self.datePicker.frame = (CGRect){0, self.view.frame.size.height-216, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
        self.countryPicker.hidden = YES;
    }];
}

- (void)hideDatePicker:(BOOL)animated {
    [UIView animateWithDuration:0.3 animations:^ {
        self.datePicker.frame = (CGRect){0, self.view.frame.size.height, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
    } completion:^(BOOL completion) {
        self.datePicker.hidden = YES;
    }];
}

- (void)showCountryPicker {
    self.countryPicker.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^ {
        self.countryPicker.frame = (CGRect){0, self.view.frame.size.height-216, self.countryPicker.frame.size.width, self.countryPicker.frame.size.height};
        self.datePicker.hidden = YES;
    }];
}

- (void)hideCountryPicker:(BOOL)animated {
    [UIView animateWithDuration:0.3 animations:^ {
        self.countryPicker.frame = (CGRect){0, self.view.frame.size.height, self.countryPicker.frame.size.width, self.countryPicker.frame.size.height};
    } completion:^(BOOL completion) {
        self.countryPicker.hidden = YES;
    }];
}

#pragma mark Picker Field Selection Handler

- (void)editDOB {
    [self resignAllFieldResponders:NO];
    
    self.datePickerIsShown = TRUE;
    [self showDatePicker];
}

- (void)editCountry {
    [self resignAllFieldResponders:NO];
    
    self.countryPickerIsShown = TRUE;
    [self showCountryPicker];
}

#pragma mark - UIPickerView Delegate Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.countriesArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.countriesArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    countryTextfield.text = [[self.countriesArray objectAtIndex:[self.countryPicker selectedRowInComponent:0]] capitalizedString];
}

@end
