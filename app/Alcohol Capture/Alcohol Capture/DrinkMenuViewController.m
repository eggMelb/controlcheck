//
//  DrinkMenuViewController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 25/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "DrinkMenuViewController.h"

@interface DrinkMenuViewController ()

@end

@implementation DrinkMenuViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    CGFloat offset = 114;
    CGFloat menuHeight = [UIScreen mainScreen].bounds.size.height-offset;
    CGFloat menuWidth = [UIScreen mainScreen].bounds.size.width;
    
    menuScrollView.contentSize = (CGSize){menuWidth*2, menuHeight};
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(menuWidth, 0, menuWidth, menuHeight)];
    view.backgroundColor = [UIColor orangeColor];
    view.userInteractionEnabled = NO;
    [menuScrollView addSubview:view];
    
    menuScrollView.scrollEnabled = NO;
    
    menuScrollView.bounces = NO;
    menuScrollView.canCancelContentTouches = YES;
    menuScrollView.delaysContentTouches = NO;
    
    CLSNSLog(@"TESTING8: %@", NSStringFromCGRect(menuScrollView.frame));
    CGFloat buttonWidth = (menuWidth/3) - 2;
    CGFloat buttonHeight = (menuHeight/3) - 2;
    
    NSArray *availableOptions = [[DrinkDataRetrievalEngine sharedEngine] drinkTypes];
    
    for (int i=0; i<9; i++) {
        DrinkMenuItemButton *button = [DrinkMenuItemButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor whiteColor];
        [button setTitle:@"Drink Name" forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        button.frame = CGRectMake((buttonWidth*(i%3))+(i%3), buttonHeight*((i-(i%3))/3)+((i-(i%3))/3), buttonWidth, buttonHeight);
        
        button.exclusiveTouch = YES;
        
        CLSNSLog(@"UIScreen: %f", [UIScreen mainScreen].bounds.size.height);
        float scaling = ([UIScreen mainScreen].bounds.size.height == 480) ? 0.5 : 1.0;
        
        NSString *drinkType = [availableOptions objectAtIndex:i];
        
        [button setTitle:drinkType forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"956-wine-glass@2x.png"] forState:UIControlStateNormal];
        
//        if (i == 1) button.drinkStrengthLevel = 0.6f;
//        else if (i == 2) button.drinkStrengthLevel = 0.3f;
        if (i == 1) button.drinkStrengthLevel = 0.85f;
        else if (i == 2) button.drinkStrengthLevel = 0.7f;
        
        if ([drinkType rangeOfString:@"Beer"].location != NSNotFound) {
            [button setImage:[UIImage imageNamed:@"beer.png"] forState:UIControlStateNormal];
        }else if ([drinkType rangeOfString:@"Wine"].location != NSNotFound) {
            if ([drinkType rangeOfString:@"Red"].location != NSNotFound) {
                [button setImage:[UIImage imageNamed:@"redwine.png"] forState:UIControlStateNormal];
            }else if ([drinkType rangeOfString:@"White"].location != NSNotFound) {
                [button setImage:[UIImage imageNamed:@"whitewine.png"] forState:UIControlStateNormal];
            }else if ([drinkType rangeOfString:@"Fortified"].location != NSNotFound) {
                [button setImage:[UIImage imageNamed:@"fortifiedwine.png"] forState:UIControlStateNormal];
            }
        }else if ([drinkType rangeOfString:@"Cocktail"].location != NSNotFound) {
            [button setImage:[UIImage imageNamed:@"cocktail.png"] forState:UIControlStateNormal];
        }else if ([drinkType rangeOfString:@"Liqueur"].location != NSNotFound) {
            [button setImage:[UIImage imageNamed:@"spiritliquerglass.png"] forState:UIControlStateNormal];
        }else if ([drinkType rangeOfString:@"Cider"].location != NSNotFound) {
            [button setImage:[UIImage imageNamed:@"premixdrink.png"] forState:UIControlStateNormal];
        }
        
        if (scaling == 0.5) {
            UIImage *tempImage = [self imageWithImage:[button imageForState:UIControlStateNormal] scaledToSize:button.imageView.image.size];
            
            UIImage *newImage = [self imageWithImage:tempImage scaledToSize:CGSizeMake(tempImage.size.width*0.5, tempImage.size.height*0.5)];
            [button setImage:newImage forState:UIControlStateNormal];
        }
        
        [button setTitleColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        [button.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
        
        {
            // the space between the image and text
            CGFloat spacing = 15.0;
            float   textMargin = 6;
            
            // get the size of the elements here for readability
            CGSize  imageSize   = button.imageView.image.size;
            CGSize  titleSize   = button.titleLabel.frame.size;
            CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);      // get the height they will take up as a unit
            
            // lower the text and push it left to center it
            button.titleEdgeInsets = UIEdgeInsetsMake( 0.0, -imageSize.width +textMargin, - (totalHeight - titleSize.height), +textMargin );   // top, left, bottom, right
            
            // the text width might have changed (in case it was shortened before due to
            // lack of space and isn't anymore now), so we get the frame size again
            titleSize = button.titleLabel.bounds.size;
            
            button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width );     // top, left, bottom, right
        }
        
        {
            button.titleLabel.numberOfLines = 2;
            button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            button.titleLabel.textAlignment = NSTextAlignmentCenter;
        }
        
        [button addTarget:self action:@selector(selectDrinkType:) forControlEvents:UIControlEventTouchUpInside];
        
        [menuScrollView addSubview:button];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GUI Event Handlers

- (void)selectDrinkType:(UIButton *)sender {
    //Property Selection
    DrinkPropertiesTableViewController *pTVC = ((DrinkPropertiesTableViewController *)[(UINavigationController *)self.presentingViewController topViewController]);
    CLSNSLog(@"CLASS: %@", NSStringFromClass(pTVC.class));
    
    if (![pTVC.drink.type isEqualToString:sender.titleLabel.text]) {
        pTVC.drink = [Drink new];
        pTVC.drink.type = @"";
        pTVC.drink.quantity = 0;
        pTVC.drink.alcoholPercentage = @"0%";
        pTVC.drink.size = 0;
    }
    
    [pTVC.drink setType:sender.titleLabel.text];//TODO: ADD SAFETY NET
    
    NSDictionary *drinkInfo = [[DrinkDataRetrievalEngine sharedEngine] infoForDrinkType:sender.titleLabel.text];
    [pTVC.drink setAlcoholPercentage:[NSString stringWithFormat:@"%@%%", [drinkInfo objectForKey:@"AlcoholPercentage"]]];//TODO: ADD SAFETY NET
    pTVC.sizeDataArray = [[DrinkDataRetrievalEngine sharedEngine] descriptionSizesForDrinkType:sender.titleLabel.text withOrder:YES];//TODO: ADD SAFETY NET
    
    if (pTVC.sizeDataArray.count == 1) {//Autofills if only 1 drink size option is available
        [pTVC.drink setSize:[[DrinkDataRetrievalEngine sharedEngine] millilitresForDrinkType:sender.titleLabel.text andDescriptionSizes:pTVC.sizeDataArray[0]]];
    }
    
    CLSNSLog(@"setDrink: %@", pTVC.drink);
    [pTVC reloadProperties];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancel:(id)sender {
    CLSNSLog(@"dismissed: %@", NSStringFromClass(self.presentingViewController.class));
    UINavigationController *controller = (UINavigationController *)self.presentingViewController;
    
    [self dismissViewControllerAnimated:YES completion:^ {
        if (self.isAutomated) [controller popViewControllerAnimated:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Helpers

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
