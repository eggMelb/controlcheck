//
//  AppDelegate.m
//  Alcohol Capture
//
//  Created by Jason Pan on 12/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#define ToDoItemKey @"1HourPassed"
#import "ServerSideProcessingEngine.h"

#import <Crashlytics/Crashlytics.h>

#import <CloudKit/CloudKit.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [ServerSideProcessingEngine sharedEngine];
    [UserDataProcessingEngine sharedEngine];
    
    DBAccountManager *accountManager = [[DBAccountManager alloc] initWithAppKey:APP_KEY secret:APP_SECRET];
    [DBAccountManager setSharedManager:accountManager];
    
    [Crashlytics startWithAPIKey:@"6a068044907e05dfad3d8936eec2eb0b9e575389"];
    if ([UserDataProcessingEngine sharedEngine].userDataDictionary) {
        NSString *currentParticipantID = [NSString stringWithFormat:@"%@", [[UserDataProcessingEngine sharedEngine].userDataDictionary objectForKey:@"Participant ID"]];
        CLSNSLog(@"Logging crashlytics ParticipantID: %@", currentParticipantID);
//        NSString *currentParticipantCountry  = [NSString stringWithFormat:@"%@", [[UserDataProcessingEngine sharedEngine].userDataDictionary objectForKey:@"Country"]];
        
        [[Crashlytics sharedInstance] setUserIdentifier:currentParticipantID];
//        [[Crashlytics sharedInstance] setValue:currentParticipantCountry forKey:@"Country"];
        
        [[Crashlytics sharedInstance] setObjectValue:currentAppStatus forKey:@"Release Cycle"];
        for (NSString *key in [UserDataProcessingEngine sharedEngine].userDataDictionary.allKeys) {
            NSString *currentObject = [NSString stringWithFormat:@"%@", [[UserDataProcessingEngine sharedEngine].userDataDictionary objectForKey:key]];
            
            [[Crashlytics sharedInstance] setObjectValue:currentObject forKey:key];
        }
        
//        [[Crashlytics sharedInstance] setObjectValue:currentParticipantCountry forKey:@"Country"];
//        [[Crashlytics sharedInstance] setObjectValue:currentParticipantCountry forKey:@"Country"];
    }
    
    // None of the code should even be compiled unless the Base SDK is iOS 8.0 or later
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    // The following line must only run under iOS 8. This runtime check prevents
    // it from running if it doesn't exist (such as running under iOS 7 or earlier).
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
//        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
        
        UIUserNotificationSettings *notificationSettings = [application currentUserNotificationSettings];
        CLSNSLog(@"notificationSettings; %@", notificationSettings);
        if (notificationSettings.types == UIUserNotificationTypeNone) {
            CLSNSLog(@"no notfications...request");
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Notifications" message:NSLocalizedString(@"NOTIFICATIONS_REQUEST_PREALERT_MESSAGE", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alertView.tag = 7612;
            [alertView show];
        }
        //redundant, but anyway - logged Sat28Mar12:34:18am (checking for notification bug in trial 2)
        else {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
        }
    }
#endif
    
    [application registerForRemoteNotifications];
    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 7612) {
        CLSNSLog(@"Show notifications request.");
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
}

//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
//    if (notification) [self processNotification:notification withStatus:@"Background"];
//}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"didReceiveRemoteNotification");
    
    CKQueryNotification *cloudKitNotification = [CKQueryNotification notificationFromRemoteNotificationDictionary:userInfo];
    NSLog(@"cloudKitNotification: %@", cloudKitNotification);
    
    NSDictionary *recordFields = cloudKitNotification.recordFields;
    NSLog(@"recordFields2: %@", recordFields);
    
//    LatestBuildVersion
    if ([recordFields.allKeys containsObject:@"LatestAppVersion"]) {
        double liveAppVersion = [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] doubleValue];
        double latestAppVersion = [[recordFields objectForKey:@"LatestAppVersion"] doubleValue];
        
        if (latestAppVersion > liveAppVersion) {
            [[ServerSideProcessingEngine sharedEngine] showUpdateAlertView];
        }else {
            [[ServerSideProcessingEngine sharedEngine] hideUpdateAlertView];
        }
    }
    
//    [[ServerSideProcessingEngine sharedEngine] showUpdateAlertView];
//    [[ServerSideProcessingEngine sharedEngine] featchInfor];
    
    
    CKModifyBadgeOperation *badgeResetOperation = [[CKModifyBadgeOperation alloc] initWithBadgeValue:0];
    badgeResetOperation.container = [CKContainer defaultContainer];
    badgeResetOperation.modifyBadgeCompletionBlock = ^(NSError *error) {
        NSLog(@"trig");
        if (error != nil) {
            NSLog(@"Error resetting badge: %@", error);
        }else {
            NSLog(@"Sucessful: %@", error);
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        }
    };
    [[CKContainer defaultContainer] addOperation:badgeResetOperation];
//    [badgeResetOperation start];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    CLSNSLog(@"applicationWillResignActive");
    [self scheduleNotifications];
    [[UserDataProcessingEngine sharedEngine] setIsUserFirstStartup:NO];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    CLSNSLog(@"applicationDidEnterBackground");
    //Made redundant - Fri3Apr42246pm 2015
//    [self scheduleNotifications];
//    [[UserDataProcessingEngine sharedEngine] setIsUserFirstStartup:NO];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    CLSNSLog(@"applicationDidBecomeActive");
    application.applicationIconBadgeNumber = 0;
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
//    [self scheduleNotifications];
    
//    CKModifyBadgeOperation *badgeResetOperation = [[CKModifyBadgeOperation alloc] initWithBadgeValue:0];
//    badgeResetOperation.modifyBadgeCompletionBlock = ^(NSError *error) {
//        if (error != nil) {
//            CLSLog(@"Error resetting badge: %@", error);
//        } else {
//            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//        }
//    };
//    [[CKContainer defaultContainer] addOperation:badgeResetOperation];
    
    
    [[ServerSideProcessingEngine sharedEngine] featchInfor];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    CLSNSLog(@"applicationWillTerminate");
    //Made redundant - Fri3Apr42246pm 2015
//    [self scheduleNotifications];
//    [[UserDataProcessingEngine sharedEngine] setIsUserFirstStartup:NO];
}

-(NSArray *)scheduleNotificationForNotificationIndex:(NSInteger)notificationIndex forNumberOfDays:(NSInteger)numberOfDays {
    NSMutableArray *notificationArray = [NSMutableArray array];
    
    NSString *notificationID = [NSString stringWithFormat:@"%li", (long)notificationIndex];
    
    NSString *notificationKey = [NSString stringWithFormat:@"Notification %@", notificationID];
    
    NSDate *initialDate;
    NSString *finalDateString;
    NSString *hourString;
    NSString *minuteString;
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"hh:mm a"];
        
        NSString *notification = [[[UserDataProcessingEngine sharedEngine] notificationsPreferences] objectForKey:notificationKey];
        initialDate = [dateFormatter dateFromString:notification];
    }
    
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm"];
        
        finalDateString = [dateFormatter stringFromDate:initialDate];
        
        hourString = [finalDateString substringToIndex:[finalDateString rangeOfString:@":"].location];
        minuteString = [finalDateString substringFromIndex:[finalDateString rangeOfString:@":"].location+1];
    }
    
    CLSNSLog(@"notificationKey: %@", notificationKey);
    CLSNSLog(@"HOUR: %@", hourString);
    CLSNSLog(@"MINUTE: %@", minuteString);
    BOOL didAddFirstTime = FALSE;
    
    for (int index = 1; index <= numberOfDays; index++) {
        NSDate *followingTargetDay;
        {
            NSDate *referenceDate = [NSDate date];
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
            NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:referenceDate];
            
            CLSNSLog(@"dateComponents.hour: %li", (long)dateComponents.hour);
            CLSNSLog(@"hour: %li", (long)[hourString integerValue]);
            CLSNSLog(@"dateComponents.minute: %li", (long)dateComponents.minute);
            CLSNSLog(@"minute: %li", (long)[minuteString integerValue]);
            if (index == 1) {
                if (dateComponents.hour >= [hourString integerValue] && dateComponents.minute >= [minuteString integerValue]) {
                    dateComponents.day += index;
                }else {
                    didAddFirstTime = TRUE;
                }
            }else {
                if (didAddFirstTime) dateComponents.day += index-1;
                else dateComponents.day += index;
            }
            
            dateComponents.hour = [hourString integerValue];
            dateComponents.minute = [minuteString integerValue];
            dateComponents.second = 0;
            
            followingTargetDay = [calendar dateFromComponents:dateComponents];
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timezone = [NSTimeZone localTimeZone];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [dateFormatter setTimeZone:timezone];
        
        CLSNSLog(@"logging days: %@", [dateFormatter stringFromDate:followingTargetDay]);
        
        //Expiry check - Implemented Saturday 7th March 11:12:37pm @in 2015
        int maximumStudyPeriod = [[[UserDataProcessingEngine sharedEngine].appSettingsDictionary objectForKey:@"Maximum Study Period (Days)"] intValue];
        NSDate *originalDate = [[UserDataProcessingEngine sharedEngine] userFirstSignInDate];
        NSDate *expirationDate = [NSDate dateWithTimeInterval:maximumStudyPeriod*24*60*60 sinceDate:originalDate];
        
        if ([expirationDate timeIntervalSince1970] < [followingTargetDay timeIntervalSince1970]) {
            break;
        }
        //Expiry check
        
        //        //Create local notification
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = followingTargetDay;
        notification.alertBody = NSLocalizedString(@"NOTIFICATIONS_REMINDER_MESSAGE", nil);
        notification.applicationIconBadgeNumber = 1;
        notification.timeZone = [NSTimeZone defaultTimeZone];
//        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        [notificationArray addObject:notification];
    }
//    [[UIApplication sharedApplication] setScheduledLocalNotifications:notificationArray];
    return [notificationArray copy];
}

- (void)scheduleStudyPeriodExpiryNotificationWithMaximumStudyPeriod:(NSInteger)days {
//    NSDate *followingTargetDay;
//    {
//        NSDate *referenceDate = [NSDate date];
//        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//        NSDateComponents *dateComponents = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:referenceDate];
//        
//        CLSNSLog(@"dateComponents.hour: %li", (long)dateComponents.hour);
//        CLSNSLog(@"hour: %li", (long)[hourString integerValue]);
//        CLSNSLog(@"dateComponents.minute: %li", (long)dateComponents.minute);
//        CLSNSLog(@"minute: %li", (long)[minuteString integerValue]);
//        dateComponents.day += index;
//        
//        dateComponents.hour = [hourString integerValue];
//        dateComponents.minute = [minuteString integerValue];
//        dateComponents.second = 0;
//        
//        followingTargetDay = [calendar dateFromComponents:dateComponents];
//    }
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    NSTimeZone *timezone = [NSTimeZone localTimeZone];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    [dateFormatter setTimeZone:timezone];
//    
//    CLSNSLog(@"logging days: %@", [dateFormatter stringFromDate:followingTargetDay]);
    
    
    NSDate *originalDate = [[UserDataProcessingEngine sharedEngine] userFirstSignInDate];
    
//    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
//    NSDate *expirationDay = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:originalDate options:0];
    
//    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:originalDate];
//    [dateComponents setDay:29];
//    NSDate *expirationDay = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
//    CLSNSLog(@"originalDate: %@", originalDate);
    NSDate *expirationDay = [NSDate dateWithTimeInterval:days*24*60*60 sinceDate:originalDate];
//    CLSNSLog(@"expirationDay: %@", expirationDay);
    
//  Create local notification
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = expirationDay;
    notification.alertBody = NSLocalizedString(@"NOTIFICATIONS_RESEARCH_PERIOD_FINISHED_MESSAGE", nil);
    notification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
}

-(void)scheduleNotifications {
    if (![[UserDataProcessingEngine sharedEngine] isUserSignedIn]) return;
//    if (![[UserDataProcessingEngine sharedEngine] isStudyPeriodExpired]) return;//Bug found - logged Sat28Mar12:30:23am
    if ([[UserDataProcessingEngine sharedEngine] isStudyPeriodExpired]) return;
    
    NSLog(@"creating notifications...");
//    return;
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    int numberOfNotifications = (int)[[UserDataProcessingEngine sharedEngine] notificationsPreferences].count;
    int numberOfDays = [[[UserDataProcessingEngine sharedEngine].appSettingsDictionary objectForKey:@"Notification Days (in advance)"] intValue];
    
    NSMutableArray *notificationsArray = [NSMutableArray array];
    
    for (int index = 0; index < numberOfNotifications; index++) {
        NSArray *tempArray = [self scheduleNotificationForNotificationIndex:index+1 forNumberOfDays:numberOfDays];
        
        for (NSObject *object in tempArray) [notificationsArray addObject:object];
    }
    [[UIApplication sharedApplication] setScheduledLocalNotifications:notificationsArray];
    
    int maximumStudyPeriod = [[[UserDataProcessingEngine sharedEngine].appSettingsDictionary objectForKey:@"Maximum Study Period (Days)"] intValue];
    [self scheduleStudyPeriodExpiryNotificationWithMaximumStudyPeriod:maximumStudyPeriod];
    
    [self performSelector:@selector(test) withObject:nil afterDelay:0];
//    CLSNSLog(@"scheduledLocalNotifications (unstable): %@", [[UIApplication sharedApplication] scheduledLocalNotifications]);
}

- (void)test {
    for (NSObject *object in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        CLSNSLog(@"scheduled: %@", object);
    }
}

//-(void)processNotification:(UILocalNotification *)notification withStatus:(NSString *)status {
//    //TODO: WARNING! CRASHES DUE TO DROPBOX! (or could cause errors)
////    if (notification) {
////        CLSNSLog(@"localNotif");
////        NSString *itemName = [notification.userInfo objectForKey:ToDoItemKey];
////        //        [(HomeViewController *)self.window.rootViewController displayItem:itemName];  // custom method
////        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;//localNotif.applicationIconBadgeNumber-1;
////        
////        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"LocalNotif: %@", status] message:@"Working" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
////        [alertView show];
////        
////        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
////        [[UIApplication sharedApplication] cancelAllLocalNotifications];
////        
////        ((HomeViewController *)[(UINavigationController *)self.window.rootViewController topViewController]).status = @"Notification";
////        if ([status isEqualToString:@"Termination"]) [(UINavigationController *)self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
////    }
//}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url sourceApplication:(NSString *)source annotation:(id)annotation {
    DBAccount *account = [[DBAccountManager sharedManager] handleOpenURL:url];
    if (account) {
        CLSNSLog(@"Dropbox: App linked successfully! :%@", account.userId);
        return YES;
    }
    CLSNSLog(@"Dropbox: Error occured while linking account! :%@", account.userId);
    return NO;
}

@end
