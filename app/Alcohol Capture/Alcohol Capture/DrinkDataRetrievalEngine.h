//
//  DrinkDataRetrievalEngine.h
//  Alcohol Capture
//
//  Created by Jason Pan on 29/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DrinkDataRetrievalEngine : NSObject

@property (nonatomic, strong) NSArray *drinkDataArray;
@property (nonatomic, strong) NSDictionary *drinkDataDictionary;

+(DrinkDataRetrievalEngine *)sharedEngine;

-(void)reloadData;
-(NSArray *)drinkTypes;
-(NSDictionary *)drinksDictionary;
-(NSDictionary *)infoForDrinkType:(NSString *)drinkType;

-(NSDictionary *)sizesForDrinkType:(NSString *)drinkType;
-(NSArray *)descriptionSizesForDrinkType:(NSString *)drinkType;

-(NSInteger)millilitresForDrinkType:(NSString *)drinkType andSize:(NSString *)size;
-(NSInteger)millilitresForDrinkType:(NSString *)drinkType andDescriptionSizes:(NSString *)descriptionSize;

-(NSString *)descriptionSizeForDrinkType:(NSString *)drinkType andSize:(NSString *)size;
-(NSArray *)descriptionSizesForDrinkType:(NSString *)drinkType withOrder:(BOOL)shouldSort;

-(NSArray *)drinkDescriptionsForDrinkType:(NSString *)drinkType;
-(NSString *)drinkDescriptionForDrinkType:(NSString *)drinkType andSize:(NSString *)size;

@end
