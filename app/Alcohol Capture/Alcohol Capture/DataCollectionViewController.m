//
//  DataCollectionViewController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 25/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "DataCollectionViewController.h"
//#import <Crashlytics/Crashlytics.h>

@interface DataCollectionViewController ()

@end

@implementation DataCollectionViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad { //Bug fix record
    [super viewDidLoad];
//    [[Crashlytics sharedInstance] crash];
    // Do any additional setup after loading the view.
    
    if ([[UserDataProcessingEngine sharedEngine] isUserFirstStartup]) {
        promptLabel.text = NSLocalizedString(@"STARTSCREEN_WELCOME_MESSAGE", nil);
    }else {
        promptLabel.text = NSLocalizedString(@"STARTSCREEN_CONSUMPTION_MESSAGE", nil);
    }
    
    if ([[UserDataProcessingEngine sharedEngine] isStudyPeriodExpired]) {
        CLSNSLog(@"Displaying Participant Summary Information...");
        NSDictionary *summaryInformationDictionary = [[UserDataProcessingEngine sharedEngine] retrieveSummaryInformation];
        
        CGFloat numberOfStandardDrinks = [[summaryInformationDictionary objectForKey:@"numberOfStandardDrinks"] floatValue];
        int numberOfDrinkingDays = [[summaryInformationDictionary objectForKey:@"numberOfDrinkingDays"] intValue];
        CGFloat averageStandardDrinksPerDay = [[summaryInformationDictionary objectForKey:@"averageStandardDrinksPerDay"] floatValue];
        
//        if (averageStandardDrinksPerDay == INFINITY || averageStandardDrinksPerDay == NAN) averageStandardDrinksPerDay = 0;//Doesn't work
//        if (averageStandardDrinksPerDay == INFINITY || isnan(averageStandardDrinksPerDay)) averageStandardDrinksPerDay = 0;//Works
        if (!isnormal(averageStandardDrinksPerDay)) averageStandardDrinksPerDay = 0;//Works
        
        CLSNSLog(@"numberOfStandardDrinks: %1.2f", numberOfStandardDrinks);
        CLSNSLog(@"numberOfDrinkingDays: %i", numberOfDrinkingDays);
        CLSNSLog(@"averageStandardDrinksPerDay: %1.2f", averageStandardDrinksPerDay);
        
        promptLabel.numberOfLines = 25;
        promptLabel.font = [UIFont fontWithName:promptLabel.font.fontName size:16.0f];
        promptLabel.textAlignment = NSTextAlignmentCenter;
        
        NSMutableString *mutableString = [NSMutableString stringWithFormat:@"Number of Standard Drinks:\n%@ drinks\n\nNumber of Drinking Days:\n%@ days\n\nAverage Standard Drinks per day:\n%@ drinks",
                                          [NSString stringWithFormat:@"%1.2f", numberOfStandardDrinks],
                                          [NSString stringWithFormat:@"%i", numberOfDrinkingDays],
                                          [NSString stringWithFormat:@"%1.2f", averageStandardDrinksPerDay]];
        
//        promptLabel.text = [mutableString copy]; //Removed Fri22May2015
        promptLabel.text = @"";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Your info summary" message:mutableString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        if (![[UserDataProcessingEngine sharedEngine] isSessionDataUnavailable]) [alertView show];
        
        for (UIButton *subview in self.view.subviews) {
            if (subview != (UIButton *)promptLabel && [NSStringFromClass(subview.class) isEqualToString:@"UIButton"]) {
                if ([subview.titleLabel.text isEqualToString:@"Yes"] || [subview.titleLabel.text isEqualToString:@"No"]) {
                    [subview removeFromSuperview];
                }
            }
        }
        
        [self performSelector:@selector(rearrangeViewsForResearchExpiryInformation) withObject:nil afterDelay:0];
        promptLabel.textColor = [UIColor blackColor];
        
        [[UserDataProcessingEngine sharedEngine] expireData];
    }
    
    for (UIView *subview in self.view.subviews) {
        subview.exclusiveTouch = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self performSelector:@selector(verifyDropbox) withObject:nil afterDelay:0.5f];
}

#pragma mark - UI Event Handlers

- (IBAction)drinkingNo:(id)sender {
    [self submitNoDrinkingSessionData];
    
    [(HomeViewController *)[[(UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController viewControllers] objectAtIndex:0] showDismissLabel];
    CLSNSLog(@"CLASS: %@", NSStringFromClass([[[(UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController viewControllers] objectAtIndex:0] class]));
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark GUI Setup (For expiration of research period)

//- (void)rearrangeViewsForResearchExpiryInformation {
//    NSLog(@"cns: %@", promptLabel.constraints);
//    
//    UIFont *font = [promptLabel.font copy];
//    
//    //        promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height-100)/2, self.view.frame.size.width, 100)];
//    promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
//    
//    promptLabel.numberOfLines = 5;
//    promptLabel.font = font;
//    promptLabel.textAlignment = NSTextAlignmentCenter;
//    
//    promptLabel.text = [[NSMutableString stringWithFormat:@"The research period has now ended.\nThank you for participating."] mutableCopy];
//    //        promptLabel.center = self.view.center;
//    //        promptLabel.frame = CGRectMake((self.view.frame.size.width-promptLabel.frame.size.width)/2, (self.view.frame.size.height-promptLabel.frame.size.height)/2, promptLabel.frame.size.width, promptLabel.frame.size.height);
//    
//    [self.view addSubview:promptLabel];
//}

CGFloat bottomY;
- (void)rearrangeViewsForResearchExpiryInformation {
    promptLabel.frame = CGRectMake(0, 0, self.view.frame.size.width-50, self.view.frame.size.width-50);
    
    CGSize rsize = [promptLabel.text sizeWithAttributes:@{NSFontAttributeName: promptLabel.font}];
    promptLabel.frame = CGRectMake(0, 0, rsize.width, rsize.height);
    promptLabel.center = self.view.center;
    bottomY = promptLabel.frame.origin.y + promptLabel.frame.size.height;
    
    [self performSelector:@selector(rearrangeViewsForResearchExpiryInformation_Part2) withObject:nil afterDelay:0];
}

- (void)rearrangeViewsForResearchExpiryInformation_Part2 {
//    NSMutableString *mutableString = [NSMutableString stringWithFormat:@"The research period has now ended.\nThank you for participating.\n\n\n\n\n%@", promptLabel.text];
    NSMutableString *mutableString = [NSMutableString stringWithFormat:@"%@\n\n\n\n\n%@", NSLocalizedString(@"STARTSCREEN_RESEARCH_PERIOD_FINISHED_MESSAGE", nil), promptLabel.text];
    promptLabel.text = [mutableString copy];
    [self performSelector:@selector(rearrangeViewsForResearchExpiryInformation_Part3) withObject:nil afterDelay:0];
}

- (void)rearrangeViewsForResearchExpiryInformation_Part3 {
    CGSize rsize = [promptLabel.text sizeWithAttributes:@{NSFontAttributeName: promptLabel.font}];
    promptLabel.frame = CGRectMake(promptLabel.frame.origin.x, promptLabel.frame.origin.y-(rsize.height-promptLabel.frame.size.height), promptLabel.frame.size.width, bottomY+100);
    
    promptLabel.textColor = [UIColor whiteColor];
}

#pragma mark Processors

- (void)verifyDropbox {
    DBAccount *account = [[DBAccountManager sharedManager] linkedAccount];
    
    if (account) {
        if ([DBFilesystem sharedFilesystem] == NULL) {
            CLSNSLog(@"Dropbox: Account already linked: %@", account.userId);
            DBFilesystem *filesystem = [[DBFilesystem alloc] initWithAccount:account];
            [DBFilesystem setSharedFilesystem:filesystem];
        }
    }else {
        CLSNSLog(@"Dropbox: Account doesn't exist");
        NSString *APP_KEY = @"12qdorcjgnf88ic";
        
        NSString *access_token = @"cfebnn42ponio11n";
        NSString *access_secret = @"p1204uio7lanrmv";
        
        NSString *uid = @"364502700";
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"9b0aa24b0bd50ce3a1a904db9d309c50" forKey:@"dropbox.sync.nonce"];
        [userDefaults synchronize];
        
        NSString *urlString =
        [NSString stringWithFormat:@"db-%@://1/connect?oauth_token=%@&oauth_token_secret=%@&uid=%@&state=9b0aa24b0bd50ce3a1a904db9d309c50", APP_KEY, access_token, access_secret, uid];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
}

- (void)submitNoDrinkingSessionData {
    [self verifyDropbox];
    
    [[UserDataProcessingEngine sharedEngine] loadBaseDictionary];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    NSString *submissionStartDate = [dateFormatter stringFromDate:[NSDate date]];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *submissionTimeString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSMutableDictionary *sessionDictionary = [[NSMutableDictionary alloc] initWithCapacity:8];
    [sessionDictionary setObject:submissionStartDate forKey:@"Submission Date"];
    [sessionDictionary setObject:submissionTimeString forKey:@"Submission Time"];
    [sessionDictionary setObject:@"N/A" forKey:@"Session Start Date"];
    [sessionDictionary setObject:@"N/A" forKey:@"Session End Date"];
    [sessionDictionary setObject:@"N/A" forKey:@"Time: Commence Drinking"];
    [sessionDictionary setObject:@"N/A" forKey:@"Time: Finish Drinking"];
    [sessionDictionary setObject:@"N/A" forKey:@"Total Drinking Time (minutes)"];
    [sessionDictionary setObject:@"N/A" forKey:@"Total Drinking Time (hours)"];
    [sessionDictionary setObject:@"N/A" forKey:@"Total Drinks"];
    
    NSMutableDictionary *numberOfDrinksDictionary = [[NSMutableDictionary alloc] initWithCapacity:[[DrinkDataRetrievalEngine sharedEngine] drinkTypes].count];
    for (int index = 0; index < [[DrinkDataRetrievalEngine sharedEngine] drinkTypes].count; index++) {
        NSString *currentDrinkType = [NSString stringWithFormat:@"%@", [[[DrinkDataRetrievalEngine sharedEngine] drinkTypes] objectAtIndex:index]];
        NSArray *currentDescriptionSizeArray = [[DrinkDataRetrievalEngine sharedEngine] drinkDescriptionsForDrinkType:currentDrinkType];
        
        for (int sizeIndex = 0; sizeIndex < currentDescriptionSizeArray.count; sizeIndex++) {
            NSString *currentSizeDescription = [NSString stringWithFormat:@"%@", [currentDescriptionSizeArray objectAtIndex:sizeIndex]];
            [numberOfDrinksDictionary setObject:@"0" forKey:currentSizeDescription];
        }
    }
    
    CLSNSLog(@"data to be saved: %@        %@      %@", numberOfDrinksDictionary, [[UserDataProcessingEngine sharedEngine] userData], sessionDictionary);
    
    [[UserDataProcessingEngine sharedEngine] saveDataWithNumberOfDrinks:numberOfDrinksDictionary andUserData:[[UserDataProcessingEngine sharedEngine] userData] andSessionData:sessionDictionary];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
