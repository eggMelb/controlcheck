//
//  HomeViewController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 19/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Register application notification handlers
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationIsActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackgroundNotification:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    BOOL isSignedIn = [[UserDataProcessingEngine sharedEngine] isUserSignedIn];
    
    //Check if user has entered participant information
    if (!isSignedIn) {
        [self performSegueWithIdentifier:@"showSetupViewController" sender:self];
        return;
    }
    
    [self performSelector:@selector(testDropbox) withObject:nil afterDelay:0.5];
    
    UINavigationController *sessionController = [self.storyboard instantiateViewControllerWithIdentifier:@"sessionController"];
    [self presentViewController:sessionController animated:NO completion:nil];
    
    //Set exclusive touch
    for (UIView *subview in self.view.subviews) {
        subview.exclusiveTouch = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//GUI Handlers

- (void)showDismissLabel {
    promptLabel.alpha = 1.0f;
}

- (void)askQuestion {
    BOOL isSignedIn = [[UserDataProcessingEngine sharedEngine] isUserSignedIn];
    
    if (!isSignedIn) {
        [self performSegueWithIdentifier:@"showSetupViewController" sender:self];
        
        return;
    }
    
    [self performSelector:@selector(testDropbox) withObject:nil afterDelay:0.5];
    
    UINavigationController *sessionController = [self.storyboard instantiateViewControllerWithIdentifier:@"sessionController"];
    [self presentViewController:sessionController animated:NO completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Dropbox Authentication

- (void)authenticationProcess {
    NSString *requestTokenOutput = nil;
    
    NSString *APP_KEY = @"12qdorcjgnf88ic";
    NSString *APP_SECRET = @"bmq92wac3vbwrei";
    {
        NSString *requestURLString = @"https://api.dropbox.com/1/oauth/request_token";
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURLString]];
        
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSString *sendString = [NSString stringWithFormat:@"OAuth oauth_version=1.0, oauth_signature_method=PLAINTEXT, oauth_consumer_key=%@, oauth_signature=%@&", APP_KEY, APP_SECRET] ;
        [request setValue:sendString forHTTPHeaderField:@"Authorization"];
        
        NSURLResponse* response = nil;
        NSMutableData *responseData = [[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil] mutableCopy];
        
        if (responseData) requestTokenOutput = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    }
    CLSNSLog(@"%@", requestTokenOutput);
    
    NSString *oauth_token_secret = [requestTokenOutput substringFromIndex:[requestTokenOutput rangeOfString:@"="].location+1];
    oauth_token_secret = [oauth_token_secret substringToIndex:[oauth_token_secret rangeOfString:@"&"].location];
    
    NSString *oauth_token = [requestTokenOutput substringFromIndex:[requestTokenOutput rangeOfString:@"="].location+1];
    oauth_token = [oauth_token substringFromIndex:[oauth_token rangeOfString:@"="].location+1];
    
    CLSNSLog(@"oauth_token_secret: %@", oauth_token_secret);
    CLSNSLog(@"oauth_token: %@", oauth_token);
    
    {
        NSString *URLString = [NSString stringWithFormat:@"https://www.dropbox.com/1/oauth/authorize?oauth_token=%@&oauth_callback=", oauth_token];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URLString]];
    }
    
}

- (void)authorization2 {
    NSString *tempRequestToken = @"kqDgRsb9aJct7zEl";
    NSString *tempRequestSecret = @"o7yrbFEeTEoThodr";
    
    NSString *requestTokenOutput = nil;
    
    NSString *APP_KEY = @"12qdorcjgnf88ic";
    NSString *APP_SECRET = @"bmq92wac3vbwrei";
    {
        NSString *requestURLString = @"https://api.dropbox.com/1/oauth/access_token";
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURLString]];
        
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        NSString *sendString = [NSString stringWithFormat:@"OAuth oauth_version=1.0, oauth_signature_method=PLAINTEXT, oauth_consumer_key=%@, oauth_token=%@, oauth_signature=%@&%@", APP_KEY, tempRequestToken, APP_SECRET, tempRequestSecret];
        [request setValue:sendString forHTTPHeaderField:@"Authorization"];
        
        NSURLResponse* response = nil;
        NSMutableData *responseData = [[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil] mutableCopy];
        
        if (responseData) requestTokenOutput = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    }
    CLSNSLog(@"%@", requestTokenOutput);
}

- (void)authorization3 {
    //Output
    //    NSString *requestTokenOutput = nil;
    //
    //    NSString *APP_KEY = @"xwyi3spjwn90kci";
    //    NSString *APP_SECRET = @"m1zrusd9a87oeqk";
    //    NSString *ACCESS_TYPE = @"kDBRootAppFolder";
    //
    //    NSString *access_token = @"zcpx47u3n29qt3ze";
    //    NSString *access_secret = @"87ioo31ipjpbqje";
    //    NSString *userID = @"363215344";
    
}

- (void)testDropbox {
    //    [self authenticationProcess];
    //    [self authorization2];
    [self authorization3];
}

#pragma mark - UIApplication Event Handlers

- (void)appplicationIsActive:(NSNotification *)notification {
    if (self.backgroundStatus) [self askQuestion];
    self.backgroundStatus = NO;
}

- (void)applicationDidEnterBackgroundNotification:(NSNotification *)notification {
    CLSNSLog(@"Application Entered Foreground");
    self.backgroundStatus = YES;
    //    [self scheduleNotifications];
}

@end
