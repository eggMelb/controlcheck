//
//  DrinkMenuViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 25/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrinkMenuItemButton.h"

#import "DrinkPropertiesTableViewController.h"
#import "DrinkDataRetrievalEngine.h"

@interface DrinkMenuViewController : UIViewController {
    IBOutlet UIScrollView *menuScrollView;
}
@property (nonatomic, assign) BOOL isAutomated;

- (IBAction)cancel:(id)sender;

@end
