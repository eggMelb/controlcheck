//
//  JPNavigationController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 2/12/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "JPNavigationController.h"

@interface JPNavigationController : UINavigationController <UINavigationControllerDelegate, UIGestureRecognizerDelegate>
@end

@implementation JPNavigationController

//- (void)viewDidLoad
//{
//    __weak JPNavigationController *weakSelf = self;
//    
//    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
//    {
//        self.interactivePopGestureRecognizer.delegate = weakSelf;
//        self.delegate = weakSelf;
//    }
//}
//
//// Hijack the push method to disable the gesture
//
//- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
//{
//    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
//        self.interactivePopGestureRecognizer.enabled = NO;
//    
//    [super pushViewController:viewController animated:animated];
//}
//
//#pragma mark UINavigationControllerDelegate
//
//- (void)navigationController:(UINavigationController *)navigationController
//       didShowViewController:(UIViewController *)viewController
//                    animated:(BOOL)animate
//{
//    // Enable the gesture again once the new controller is shown
//    
//    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)])
//        self.interactivePopGestureRecognizer.enabled = YES;
//}


@end
