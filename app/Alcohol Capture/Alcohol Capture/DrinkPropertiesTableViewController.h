//
//  DrinkPropertiesTableViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 20/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Drink.h"

#import "DrinkTableViewController.h"
#import "UIViewController+BackButtonHandler.h"
#import "EstrendoDataValidityChecker.h"
#import "DrinkDataRetrievalEngine.h"
#import "DrinkMenuViewController.h"

@interface DrinkPropertiesTableViewController : UITableViewController <UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate, UIAlertViewDelegate> {
    NSArray *propertyArray;
    NSMutableDictionary *propertyDictionary;
}
@property (nonatomic, strong) Drink *drink;

@property (nonatomic, assign) BOOL isAddingNewDrink;
- (void)reloadProperties;

@property (nonatomic, strong) NSMutableArray *pickerViewDataArray;
@property (nonatomic, strong) UIPickerView *propertyPickerView;

@property (nonatomic, strong) NSArray *quantityDataArray;
@property (nonatomic, strong) NSArray *alcoholPercentageDataArray;
@property (nonatomic, strong) NSArray *sizeDataArray;

@property (nonatomic, strong) NSIndexPath *currentSelectedIndexPath;

@end
