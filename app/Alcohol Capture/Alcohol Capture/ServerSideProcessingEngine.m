//
//  ServerSideProcessingEngine.m
//  Alcohol Capture
//
//  Created by Jason Pan on 23/03/2015.
//  Copyright (c) 2015 Jason Pan. All rights reserved.
//

#import "ServerSideProcessingEngine.h"
#import <CloudKit/CloudKit.h>
#import <Crashlytics/Crashlytics.h>

@implementation ServerSideProcessingEngine

+ (ServerSideProcessingEngine *)sharedEngine {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static ServerSideProcessingEngine *instance = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        instance = [[self alloc] init];
        [instance initialize];
        
    });
    
    // returns the same object each time
    return instance;
}

- (void)showUpdateAlertView {
    if (!self.updateAlertView.visible) [self.updateAlertView show];
}

- (void)hideUpdateAlertView {
    if (self.updateAlertView.visible) [self.updateAlertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)sampleSubscription {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"artist = %@", @"Mei Chen"];
    
    CKSubscription *subscription = [[CKSubscription alloc]
                                    initWithRecordType:@"Artwork"
                                    predicate:predicate
                                    options:CKSubscriptionOptionsFiresOnRecordCreation];
    
    CKNotificationInfo *notificationInfo = [CKNotificationInfo new];
    notificationInfo.alertLocalizationKey = @"New artwork by your favorite artist.";
    notificationInfo.shouldBadge = YES;
    
    subscription.notificationInfo = notificationInfo;
    
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    
    [publicDatabase saveSubscription:subscription completionHandler:^(CKSubscription *subscription, NSError *error) {
        if (error) {
            // insert error handling
        }
    }];
}

-(void) test {
//    CKSubscription *subscriptionOUP = [[CKSubscription alloc] initWithRecordType:@"OUP" predicate:[NSPredicate predicateWithFormat:@"TRUEPREDICATE"] subscriptionID:@"oup" options:CKSubscriptionOptionsFiresOnRecordUpdate];
//    
//    NSString *ekf = @"oup-\(ou.recordID)";
//    NSPredicate *pr = [NSPredicate predicateWithFormat:@"ou = %@", [[CKReference alloc] initWithRecordID:[[CKRecordID alloc] initWithRecordName:@"a5b92ec7-8553-43d2-a5e3-ccff28c276bc"] action:CKReferenceActionDeleteSelf]];
//    CKSubscription *subscriptionOUP = [[CKSubscription alloc] initWithRecordType:@"OUP" predicate:pr subscriptionID:ekf options:CKSubscriptionOptionsFiresOnRecordUpdate];
//    
//    NSLog(@"ekf: %@", ekf);
//    NSLog(@"pr: %@", pr);
    
    
    
    
    
    
//    NSString *ekf = @"oup-\(recordID)";
//    NSPredicate *pr = [NSPredicate predicateWithFormat:@"(recordID = %@) AND (LatestAppVersion > 1.02)", [[CKReference alloc] initWithRecordID:[[CKRecordID alloc] initWithRecordName:@"a5b92ec7-8553-43d2-a5e3-ccff28c276bc"] action:CKReferenceActionDeleteSelf]];
    NSPredicate *pr = [NSPredicate predicateWithFormat:@"recordID = %@", [[CKReference alloc] initWithRecordID:[[CKRecordID alloc] initWithRecordName:@"a5b92ec7-8553-43d2-a5e3-ccff28c276bc"] action:CKReferenceActionDeleteSelf]];
//    CKSubscription *subscriptionOUP = [[CKSubscription alloc] initWithRecordType:@"App_Status" predicate:pr subscriptionID:ekf options:CKSubscriptionOptionsFiresOnRecordUpdate];
    CKSubscription *subscriptionOUP = [[CKSubscription alloc] initWithRecordType:@"App_Status" predicate:pr options:CKSubscriptionOptionsFiresOnRecordUpdate];
    
//    NSLog(@"ekf: %@", ekf);
    NSLog(@"pr: %@", pr);
    
    
    CKNotificationInfo *notificationInfo = [CKNotificationInfo new];
    notificationInfo.alertLocalizationKey = NSLocalizedString(@"SERVER_PROCESSING_ENGINE_UPDATE_AVAILABLE_NOTIFICATION_MESSAGE", nil);
    notificationInfo.shouldBadge = YES;
    notificationInfo.desiredKeys = @[@"LatestAppVersion", @"LatestBuildVersion"];
    subscriptionOUP.notificationInfo = notificationInfo;
    
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    [publicDatabase saveSubscription:subscriptionOUP completionHandler:^(CKSubscription *subscription, NSError *error) {
        if (error) {
            // insert error handling
            NSLog(@"error in creasing subscription: %@", error.description);
        }else {
            NSLog(@"subscription created: %@", subscription);
        }
    }];
}

- (void)createVersionSubscription {
    [self test];
    return;
    
    CKModifyBadgeOperation *badgeResetOperation = [[CKModifyBadgeOperation alloc] initWithBadgeValue:0];
    badgeResetOperation.modifyBadgeCompletionBlock = ^(NSError *error) {
        if (error != nil) {
            CLSLog(@"Error resetting badge: %@", error);
        } else {
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        }
    };
    [[CKContainer defaultContainer] addOperation:badgeResetOperation];
    
    NSLog(@"createVersionSubscription-- init");
    
    NSString *liveAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//    NSString *latestAppVersion = [NSString stringWithFormat:@"%@", [record objectForKey:@"LatestAppVersion"]];
    
//    NSString *attributeName  = @"recordID.recordName";
    NSString *attributeName  = @"recordID";
    NSString *attributeValue = @"a5b92ec7-8553-43d2-a5e3-ccff28c276bc";
//    CKReference *reference = [[CKReference alloc] initWithRecordID:<#(CKRecordID *)#> action:<#(CKReferenceAction)#>]
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ = %@", attributeName, attributeValue];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%@ == %@) && !(LatestAppVersion like %@)", attributeName, attributeValue, liveAppVersion];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(%K = %@) && !(LatestAppVersion = %f)", attributeName, [[CKRecordID alloc] initWithRecordName:attributeName], [liveAppVersion floatValue]];
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"LatestAppVersion > %f", [liveAppVersion doubleValue]];
    
    CKReference *reference = [[CKReference alloc] initWithRecordID:[[CKRecordID alloc] initWithRecordName:attributeValue] action:CKReferenceActionNone];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ = %@", attributeName, reference];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"LatestAppVersion > 1.02"];
    CKSubscription *subscription = [[CKSubscription alloc] initWithRecordType:@"App_Status" predicate:predicate options:CKSubscriptionOptionsFiresOnRecordUpdate];
    
//    NSPredicate* predicate2 = [NSPredicate predicateWithFormat:@"expiryDate BETWEEN (%@, %@)",
//                              [NSDate date],
//                              [NSDate dateWithTimeIntervalSinceNow:k30Days]];
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"artist = %@", @"Mei Chen"];
//    
//    CKSubscription *subscription = [[CKSubscription alloc]
//                                    initWithRecordType:@"Artwork"
//                                    predicate:predicate
//                                    options:CKSubscriptionOptionsFiresOnRecordCreation];
    
    CKNotificationInfo *notificationInfo = [CKNotificationInfo new];
    notificationInfo.alertLocalizationKey = NSLocalizedString(@"SERVER_PROCESSING_ENGINE_UPDATE_AVAILABLE_NOTIFICATION_MESSAGE", nil);
    notificationInfo.shouldBadge = YES;
    
//    notificationInfo.shouldSendContentAvailable = YES;//self
    
    subscription.notificationInfo = notificationInfo;
    
    CKDatabase *publicDatabase = [[CKContainer defaultContainer] publicCloudDatabase];
    
    [publicDatabase saveSubscription:subscription completionHandler:^(CKSubscription *subscription, NSError *error) {
        if (error) {
            // insert error handling
            NSLog(@"error in creasing subscription: %@", error.description);
        }else {
            NSLog(@"subscription created: %@", subscription);
        }
    }];
}

- (void)featchInfor {
    
    CLSNSLog(@"ServerSideProcessingEngine: Initializing...");
    
////    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
////    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"App_Status" predicate:predicate];
//    
//    CKContainer *container = [CKContainer defaultContainer];
//    CKDatabase *publicDB = [container publicCloudDatabase];
//    
//    CKRecordID *recordID = [[CKRecordID alloc] initWithRecordName:@"a5b92ec7-8553-43d2-a5e3-ccff28c276bc"];
//    
//    [publicDB fetchRecordWithID:recordID completionHandler:^(CKRecord *record, NSError *error) {
//        CLSNSLog(@"record: %@", record);
//        CLSNSLog(@"error: %@", error);
//    }];
//    
////    CKReference *ownerID = [record objectForKey:@"Version"];
////    [publicDB fetchRecordWithID:ownerID.recordID completionHandler:^(CKRecord *record, NSError*error) {
////        // here i expect record with recordID same as recordID in my reference attribute
////        CLSNSLog(@"record: %@", record);
////    }];
    
    
    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"App_Status" predicate:predicate];
    
    CKContainer *container = [CKContainer defaultContainer];
    //    CKContainer *container = [CKContainer containerWithIdentifier:@"iCloud.com.jasonpan.Alcohol-Capture"];
    CKDatabase *publicDB = [container publicCloudDatabase];
    
    [publicDB performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error) {
        //        CLSNSLog(@"test");
        if (error) {
            CLSNSLog(@"Error: %@", error.description);
        }
        
        CLSNSLog(@"results: %@", results);
        
        if (results.count != 1) {
            if (results.count == 0) {
                CLSNSLog(@"No results; continue as normal");
            }else if (results.count > 1) {
                CLSNSLog(@"critical error; there shouldn't be more than 1 App_Status record");
            }
            CLSNSLog(@"ServerSideProcessingEngine: Abandoning latest app version check.");
        }else {
            for (CKRecord *record in results) {
                CLSNSLog(@"record: %@", record);
                
                if ([record.allKeys containsObject:@"LatestAppVersion"]) {
                    NSString *liveAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
                    NSString *latestAppVersion = [NSString stringWithFormat:@"%@", [record objectForKey:@"LatestAppVersion"]];
                    
                    CLSNSLog(@"Latest Version is %@", [record objectForKey:@"LatestAppVersion"]);
                    
                    if ([latestAppVersion isEqualToString:liveAppVersion]) {
                        CLSNSLog(@"App versions equal; continue as normal");
                        CLSNSLog(@"ServerSideProcessingEngine: Latest app version check passed.");
                        
                        if (self.updateAlertView.visible) [self.updateAlertView dismissWithClickedButtonIndex:0 animated:YES];
                    }else {
                        CLSNSLog(@"App versions aren't equal; direct user to update");
                        CLSNSLog(@"ServerSideProcessingEngine: Latest app version check failed.");
                        
                        dispatch_sync(dispatch_get_main_queue(), ^ {
                            //                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Update" message:@"Please update to the latest version to continue participating in this research study. This update is not optional." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            if (!self.updateAlertView.visible) [self.updateAlertView show];
                        });
                        
                    }
                }else {
                    CLSNSLog(@"No latest app version status?");
                    CLSNSLog(@"ServerSideProcessingEngine: Abandoning latest app version check.");
                }
            }
        }
    }];
}

- (void)initialize {
    self.updateAlertView = [[UIAlertView alloc] initWithTitle:@"Update" message:NSLocalizedString(@"SERVER_PROCESSING_ENGINE_FORCE_UPDATE_MESSAGE", nil) delegate:nil cancelButtonTitle:nil otherButtonTitles:nil, nil];
    self.updateAlertView.tag = 9246;
    
    [self featchInfor];
    [self createVersionSubscription];
    
    return;
    
    CLSNSLog(@"ServerSideProcessingEngine: Initializing...");
//    // register to observe notifications from the store
//    [[NSNotificationCenter defaultCenter] addObserver: self
//                                             selector: @selector (storeDidChange:)
//                                                 name: NSUbiquitousKeyValueStoreDidChangeExternallyNotification
//                                               object: [NSUbiquitousKeyValueStore defaultStore]];
//    
//    // get changes that might have happened while this
//    // instance of your app wasn't running
//    [[NSUbiquitousKeyValueStore defaultStore] synchronize];
//    
//    [[NSUbiquitousKeyValueStore defaultStore] setString:@"testValue" forKey:@"testKey"];
//    [[NSUbiquitousKeyValueStore defaultStore] synchronize];
//    
//    CLSNSLog(@"test: %@", [[NSUbiquitousKeyValueStore defaultStore] stringForKey:@"testKey"]);
//    
////    CLSNSLog(@"curretiCloudoken: %@", currentiCloudToken);
    
//    NSPredicate *predicate = [NSPredicate predicateWithValue:TRUE];//[NSPredicate predicateWithFormat:@"%@ == %@"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"TRUEPREDICATE"];
    CKQuery *query = [[CKQuery alloc] initWithRecordType:@"App_Status" predicate:predicate];
    
    CKContainer *container = [CKContainer defaultContainer];
//    CKContainer *container = [CKContainer containerWithIdentifier:@"iCloud.com.jasonpan.Alcohol-Capture"];
    CKDatabase *publicDB = [container publicCloudDatabase];
    
    [publicDB performQuery:query inZoneWithID:nil completionHandler:^(NSArray *results, NSError *error) {
//        CLSNSLog(@"test");
        if (error) {
            CLSNSLog(@"Error: %@", error.description);
        }
        
        CLSNSLog(@"results: %@", results);
        
        if (results.count != 1) {
            if (results.count == 0) {
                CLSNSLog(@"No results; continue as normal");
            }else if (results.count > 1) {
                CLSNSLog(@"critical error; there shouldn't be more than 1 App_Status record");
            }
            CLSNSLog(@"ServerSideProcessingEngine: Abandoning latest app version check.");
        }else {
            for (CKRecord *record in results) {
                CLSNSLog(@"record: %@", record);
                
                if ([record.allKeys containsObject:@"LatestAppVersion"]) {
                    NSString *liveAppVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
                    NSString *latestAppVersion = [NSString stringWithFormat:@"%@", [record objectForKey:@"LatestAppVersion"]];
                    
                    CLSNSLog(@"Latest Version is %@", [record objectForKey:@"LatestAppVersion"]);
                    
                    if ([latestAppVersion isEqualToString:liveAppVersion]) {
                        CLSNSLog(@"App versions equal; continue as normal");
                        CLSNSLog(@"ServerSideProcessingEngine: Latest app version check passed.");
                        
                        if (self.updateAlertView.visible) [self.updateAlertView dismissWithClickedButtonIndex:0 animated:YES];
                    }else {
                        CLSNSLog(@"App versions aren't equal; direct user to update");
                        CLSNSLog(@"ServerSideProcessingEngine: Latest app version check failed.");
                        
                        dispatch_sync(dispatch_get_main_queue(), ^ {
//                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Update" message:@"Please update to the latest version to continue participating in this research study. This update is not optional." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            if (!self.updateAlertView.visible) [self.updateAlertView show];
                        });
                        
                    }
                }else {
                    CLSNSLog(@"No latest app version status?");
                    CLSNSLog(@"ServerSideProcessingEngine: Abandoning latest app version check.");
                }
                
                //            CKReference *ownerID = [record objectForKey:@"Version"];
                //            [publicDB fetchRecordWithID:ownerID.recordID completionHandler:^(CKRecord *record, NSError*error) {
                //                // here i expect record with recordID same as recordID in my reference attribute
                //                CLSNSLog(@"record: %@", record);
                //            }];
            }
        }
    }];
    
    
    
//    CKQueryOperation *operation = [[CKQueryOperation alloc] initWithQuery:query];
//    [operation start]
//
//    CLSNSLog(@"operation: %@", operation);
}

- (void)storeDidChange:(id)sender {
    
}

- (void)currentID {
    
}

#pragma mark - Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 9246) {
        if (buttonIndex == 0) {
            CLSNSLog(@"ServerSideProcessingEngine: Now directing user to update page...");
            //TODO: no update page atm...
        }
    }
}

@end
