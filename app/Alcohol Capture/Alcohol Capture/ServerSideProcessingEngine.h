//
//  ServerSideProcessingEngine.h
//  Alcohol Capture
//
//  Created by Jason Pan on 23/03/2015.
//  Copyright (c) 2015 Jason Pan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerSideProcessingEngine : NSObject <UIAlertViewDelegate>

+ (ServerSideProcessingEngine *)sharedEngine;
- (void)featchInfor;

- (void)showUpdateAlertView;
- (void)hideUpdateAlertView;

@property (nonatomic, strong) UIAlertView *updateAlertView;

@end
