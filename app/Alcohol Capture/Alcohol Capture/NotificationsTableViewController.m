//
//  NotificationsTableViewController.m
//  Alcohol Capture
//
//  Created by Jason Pan on 4/12/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "NotificationsTableViewController.h"
#include <objc/runtime.h>

@interface NotificationsTableViewController ()

@end

@implementation NotificationsTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialize];
    [self initializeDatePicker];
    
    [self processPresetNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initializers

- (void)initialize {
    self.hasMadeChanges = NO;
    self.navigationItem.rightBarButtonItem.title = @"Done";
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView:)];
    [self.tableView addGestureRecognizer:tap];
}

- (void)initializeDatePicker {
    self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 162)];
    [self.datePicker addTarget:self action:@selector(datePickerValueDidChange:) forControlEvents:UIControlEventValueChanged];
    self.datePicker.datePickerMode = UIDatePickerModeTime;
    
    [self.tableView addSubview:self.datePicker];
}

#pragma mark - UI Event Handlers

- (IBAction)save:(id)sender {
    if (self.hasMadeChanges) {
        NSMutableDictionary *preferencesDictionary = [[NSMutableDictionary alloc] initWithDictionary:[[UserDataProcessingEngine sharedEngine] userPreferencesDictionary]];
        
        NSMutableDictionary *notificationsPreferencesDictionary = [NSMutableDictionary dictionary];
        [notificationsPreferencesDictionary setObject:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].detailTextLabel.text forKey:@"Notification 1"];
        
        for (int index = 0; index < [UserDataProcessingEngine sharedEngine].notificationsPreferences.count-kNumberOfMorningReminders; index++) {
            NSString *notificationKey = [NSString stringWithFormat:@"Notification %i", kNumberOfMorningReminders+index+1];
            [notificationsPreferencesDictionary setObject:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:1]].detailTextLabel.text forKey:notificationKey];
        }
        
        [preferencesDictionary setObject:notificationsPreferencesDictionary forKey:@"Notifications"];
        
        [[UserDataProcessingEngine sharedEngine] saveUserPreferencesWithPreferenceDictionary:preferencesDictionary];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didTapOnTableView:(UIGestureRecognizer*) recognizer {
    CGPoint tapLocation = [recognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapLocation];
    
    if (indexPath) {
        recognizer.cancelsTouchesInView = NO;
    } else {
        self.currentSelectedIndexPath = nil;
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
        [self hideDatePicker];
    }
}

- (void)datePickerValueDidChange:(UIDatePicker *)sender {
    if ([self.tableView indexPathForSelectedRow].section == 0) return;
    
    self.hasMadeChanges = YES;
    self.navigationItem.rightBarButtonItem.title = @"Save";
    
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    selectedCell.detailTextLabel.text = [[dateFormatter stringFromDate:sender.date] lowercaseString];
}

#pragma mark - Processors

- (void)processPresetNotifications {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    CLSNSLog(@"notificationPrefernces: %@", [[UserDataProcessingEngine sharedEngine] notificationsPreferences]);
    
    int numberOfNotifications = (int)[[UserDataProcessingEngine sharedEngine] notificationsPreferences].count;
    for (int index = 0; index <= numberOfNotifications; index++) {
        NSString *getterPropertyMethodName = [NSString stringWithFormat:@"reminderDate%i", index+1];
        const char *getterPropertyMethodNameCString = [getterPropertyMethodName cStringUsingEncoding:NSASCIIStringEncoding];
        
        NSString *notificationKey = [NSString stringWithFormat:@"Notification %i", index+1];
        
        NSString *notification = [[[UserDataProcessingEngine sharedEngine] notificationsPreferences] objectForKey:notificationKey];
        [self setDateForDate:[dateFormatter dateFromString:notification] withSelectorIdentifier:sel_getUid(getterPropertyMethodNameCString)];
    }
}

#pragma mark - Associated Objects/Objective-C Runtime methods (Special stuff)

- (NSDate *)dateForSelectorIdentifier:(SEL)selectorIdentifier {
    CLSNSLog(@"Getter: %@, %@, %@", self, NSStringFromSelector(_cmd), NSStringFromSelector(selectorIdentifier));
    return objc_getAssociatedObject(self, selectorIdentifier);
}

- (void)setDateForDate:(NSDate *)newDate withSelectorIdentifier:(SEL)selectorIdentifier {
    CLSNSLog(@"Setter: %@, %@, %@, %@", self, NSStringFromSelector(_cmd), newDate, NSStringFromSelector(selectorIdentifier));
    id oldName = objc_getAssociatedObject(self, selectorIdentifier);
    if (oldName != newDate) objc_setAssociatedObject(self, selectorIdentifier, newDate, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - UIDatePicker Show/Hide Methods

- (void)showDatePicker {
    [UIView animateWithDuration:0.3 animations:^ {
        self.datePicker.frame = (CGRect){0, self.view.frame.size.height-self.datePicker.frame.size.height-44-20, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
    }];
}

- (void)hideDatePicker {
    [UIView animateWithDuration:0.3 animations:^ {
        self.datePicker.frame = (CGRect){0, self.view.frame.size.height+44+20, self.datePicker.frame.size.width, self.datePicker.frame.size.height};
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    CLSNSLog(@"number: %i", (int)[[UserDataProcessingEngine sharedEngine] notificationsPreferences].count);
    CLSNSLog(@"notificationsPreferences: %@", [[UserDataProcessingEngine sharedEngine] notificationsPreferences]);
    if (section == 0) return 1;
    if (section == 1) return [[UserDataProcessingEngine sharedEngine] notificationsPreferences].count-1;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"notification"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    
    if (indexPath.section == 0) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (indexPath.row == 0) {
            cell.textLabel.text = @"Morning";
        }
        
        NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc] init];
        [tempDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        
        NSString *getterPropertyMethodName = [NSString stringWithFormat:@"reminderDate%i", (int)(indexPath.row+1)];
        const char *getterPropertyMethodNameCString = [getterPropertyMethodName cStringUsingEncoding:NSASCIIStringEncoding];
        cell.detailTextLabel.text = [dateFormatter stringFromDate:[self dateForSelectorIdentifier:sel_getUid(getterPropertyMethodNameCString)]];
        
    }else if (indexPath.section == 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        NSString *notificationKey = [NSString stringWithFormat:@"Notification %i", (int)(1+indexPath.row+1)];
        cell.textLabel.text = notificationKey;
        
        NSDateFormatter *tempDateFormatter = [[NSDateFormatter alloc] init];
        [tempDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        
        NSString *getterPropertyMethodName = [NSString stringWithFormat:@"reminderDate%i", (int)(1+indexPath.row+1)];
        const char *getterPropertyMethodNameCString = [getterPropertyMethodName cStringUsingEncoding:NSASCIIStringEncoding];
        cell.detailTextLabel.text = [dateFormatter stringFromDate:[self dateForSelectorIdentifier:sel_getUid(getterPropertyMethodNameCString)]];
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        self.currentSelectedIndexPath = nil;
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self hideDatePicker];
    }else if (indexPath.section == 1) {
        if (indexPath == [self.tableView indexPathForSelectedRow] && indexPath == self.currentSelectedIndexPath) {
            self.currentSelectedIndexPath = nil;
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            [self hideDatePicker];
            
            return;
        }
        self.currentSelectedIndexPath = indexPath;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        CLSNSLog(@"currentDate: %@", [dateFormatter stringFromDate:[NSDate date]]);
        
        NSString *getterPropertyMethodName = [NSString stringWithFormat:@"reminderDate%i", (int)(kNumberOfMorningReminders+indexPath.row+1)];
        const char *getterPropertyMethodNameCString = [getterPropertyMethodName cStringUsingEncoding:NSASCIIStringEncoding];
        self.datePicker.date = [self dateForSelectorIdentifier:sel_getUid(getterPropertyMethodNameCString)];
        
        [self showDatePicker];
    }
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    CLSNSLog(@"alertView with tag: %li tapped buttonIndex: %li", (long)alertView.tag, (long)buttonIndex);
    
    if (alertView.tag == 8612 && buttonIndex == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
