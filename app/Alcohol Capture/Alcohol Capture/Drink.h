//
//  Drink.h
//  Alcohol Capture
//
//  Created by Jason Pan on 20/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Drink : NSObject

@property (nonatomic, strong) NSString *type; //Drink Type (Description)
@property (nonatomic, assign) NSInteger quantity; //Number of drinks
@property (nonatomic, strong) NSString *alcoholPercentage; //Alcohol Percentage (x%) - e.g. 50
@property (nonatomic, assign) NSInteger size; //Drink size in millilitres (mL)

@end