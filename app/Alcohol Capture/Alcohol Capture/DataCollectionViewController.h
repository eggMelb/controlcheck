//
//  DataCollectionViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 25/10/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Dropbox/Dropbox.h>

#import "HomeViewController.h"

#import "UserDataProcessingEngine.h"

@interface DataCollectionViewController : UIViewController {
    IBOutlet UILabel *promptLabel;
}

- (void)submitNoDrinkingSessionData;

@end
