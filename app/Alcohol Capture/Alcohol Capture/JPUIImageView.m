//
//  JPUIImageView.m
//  Alcohol Capture
//
//  Created by Jason Pan on 4/12/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "JPUIImageView.h"
#import <Crashlytics/Crashlytics.h>

@implementation JPUIImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

-(void)awakeFromNib {
    [self initialize];
}

-(void)initialize {
////    //158
////    //70
////    if (self.frame.size.width == 158) self.layer.cornerRadius = 30.0f;
////    else if (self.frame.size.width == 70) self.layer.cornerRadius = 12.0f;
//////    self.layer.cornerRadius = self.frame.size.width/3;
////    
////    self.layer.masksToBounds = YES;
////    
//////    self.layer.borderWidth = 1.5f;
////    if (self.frame.size.width == 158) self.layer.borderWidth = 1.5f;
////    else if (self.frame.size.width == 70) self.layer.borderWidth = 0.8f;
////    self.layer.borderColor = [[UIColor blackColor] CGColor];
//    
//    CLSNSLog(@"appIcon: %f", self.bounds.size.width);
//    
//    self.layer.masksToBounds = YES;
//    self.layer.borderColor = [[UIColor blackColor] CGColor];
//    
//    if (self.bounds.size.width == 70) {
//        self.layer.cornerRadius = 12.0f;
//        self.layer.borderWidth = 0.8f;
//        
//        self.image = [UIImage imageNamed:@"internal70.png"];
//    }else if (self.bounds.size.width == 158) {
//        self.layer.cornerRadius = 30.0f;
//        self.layer.borderWidth = 1.5f;
//        
//        self.image = [UIImage imageNamed:@"internal250.png"];
//    }
    
    
    CLSNSLog(@"appIcon: %f", self.bounds.size.width);
    
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [[UIColor blackColor] CGColor];
    
    if ([UIScreen mainScreen].bounds.size.height == 480) {
        self.layer.cornerRadius = 12.0f;
        self.layer.borderWidth = 0.8f;
        
        self.image = [UIImage imageNamed:@"internal70.png"];
    }else if ([UIScreen mainScreen].bounds.size.height == 568) {
        self.layer.cornerRadius = 30.0f;
        self.layer.borderWidth = 1.5f;
        
        self.image = [UIImage imageNamed:@"internal250.png"];
    }else {
        self.layer.cornerRadius = 60.0f;
        self.layer.borderWidth = 2.0f;
        
        self.image = [UIImage imageNamed:@"internal500.png"];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self initialize];
}
*/

@end
