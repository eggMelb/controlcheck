//
//  UIViewController+FixNavigationBarCorruption.h
//  Alcohol Capture
//
//  Created by Jason Pan on 4/12/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (FixNavigationBarCorruption)

- (void)fixNavigationBarCorruption;

@end
