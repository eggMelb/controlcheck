//
//  HistoryTableViewController.h
//  Alcohol Capture
//
//  Created by Jason Pan on 8/12/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserDataProcessingEngine.h"

@interface HistoryTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *pastSessionCellsArray;

@end
