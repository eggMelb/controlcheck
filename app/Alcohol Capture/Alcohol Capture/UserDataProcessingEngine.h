//
//  UserDataProcessingEngine.h
//  Alcohol Capture
//
//  Created by Jason Pan on 29/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CHCSVParser.h"

#import "DrinkDataRetrievalEngine.h"
#import "Drink.h"

#import <Dropbox/Dropbox.h>

#import <Crashlytics/Crashlytics.h>

#define isDebuggingEndTrialPeriod TRUE

#define forceResearchPeriodExpire FALSE

#define currentAppStatus @"Beta"
//#define currentAppStatus @"Trial"
//#define currentAppStatus @"Production"

@interface UserDataProcessingEngine : NSObject {
//    BOOL isUserFirstStartup;
}

@property (nonatomic, strong) NSArray *templateHeadersArray;
@property (nonatomic, strong) NSArray *baseArray;
@property (nonatomic, strong) NSDictionary *currentSessionData;
@property (nonatomic, strong) NSDictionary *userDataDictionary;
@property (nonatomic, strong) NSDictionary *userPreferencesDictionary;
@property (nonatomic, strong) NSArray *pastUploadDates;
@property (nonatomic, strong) NSArray *pastUploadTimes;
@property (nonatomic, strong) NSArray *pastUploadTimestamps;
@property (nonatomic, strong) NSDictionary *appSettingsDictionary;

@property (nonatomic, assign, readonly) BOOL isUserSignedIn;
//@property (nonatomic, assign, readonly) BOOL isUserFirstStartup;
@property (nonatomic, assign) BOOL isUserFirstStartup;

- (BOOL)isStudyPeriodExpired;
- (NSDate *)userFirstSignInDate;

+(UserDataProcessingEngine *)sharedEngine;

-(void)loadBaseDictionary;
-(void)saveDataWithNumberOfDrinks:(NSDictionary *)numberOfDrinksDictionary andUserData:(NSDictionary *)userDictionary andSessionData:(NSDictionary *)sessionDictionary;

-(NSDictionary *)userData;

-(void)uploadData;

-(void)saveSessionData:(NSDictionary *)sessionDataDictionary;
-(NSDictionary *)sessionData;

-(NSString *)fileNameForDataName:(NSString *)dataName;
-(NSString *)filePathForDataName:(NSString *)dataName;

//-(BOOL)isUserSignedIn;
//-(BOOL)isUserFirstStartup;
-(void)loadUserData;
-(void)saveUserDataWithUserInfoDictionary:(NSDictionary *)userInfoDictionary;
-(void)updateUserDataWithUserInfoDictionary:(NSDictionary *)userInfoDictionary;

-(void)loadUserPreferences;
-(NSDictionary *)notificationsPreferences;
-(void)saveUserPreferencesWithPreferenceDictionary:(NSDictionary *)preferenceDictionary;

-(void)loadPastUploadDates;

-(void)loadAppSettings;


- (NSDictionary *)retrieveSummaryInformation;

- (void)expireData;
- (BOOL)isSessionDataUnavailable;

@end
