//
//  DrinkDataRetrievalEngine.m
//  Alcohol Capture
//
//  Created by Jason Pan on 29/11/2014.
//  Copyright (c) 2014 Jason Pan. All rights reserved.
//

#import "DrinkDataRetrievalEngine.h"
#import <Crashlytics/Crashlytics.h>

@implementation DrinkDataRetrievalEngine

+(DrinkDataRetrievalEngine *)sharedEngine {
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static DrinkDataRetrievalEngine *instance = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        instance = [[self alloc] init];
        [instance initialize];
    });
    
    // returns the same object each time
    return instance;
}

-(void)reloadData {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DrinksPropertyList" ofType:@"plist"];
    
    self.drinkDataArray = [NSArray arrayWithContentsOfFile:path];
    
    CLSNSLog(@"Loaded drinkDataArray: %@", self.drinkDataArray);
    
    NSMutableDictionary *drinkDataDictionary = [[NSMutableDictionary alloc] initWithCapacity:self.drinkDataArray.count];
    for (int index = 0; index < self.drinkDataArray.count; index++) {
        NSDictionary *currentDrinkDictionary = [self.drinkDataArray objectAtIndex:index];
        NSMutableDictionary *drinkDictionary = [[NSMutableDictionary alloc] initWithCapacity:100];
        
        for (int subIndex = 0; subIndex < currentDrinkDictionary.allKeys.count; subIndex++) {
            NSString *key = [currentDrinkDictionary.allKeys objectAtIndex:subIndex];
            
            if ([key isEqualToString:@"DrinkType"]) {
                [drinkDataDictionary setObject:drinkDictionary forKey:[currentDrinkDictionary objectForKey:key]];
            }else {
                [drinkDictionary setObject:[currentDrinkDictionary objectForKey:key] forKey:key];
            }
        }
    }
    
    self.drinkDataDictionary = drinkDataDictionary;
    CLSNSLog(@"Loaded drinkDataDictionary: %@", self.drinkDataDictionary);
}

-(void)initialize {
    [self reloadData];
}

-(NSArray *)drinkTypes {
    NSMutableArray *drinkTypes = [[NSMutableArray alloc] initWithCapacity:self.drinkDataArray.count];
    
    for (int index = 0; index < self.drinkDataArray.count; index++) {
        NSString *currentDrinkType = [NSString stringWithFormat:@"%@", [[self.drinkDataArray objectAtIndex:index] objectForKey:@"DrinkType"]];
        [drinkTypes addObject:currentDrinkType];
    }
    
    return drinkTypes;
}

-(NSDictionary *)drinksDictionary {
    return self.drinkDataDictionary;
}

-(NSDictionary *)infoForDrinkType:(NSString *)drinkType {
    return [self.drinkDataDictionary objectForKey:drinkType];
}

-(NSDictionary *)sizesForDrinkType:(NSString *)drinkType {
    return [[self.drinkDataDictionary objectForKey:drinkType] objectForKey:@"Sizes"];
}

-(NSArray *)descriptionSizesForDrinkType:(NSString *)drinkType {
    NSDictionary *sizesDictionary = [[DrinkDataRetrievalEngine sharedEngine] sizesForDrinkType:drinkType];
    
    NSMutableArray *descriptionArray = [[NSMutableArray alloc] initWithCapacity:100];
    for (int index = 0; index < sizesDictionary.allKeys.count; index++) {
        CLSNSLog(@"valueClass: %@", [[sizesDictionary.allValues objectAtIndex:index] class]);
        if ([NSStringFromClass([[sizesDictionary.allValues objectAtIndex:index] class]) isEqualToString:@"__NSCFArray"]) {
            NSArray *sizeCategoryArray = [sizesDictionary.allValues objectAtIndex:index];
            
            for (int subIndex = 0; subIndex < sizeCategoryArray.count; subIndex++) {
                NSString *description = [NSString stringWithFormat:@"%@ (%@ mL)", [sizesDictionary.allKeys objectAtIndex:index], [sizeCategoryArray objectAtIndex:subIndex]];
                [descriptionArray addObject:description];
            }
        }else {
            NSString *description = [NSString stringWithFormat:@"%@ (%@ mL)", [sizesDictionary.allKeys objectAtIndex:index], [sizesDictionary.allValues objectAtIndex:index]];
            [descriptionArray addObject:description];
        }
    }
    
    return descriptionArray;
}

-(NSInteger)millilitresForDrinkType:(NSString *)drinkType andSize:(NSString *)size {
    return [[[[self.drinkDataDictionary objectForKey:drinkType] objectForKey:@"Sizes"] objectForKey:size] intValue];
}

-(NSInteger)millilitresForDrinkType:(NSString *)drinkType andDescriptionSizes:(NSString *)descriptionSize {
    NSString *volume = [descriptionSize substringFromIndex:[descriptionSize rangeOfString:@"("].location+1];
    volume = [volume substringToIndex:[volume rangeOfString:@" mL)"].location];
    CLSNSLog(@"VOLUME: %@", volume);
    
    return [volume integerValue];
}

-(NSString *)descriptionSizeForDrinkType:(NSString *)drinkType andSize:(NSString *)size {
    NSDictionary *sizesDictionary = [[DrinkDataRetrievalEngine sharedEngine] sizesForDrinkType:drinkType];
    
    __block NSString *sizeName = nil;
    
    [sizesDictionary keysOfEntriesPassingTest:^BOOL(id key, id obj, BOOL *stop) {
        CLSNSLog(@"%@ %@ %@", key, obj, size);
        
        BOOL found = ([size isEqualToString:[NSString stringWithFormat:@"%@", obj]]);
        if (found) {
            *stop = YES;
            sizeName = key;
        }
        return found;
        
    }];
    
    if (sizeName == nil) {
        for (int index = 0; index < sizesDictionary.allKeys.count; index++) {
            if ([NSStringFromClass([[sizesDictionary objectForKey:[sizesDictionary.allKeys objectAtIndex:index]] class]) isEqualToString:@"__NSCFArray"]) {
                sizeName = [sizesDictionary.allKeys objectAtIndex:index];
            }
        }
    }
    
    CLSNSLog(@"keysForObject: %@", sizeName);
    
    NSString *description = [NSString stringWithFormat:@"%@ (%@ mL)", sizeName, size];
    
    if ([description rangeOfString:@"(null)"].location != NSNotFound) {
        description = @"(unfilled)";
    }
    
    return description;
}

-(NSArray *)descriptionSizesForDrinkType:(NSString *)drinkType withOrder:(BOOL)shouldSort {
    if (!shouldSort) {
        return [self descriptionSizesForDrinkType:drinkType];
    }else {
        NSDictionary *sizesDictionary = [[DrinkDataRetrievalEngine sharedEngine] sizesForDrinkType:drinkType];
        NSMutableArray *platSizesArray = [[NSMutableArray alloc] initWithCapacity:100];
        
        for (int index = 0; index < sizesDictionary.allKeys.count; index++) {
            if ([NSStringFromClass([[sizesDictionary objectForKey:[sizesDictionary.allKeys objectAtIndex:index]] class]) isEqualToString:@"__NSCFArray"]) {
                NSArray *subArray = [sizesDictionary.allValues objectAtIndex:index];
                
                for (int subIndex = 0; subIndex < subArray.count; subIndex++) {
                    [platSizesArray addObject:[NSString stringWithFormat:@"%@", [subArray objectAtIndex:subIndex]]];
                }
            }else {
                [platSizesArray addObject:[NSString stringWithFormat:@"%@", [sizesDictionary.allValues objectAtIndex:index]]];
            }
        }
        
        NSArray *sortedArray = [platSizesArray sortedArrayUsingComparator:^NSComparisonResult(NSString *a, NSString *b) {
            NSNumber *first = [NSNumber numberWithInteger:[a integerValue]];
            NSNumber *second = [NSNumber numberWithInteger:[b integerValue]];
            
            return [first compare:second];
        }];
        
        NSMutableArray *sortedDescriptionSizesArray = [[NSMutableArray alloc] initWithCapacity:100];
        
        for (int index = 0; index < sortedArray.count; index++) {
            NSString *currentSize = [NSString stringWithFormat:@"%@", [sortedArray objectAtIndex:index]];
            NSString *descriptionSize = [self descriptionSizeForDrinkType:drinkType andSize:currentSize];
            
            [sortedDescriptionSizesArray addObject:descriptionSize];
        }
        
        return sortedDescriptionSizesArray;
    }
}

-(NSArray *)drinkDescriptionsForDrinkType:(NSString *)drinkType {
    NSDictionary *sizesDictionary = [[DrinkDataRetrievalEngine sharedEngine] sizesForDrinkType:drinkType];
    
    NSMutableArray *platSizesArray = [[NSMutableArray alloc] initWithCapacity:100];
    for (int index = 0; index < sizesDictionary.allKeys.count; index++) {
        
        if ([NSStringFromClass([[sizesDictionary objectForKey:[sizesDictionary.allKeys objectAtIndex:index]] class]) isEqualToString:@"__NSCFArray"]) {
            NSArray *subArray = [sizesDictionary.allValues objectAtIndex:index];
            
            for (int subIndex = 0; subIndex < subArray.count; subIndex++) {
                [platSizesArray addObject:[NSString stringWithFormat:@"%@", [subArray objectAtIndex:subIndex]]];
            }
        }else {
            [platSizesArray addObject:[NSString stringWithFormat:@"%@", [sizesDictionary.allValues objectAtIndex:index]]];
        }
    }

    NSArray *sortedArray = [platSizesArray sortedArrayUsingComparator:^NSComparisonResult(NSString *a, NSString *b) {
        NSNumber *first = [NSNumber numberWithInteger:[a integerValue]];
        NSNumber *second = [NSNumber numberWithInteger:[b integerValue]];
        
        return [first compare:second];
    }];
    
    
    NSMutableArray *descriptionArray = [[NSMutableArray alloc] initWithCapacity:100];
    for (int index = 0; index < sortedArray.count; index++) {
        NSString *description = [NSString stringWithFormat:@"%@ (%@ mL)", drinkType, [sortedArray objectAtIndex:index]];
        [descriptionArray addObject:description];
    }
    
    return descriptionArray;
}

-(NSString *)drinkDescriptionForDrinkType:(NSString *)drinkType andSize:(NSString *)size {
    return [NSString stringWithFormat:@"%@ (%@ mL)", drinkType, size];
}

@end
