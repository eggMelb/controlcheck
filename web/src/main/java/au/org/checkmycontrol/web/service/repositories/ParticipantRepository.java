package au.org.checkmycontrol.web.service.repositories;

import au.org.checkmycontrol.web.domain.Participant;
import org.springframework.data.repository.CrudRepository;

public interface ParticipantRepository extends CrudRepository<Participant, Long> {
}
