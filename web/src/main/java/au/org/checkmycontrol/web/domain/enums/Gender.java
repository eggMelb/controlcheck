package au.org.checkmycontrol.web.domain.enums;

public enum Gender {
    MALE, FEMALE
}
