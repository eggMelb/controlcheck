package au.org.checkmycontrol.web.domain;

import au.org.checkmycontrol.web.domain.constraints.ValidDrinkVolume;
import au.org.checkmycontrol.web.domain.enums.DrinkType;
import au.org.checkmycontrol.web.domain.enums.DrinkVolume;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Embeddable
@ValidDrinkVolume
@AllArgsConstructor
@NoArgsConstructor
public class Drink {
    @NotNull
    @Min(1)
    private Integer count;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DrinkType type;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DrinkVolume volume;
}
