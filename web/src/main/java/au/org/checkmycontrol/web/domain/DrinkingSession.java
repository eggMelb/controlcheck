package au.org.checkmycontrol.web.domain;

import au.org.checkmycontrol.web.domain.constraints.Before;
import au.org.checkmycontrol.web.domain.constraints.Before.When;
import au.org.checkmycontrol.web.domain.constraints.ValidDrinkingSessionDuration;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@ToString(exclude = {"identifier"})
@EqualsAndHashCode(exclude = {"identifier"})
@Entity
@Table(name = "session")
@ValidDrinkingSessionDuration
public class DrinkingSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false)
    private Identifier identifier;

    @NotEmpty
    @Column(nullable = false)
    private String appVersion;

    @NotNull
    @Before(value = When.NOW, inclusive = true)
    @Column(nullable = false)
    private LocalDateTime startedAt;

    @NotNull
    @Before(value = When.NOW, inclusive = true)
    @Column(nullable = false)
    private LocalDateTime endedAt;

    @NotEmpty
    @Valid
    @ElementCollection
    @CollectionTable(name = "session_drink", joinColumns = @JoinColumn(name = "session_id"))
    @AttributeOverrides({
            @AttributeOverride(name = "count", column = @Column(name = "count", nullable = false)),
            @AttributeOverride(name = "type", column = @Column(name = "type", nullable = false)),
            @AttributeOverride(name = "volume", column = @Column(name = "volume", nullable = false))
    })
    private List<Drink> drinks = new ArrayList<>();

    @Column(nullable = false)
    private LocalDateTime createdAt;

    public DrinkingSession(final Identifier identifier) {
        this.identifier = identifier;
    }
}
