package au.org.checkmycontrol.web.domain.constraints;

import au.org.checkmycontrol.web.domain.constraints.before.LocalDateBeforeValidator;
import au.org.checkmycontrol.web.domain.constraints.before.LocalDateTimeValidator;
import au.org.checkmycontrol.web.domain.constraints.before.YearBeforeValidator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = {LocalDateBeforeValidator.class, LocalDateTimeValidator.class, YearBeforeValidator.class})
@Documented
public @interface Before {
    String message() default "{au.org.checkmycontrol.web.domain.constraints.Before.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    When value();

    boolean inclusive() default false;

    @RequiredArgsConstructor
    enum When {
        NOW(LocalDateTime.now()) {
            @Override
            public String toString() {
                return getAnchor().toString();
            }
        },
        TODAY(LocalDate.now().atStartOfDay()) {
            @Override
            public String toString() {
                return LocalDate.from(getAnchor()).toString();
            }
        },
        THIS_YEAR(Year.now().atDay(1).atStartOfDay()) {
            @Override
            public String toString() {
                return Year.from(getAnchor()).toString();
            }
        };

        @Getter
        private final LocalDateTime anchor;

        @Override
        public abstract String toString();
    }
}
