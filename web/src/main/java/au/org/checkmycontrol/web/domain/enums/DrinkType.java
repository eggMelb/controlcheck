package au.org.checkmycontrol.web.domain.enums;

import lombok.Getter;

import java.util.EnumSet;

import static au.org.checkmycontrol.web.domain.enums.DrinkVolume.*;

public enum DrinkType {
    BEER_FULL(ML_200, ML_285, ML_375, ML_425, ML_570),
    BEER_LIGHT(ML_200, ML_285, ML_375, ML_425, ML_570),
    BEER_MID(ML_200, ML_285, ML_375, ML_425, ML_570),
    CIDER_PREMIX_SPIRT(ML_300, ML_330, ML_375, ML_500),
    COCKTAIL(ML_30, ML_60, ML_90, ML_120),
    SPIRIT_LIQUEUR(ML_30, ML_60),
    WINE_FORTIFIED(ML_60),
    WINE_RED(ML_150),
    WINE_WHITE_CHAMPAGNE(ML_150);

    @Getter
    private final EnumSet<DrinkVolume> allowedVolumes;

    DrinkType(DrinkVolume first, DrinkVolume... rest) {
        this.allowedVolumes = EnumSet.of(first, rest);
    }
}
