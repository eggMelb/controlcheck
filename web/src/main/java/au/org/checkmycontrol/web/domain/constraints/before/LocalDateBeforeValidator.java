package au.org.checkmycontrol.web.domain.constraints.before;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class LocalDateBeforeValidator extends AbstractBeforeValidator<LocalDate> {
    @Override
    protected LocalDate convertAnchor(LocalDateTime localDateTime) {
        return LocalDate.from(localDateTime);
    }

    @Override
    protected boolean isBefore(LocalDate value, LocalDate anchor, boolean inclusive) {
        return value.isBefore(anchor) || (inclusive && value.equals(anchor));
    }
}
