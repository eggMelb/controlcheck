package au.org.checkmycontrol.web.service.repositories;

import au.org.checkmycontrol.web.domain.Identifier;
import org.springframework.data.repository.CrudRepository;

public interface IdentifierRepository extends CrudRepository<Identifier, String> {
    Identifier findTopByOrderByIdDesc();
}
