package au.org.checkmycontrol.web.domain.constraints.before;

import au.org.checkmycontrol.web.domain.constraints.Before;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

abstract class AbstractBeforeValidator<T> implements ConstraintValidator<Before, T> {
    protected T anchor;

    protected boolean inclusive;

    @Override
    public void initialize(Before constraintAnnotation) {
        anchor = convertAnchor(constraintAnnotation.value().getAnchor());
        inclusive = constraintAnnotation.inclusive();
    }

    @Override
    public boolean isValid(T value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        return isBefore(value, anchor, inclusive);
    }

    abstract T convertAnchor(LocalDateTime localDateTime);

    protected abstract boolean isBefore(T value, T anchor, boolean inclusive);
}
