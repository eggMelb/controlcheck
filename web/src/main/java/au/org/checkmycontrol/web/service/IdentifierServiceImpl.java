package au.org.checkmycontrol.web.service;

import au.org.checkmycontrol.web.domain.Identifier;
import au.org.checkmycontrol.web.service.repositories.IdentifierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Component("IdentifierService")
@Transactional
public class IdentifierServiceImpl implements IdentifierService {
    @Autowired
    private IdentifierRepository identifierRepository;

    @Override
    public Identifier generateUnique() {
        final Identifier identifier = Identifier.generateNext(identifierRepository.findTopByOrderByIdDesc());

        updateCalculatedFields(identifier);

        return identifierRepository.save(identifier);
    }

    @Override
    public Identifier save(Identifier identifier) {
        Assert.notNull(identifier, "identifier must not be null");

        return identifierRepository.save(identifier);
    }

    @Override
    public Identifier findById(String id) {
        Assert.hasLength(id, "id must not be empty");

        return identifierRepository.findOne(id);
    }

    private void updateCalculatedFields(final Identifier identifier) {
        identifier.setCreatedAt(LocalDateTime.now());
    }
}
