package au.org.checkmycontrol.web.domain.constraints.before;

import java.time.LocalDateTime;
import java.time.Year;

public class YearBeforeValidator extends AbstractBeforeValidator<Year> {
    @Override
    protected Year convertAnchor(LocalDateTime localDateTime) {
        return Year.from(localDateTime);
    }

    @Override
    protected boolean isBefore(Year value, Year anchor, boolean inclusive) {
        return value.isBefore(anchor) || (inclusive && value.equals(anchor));
    }
}
