package au.org.checkmycontrol.web.domain.constraints;

import au.org.checkmycontrol.web.domain.Drink;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidDrinkVolumeValidator implements ConstraintValidator<ValidDrinkVolume, Drink> {
    private String message;

    @Override
    public void initialize(ValidDrinkVolume constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(Drink value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        if (value.getType() == null || value.getVolume() == null) {
            return true;
        }

        final boolean isValid = value.getType().getAllowedVolumes().contains(value.getVolume());

        if (!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message).addPropertyNode("volume").addConstraintViolation();
        }

        return isValid;
    }
}
