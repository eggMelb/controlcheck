package au.org.checkmycontrol.web.domain;

import au.org.checkmycontrol.web.domain.constraints.Before;
import au.org.checkmycontrol.web.domain.constraints.Before.When;
import au.org.checkmycontrol.web.domain.enums.Country;
import au.org.checkmycontrol.web.domain.enums.Gender;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@ToString(exclude = {"identifier"})
@EqualsAndHashCode(exclude = {"identifier"})
@NoArgsConstructor
@Entity
@Table(name = "participant")
public class Participant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(optional = false)
    private Identifier identifier;

    @NotNull
    @Before(value = When.TODAY, inclusive = true)
    private LocalDate dateOfBirth;

    private Gender gender;

    private Double heightInCm;

    private Double weightInKg;

    private Country country;

    public Participant(final Identifier identifier) {
        this.identifier = identifier;
    }
}
