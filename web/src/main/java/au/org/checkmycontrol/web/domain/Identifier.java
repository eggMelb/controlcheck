package au.org.checkmycontrol.web.domain;

import au.org.checkmycontrol.web.domain.constraints.ValidIdentifier;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.Assert;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Makes use of the ISO 7064 (MOD 97-10) scheme to create a two digit checksum for code
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "identifier")
@ValidIdentifier
public class Identifier implements Serializable {
    private static final int TOTAL_LENGTH = 8;

    private static final int CHECKSUM_LENGTH = 2;

    private static final int PAYLOAD_LENGTH = TOTAL_LENGTH - CHECKSUM_LENGTH;

    private static final Pattern CODE_PATTERN = Pattern.compile("^[0-9]{" + TOTAL_LENGTH + "}$");

    @Id
    @NotNull
    @Column(length = TOTAL_LENGTH)
    private String id;

    @OneToOne(mappedBy = "identifier", cascade = CascadeType.ALL)
    private Participant participant;

    @OneToMany(mappedBy = "identifier", cascade = CascadeType.ALL)
    private List<DrinkingSession> drinkingSessions = new ArrayList<>();

    @NotNull
    @Column(nullable = false)
    private LocalDateTime createdAt;

    public Identifier(final String id) {
        this.id = id;
    }

    public static Identifier generateNext(final Identifier previous) throws IllegalArgumentException {
        if (previous == null || previous.getId() == null) {
            return new Identifier(generateFromPayload(0));
        }
        Assert.isTrue(isValid(previous.getId()), "previous id must be valid");

        final int previousPayload = Integer.parseInt(getPayload(previous.getId()));

        return new Identifier(generateFromPayload(previousPayload + 1));
    }

    public static boolean isValid(final String id) throws IllegalArgumentException {
        Assert.hasLength(id, "id must not be empty");

        return CODE_PATTERN.matcher(id).matches() && Integer.parseInt(id) % 97 == 1;
    }

    private static String generateFromPayload(final int payload) {
        Assert.isTrue(payload < Math.pow(10d, PAYLOAD_LENGTH), "payload must be < " + (int) Math.pow(10d, PAYLOAD_LENGTH));

        final String payloadString = String.format("%0" + PAYLOAD_LENGTH + "d", payload);

        return payloadString + calculateChecksum(payloadString);
    }

    private static String calculateChecksum(final String payload) {
        final int payloadValue = Integer.parseInt(getPayload(payload));

        final int checksumValue = (98 - (payloadValue * 100) % 97) % 97;

        return String.format("%0" + CHECKSUM_LENGTH + "d", checksumValue);
    }

    private static String getPayload(final String code) {
        return code.substring(0, PAYLOAD_LENGTH);
    }
}
