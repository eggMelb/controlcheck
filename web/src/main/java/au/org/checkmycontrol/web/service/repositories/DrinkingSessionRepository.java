package au.org.checkmycontrol.web.service.repositories;

import au.org.checkmycontrol.web.domain.DrinkingSession;
import org.springframework.data.repository.CrudRepository;

public interface DrinkingSessionRepository extends CrudRepository<DrinkingSession, Long> {
}
