package au.org.checkmycontrol.web.service;

import au.org.checkmycontrol.web.domain.DrinkingSession;

public interface DrinkingSessionService {
    DrinkingSession save(DrinkingSession drinkingSession);

    void updateCalculatedFields(DrinkingSession drinkingSession);
}
