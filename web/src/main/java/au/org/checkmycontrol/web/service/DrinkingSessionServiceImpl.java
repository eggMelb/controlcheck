package au.org.checkmycontrol.web.service;

import au.org.checkmycontrol.web.domain.DrinkingSession;
import au.org.checkmycontrol.web.service.repositories.DrinkingSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

@Component("DrinkingSessionService")
@Transactional
public class DrinkingSessionServiceImpl implements DrinkingSessionService {
    @Autowired
    private DrinkingSessionRepository drinkingSessionRepository;

    @Override
    public DrinkingSession save(DrinkingSession drinkingSession) {
        assertDrinkingSessionNotNull(drinkingSession);

        updateCalculatedFields(drinkingSession);

        return drinkingSessionRepository.save(drinkingSession);
    }

    @Override
    public void updateCalculatedFields(DrinkingSession drinkingSession) {
        assertDrinkingSessionNotNull(drinkingSession);
    }

    private void assertDrinkingSessionNotNull(final DrinkingSession drinkingSession) {
        Assert.notNull(drinkingSession, "drinkingSession must not be null");
    }
}
