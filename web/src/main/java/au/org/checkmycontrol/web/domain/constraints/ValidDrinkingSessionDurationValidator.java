package au.org.checkmycontrol.web.domain.constraints;

import au.org.checkmycontrol.web.domain.DrinkingSession;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidDrinkingSessionDurationValidator implements ConstraintValidator<ValidDrinkingSessionDuration, DrinkingSession> {
    private String message;

    @Override
    public void initialize(ValidDrinkingSessionDuration constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(DrinkingSession value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        if (value.getStartedAt() == null || value.getEndedAt() == null) {
            return true;
        }

        final boolean isValid = value.getEndedAt().isAfter(value.getStartedAt());

        if (!isValid) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message).addPropertyNode("endedAt").addConstraintViolation();
        }

        return isValid;
    }
}
