package au.org.checkmycontrol.web.domain.enums;

public enum DrinkVolume {
    ML_30,
    ML_60,
    ML_90,
    ML_120,
    ML_150,
    ML_200,
    ML_285,
    ML_300,
    ML_330,
    ML_375,
    ML_425,
    ML_500,
    ML_570;
}
