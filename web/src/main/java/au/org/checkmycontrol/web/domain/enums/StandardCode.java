package au.org.checkmycontrol.web.domain.enums;

import org.springframework.util.Assert;

public interface StandardCode {
    static <T extends Enum<T> & StandardCode> T getValueByCode(final String code, final Class<T> enumClazz) {
        Assert.hasLength(code, "code must not be empty");
        Assert.notNull(enumClazz, "enumClazz must not be empty");
        Assert.isTrue(enumClazz.isEnum(), "enumClazz must be an enum");

        T value = null;
        for (final T enumConstant : enumClazz.getEnumConstants()) {
            if (enumConstant.getCode().equals(code)) {
                if (value != null) {
                    throw new IllegalStateException(String.format("multiple values match code '%1$s' in class '%2$s'", code, enumClazz));
                }

                value = enumConstant;
            }
        }

        if (value == null) {
            throw new IllegalStateException(String.format("not value found matching code '%1$s' in class '%2$s", code, enumClazz));
        }

        return value;
    }

    String getCode();
}
