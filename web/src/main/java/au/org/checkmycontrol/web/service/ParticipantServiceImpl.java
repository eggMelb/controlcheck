package au.org.checkmycontrol.web.service;

import au.org.checkmycontrol.web.domain.Participant;
import au.org.checkmycontrol.web.service.repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

@Component("ParticipantService")
@Transactional
public class ParticipantServiceImpl implements ParticipantService {
    @Autowired
    private ParticipantRepository participantRepository;

    @Override
    public Participant save(Participant participant) {
        assertParticipantNotNull(participant);

        updateCalculatedFields(participant);

        return participantRepository.save(participant);
    }

    @Override
    public void updateCalculatedFields(Participant participant) {
        assertParticipantNotNull(participant);
    }

    private void assertParticipantNotNull(final Participant participant) {
        Assert.notNull(participant, "participant must not be null");
    }
}
