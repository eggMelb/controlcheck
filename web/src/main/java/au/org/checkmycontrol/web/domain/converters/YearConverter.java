package au.org.checkmycontrol.web.domain.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.Year;
import java.util.Date;

import static java.time.Instant.ofEpochMilli;
import static java.time.LocalDateTime.ofInstant;
import static java.time.ZoneId.systemDefault;

@Converter(autoApply = true)
public class YearConverter implements AttributeConverter<Year, Date> {
    @Override
    public Date convertToDatabaseColumn(Year year) {
        return year == null ? null : Date.from(year.atDay(1).atStartOfDay().atZone(systemDefault()).toInstant());
    }

    @Override
    public Year convertToEntityAttribute(Date date) {
        return date == null ? null : Year.from(ofInstant(ofEpochMilli(date.getTime()), systemDefault()).toLocalDate());
    }
}
