package au.org.checkmycontrol.web.domain.constraints;

import au.org.checkmycontrol.web.domain.Identifier;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ValidIdentifierValidator implements ConstraintValidator<ValidIdentifier, Identifier> {
    @Override
    public void initialize(ValidIdentifier constraintAnnotation) {
    }

    @Override
    public boolean isValid(Identifier value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        if (value.getId() == null) {
            return true;
        }

        boolean isValid;
        try {
            isValid = Identifier.isValid(value.getId());
        } catch (IllegalArgumentException e) {
            isValid = false;
        }

        return isValid;
    }
}
