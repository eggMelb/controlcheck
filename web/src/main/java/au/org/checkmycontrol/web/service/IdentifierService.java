package au.org.checkmycontrol.web.service;

import au.org.checkmycontrol.web.domain.Identifier;

public interface IdentifierService {
    Identifier generateUnique();

    Identifier save(Identifier identifier);

    Identifier findById(String id);
}
