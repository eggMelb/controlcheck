package au.org.checkmycontrol.web.domain.constraints.before;

import java.time.LocalDateTime;

public class LocalDateTimeValidator extends AbstractBeforeValidator<LocalDateTime> {
    @Override
    LocalDateTime convertAnchor(LocalDateTime localDateTime) {
        return localDateTime;
    }

    @Override
    protected boolean isBefore(LocalDateTime value, LocalDateTime anchor, boolean inclusive) {
        return value.isBefore(anchor) || (inclusive && value.equals(anchor));
    }
}
