package au.org.checkmycontrol.web.controllers;

import au.org.checkmycontrol.web.domain.Drink;
import au.org.checkmycontrol.web.domain.DrinkingSession;
import au.org.checkmycontrol.web.domain.Identifier;
import au.org.checkmycontrol.web.domain.Participant;
import au.org.checkmycontrol.web.domain.enums.Country;
import au.org.checkmycontrol.web.domain.enums.DrinkType;
import au.org.checkmycontrol.web.domain.enums.DrinkVolume;
import au.org.checkmycontrol.web.domain.enums.Gender;
import au.org.checkmycontrol.web.service.DrinkingSessionService;
import au.org.checkmycontrol.web.service.IdentifierService;
import au.org.checkmycontrol.web.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    private ParticipantService participantService;

    @Autowired
    private DrinkingSessionService drinkingSessionService;

    @Autowired
    private IdentifierService identifierService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String main(Model model) {
        final Identifier identifier = identifierService.generateUnique();

        final Participant participant = new Participant(identifier);
        identifier.setParticipant(participant);
        participant.setCountry(Country.AUSTRALIA);
        participant.setDateOfBirth(LocalDate.now().minusYears(13));
        participant.setGender(Gender.MALE);
        participant.setHeightInCm(170d);
        participant.setWeightInKg(85d);

        participantService.save(participant);

        final DrinkingSession drinkingSession = new DrinkingSession(identifier);
        identifier.getDrinkingSessions().add(drinkingSession);
        drinkingSession.setAppVersion("123");
        drinkingSession.setCreatedAt(LocalDateTime.now());
        drinkingSession.setStartedAt(LocalDateTime.now().minusHours(12));
        drinkingSession.setEndedAt(LocalDateTime.now().minusHours(10));

        final List<Drink> drinks = new ArrayList<>();
        drinks.add(new Drink(1, DrinkType.BEER_FULL, DrinkVolume.ML_375));
        drinks.add(new Drink(2, DrinkType.WINE_FORTIFIED, DrinkVolume.ML_60));
        drinkingSession.setDrinks(drinks);

        drinkingSessionService.save(drinkingSession);

        identifierService.save(identifier);

        model.addAttribute("value", identifier.toString());

        return "main";
    }
}
