package au.org.checkmycontrol.web.service;

import au.org.checkmycontrol.web.domain.Participant;
import org.springframework.stereotype.Service;

@Service
public interface ParticipantService {
    Participant save(Participant participant);

    void updateCalculatedFields(Participant participant);
}
